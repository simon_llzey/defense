package com.defense.http.listener;

import android.util.Log;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.defense.R;
import com.defense.custom.OverallApplication;

import java.util.Observer;

/**
 * Created by Administrator on 2016/8/9.
 */

public class StringErrorListener implements Response.ErrorListener {
    private Observer observer;

    public StringErrorListener(Observer observer) {
        this.observer = observer;
    }

    public StringErrorListener() {
        observer = null;
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        getTrouble(volleyError);
        /*if(volleyError != null && !StringUtils.isBlank(volleyError.getMessage())) {
            Log.e("Error", volleyError.getMessage());
            Toast.makeText(OverallApplication.getContext(), volleyError.getMessage(), Toast.LENGTH_SHORT).show();
        }*/
        if(observer != null)
            observer.update(null, null);
    }

    public void getTrouble(VolleyError volleyError) {
        String errType = volleyError.fillInStackTrace().toString();
        if (errType.indexOf("com.android.volley.ServerError") >= 0) {
            toast(OverallApplication.getContext().getResources().getString(R.string.Listener1));
        } else if(errType.indexOf("com.android.volley.NoConnectionError") >= 0) {
            toast(OverallApplication.getContext().getResources().getString(R.string.Listener2));

        } else if(errType.indexOf("com.android.volley.TimeoutError") >=0) {
            toast(OverallApplication.getContext().getResources().getString(R.string.Listener3));
        } else {
            toast(OverallApplication.getContext().getResources().getString(R.string.Listener4));
        }
    }

    private void toast(String s) {
        Toast.makeText(OverallApplication.getContext(), s, Toast.LENGTH_SHORT).show();
    }
}
