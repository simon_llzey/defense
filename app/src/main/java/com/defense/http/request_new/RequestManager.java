package com.defense.http.request_new;

import android.text.TextUtils;

import com.defense.http.model.ResultParam;
import com.defense.http.services.RequestService;
import com.defense.http.util.GsonUtils;
import com.defense.http.util.RequesterBase;
import com.defense.model.AnswerResultParam;
import com.defense.model.CommentParam;
import com.defense.model.ContentDetailParam;
import com.defense.model.ContentListResultParam;
import com.defense.model.InstructorDetailParam;
import com.defense.model.InstructorResultParam;
import com.defense.model.LoginParam;
import com.defense.model.LoginResultParam;
import com.defense.model.PraiseParam;
import com.defense.model.RequestParam;
import com.defense.model.SaveCommentParam;
import com.defense.model.ScheduleResultParam;
import com.defense.model.SchoolDetailParam;
import com.defense.model.SchoolList;
import com.defense.model.SchoolListResultParam;
import com.defense.model.SchoolResultParam;
import com.defense.model.SchoolSpecialtyResultParam;
import com.defense.model.TradeRequestParam;
import com.defense.model.TradeResultParam;
import com.defense.model.TrainGuideParam;
import com.defense.model.TrainStyleDetailParam;
import com.defense.model.TrainStyleParam;
import com.defense.model.UploadHeadParam;
import com.defense.model.UploadResult;

import org.xutils.common.Callback;
import org.xutils.http.HttpMethod;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.util.Observer;

import static com.android.volley.Request.Method.POST;

/**
 * Created by simon on 17/3/23.
 */

public class RequestManager {
    /**
     * 登录
     *
     * @param param
     * @param observer
     */
    public static void login(LoginParam param, Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.LOGIN);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("username", param.getUsername() + "");
        params.addBodyParameter("password", param.getPassword() + "");

        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new LoginResultParam()));
    }

    /**
     * 1.获取登陆后按用户身份特定栏目子列表
     *
     * @param requestParam
     * @param observer
     */
    public static void getTradeListData(TradeRequestParam requestParam, final Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.TRADE_LIST);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("pId", requestParam.getPid() + "");

        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new TradeResultParam()));
    }


    /**
     * 2.	获取未登陆时栏目列表（未登陆国防知识子列表）或栏目详情
     *
     * @param requestParam
     * @param observer
     */
    public static void getListData(TradeRequestParam requestParam, final Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.NO_LOGIN_COLUMN_LIST);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("pId", requestParam.getPid() + "");

        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new TradeResultParam()));
    }


    /**
     * 3.	栏目内容列表接口
     *
     * @param requestParam
     * @param observer
     */
    public static void getContentList(TradeRequestParam requestParam, final Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.CONTENT_LIST);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("module", requestParam.getModule() + "");
        if (requestParam.getHome() > -1) {
            params.addBodyParameter("home", requestParam.getHome() + "");
        }

        if (requestParam.getPageOffset() > -1) {
            params.addBodyParameter("pageOffset", requestParam.getPageOffset() + "");
        }
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new ContentListResultParam()));
    }


    /**
     * 4.	内容详情接口
     *
     * @param requestParam
     * @param observer
     */
    public static void getContentDetail(TradeRequestParam requestParam, final Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.CONTENT_DETAIL);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("id", requestParam.getId() + "");
        if (requestParam.getBrowse() > -1) {
            params.addBodyParameter("browse", requestParam.getBrowse() + "");
        }
        if (requestParam.getPraise() > -1) {
            params.addBodyParameter("praise", requestParam.getPraise() + "");
        }

        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new ContentDetailParam()));
    }

    /**
     * 5.	获取文章（国防知识）测试题
     *
     * @param requestParam
     * @param observer
     */
    public static void getDetailAnswer(TradeRequestParam requestParam, final Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.CONTENT_DETAIL_ANSWER);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("cId", requestParam.getcId() + "");
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new AnswerResultParam()));
    }

    /**
     * 1.	获取类别（理论）考核题
     *
     * @param requestParam
     * @param observer
     */
    public static void getTheoryList(RequestParam requestParam, final Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.THEORY_LIST);
        params.setConnectTimeout(15*1000);
//        params.addBodyParameter("tId", requestParam.gettId() + "");
        /*if (requestParam.getPageOffset() > -1) {
            params.addBodyParameter("pageOffset", requestParam.getPageOffset() + "");
        }
        if (requestParam.getPageSize() > -1) {
            params.addBodyParameter("pageSize", requestParam.getPageSize() + "");
        }*/
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new AnswerResultParam()));
    }

    /**
     * 15.	获取教官列表
     *
     * @param observer
     */
    public static void getInstructorList(final Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.INSTRUCTOR_LIST);
        params.setConnectTimeout(15*1000);
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new InstructorResultParam()));
    }


    /**
     * 3.	栏目内容列表接口 -- 军训风采
     *
     * @param requestParam
     * @param observer
     */
    public static void getTrainStyle(TradeRequestParam requestParam, final Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.CONTENT_LIST);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("module", requestParam.getModule() + "");
        if (requestParam.getHome() > -1) {
            params.addBodyParameter("home", requestParam.getHome() + "");
        }

        if (requestParam.getPageOffset() > -1) {
            params.addBodyParameter("pageOffset", requestParam.getPageOffset() + "");
        }
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new TrainStyleParam()));
    }

    /**
     * 4.	内容详情接口-军训风采详情
     *
     * @param requestParam
     * @param observer
     */
    public static void getTrainStyleDetail(TradeRequestParam requestParam, final Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.TRAIN_STYLE_DETAIL);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("id", requestParam.getId() + "");
        params.addBodyParameter("browse", requestParam.getBrowse() + "");
        params.addBodyParameter("comment", requestParam.getComment() + "");
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new TrainStyleDetailParam()));
    }

    /**
     * 军训指南-军训常识
     * 军训指南-管理规定
     *
     * @param requestParam
     * @param observer
     */
    public static void getTrainGuide_1(TradeRequestParam requestParam, final Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.CONTENT_LIST);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("module", requestParam.getModule() + "");
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new TrainGuideParam()));
    }


    /**
     * 教官详情
     *
     * @param requestParam
     * @param observer
     */
    public static void getInstructorDetail(TradeRequestParam requestParam, final Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.CONTENT_DETAIL);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("id", requestParam.getId() + "");
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new InstructorDetailParam()));
    }


    /**
     * 8.	获取文章点赞列表
     *
     * @param requestParam
     * @param observer
     */
    public static void getPraiseList(TradeRequestParam requestParam, final Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.PRAISE_LIST);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("cId", requestParam.getcId() + "");
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new PraiseParam()));
    }


    /**
     * 5.	获取文章评论列表详情
     *
     * @param requestParam
     * @param observer
     */
    public static void getCommentList(TradeRequestParam requestParam, final Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.COMMENT_LIST);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("cId", requestParam.getcId() + "");
        if (requestParam.getPageOffset() > -1) {
            params.addBodyParameter("pageOffset", requestParam.getPageOffset() + "");
        }
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new CommentParam()));
    }

    /**
     * 保存评论
     *
     * @param requestParam
     * @param observer
     */
    public static void saveComment(TradeRequestParam requestParam, final Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.SAVE_COMMENT_LIST);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("cId", requestParam.getcId() + "");
        params.addBodyParameter("details", requestParam.getDetails());
        if (requestParam.getPid() > -1) {
            params.addBodyParameter("pId", requestParam.getPid() + "");
        }
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new SaveCommentParam()));
    }


    /**
     * 保存点赞
     *
     * @param requestParam
     * @param observer
     */
    public static void savePraise(TradeRequestParam requestParam, final Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.SAVE_PRAISE);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("cId", requestParam.getcId() + "");
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new SaveCommentParam()));
    }


    /**
     * 1.	用户获取验证码接口
     *
     * @param requestParam
     * @param observer
     */
    public static void getValidate(RequestParam requestParam, final Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.VALIDATE_CODE);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("phone", requestParam.getPhone());
        params.addBodyParameter("type", requestParam.getType());
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new ResultParam()));
    }

    /**
     * 1.	用户注册接口
     *
     * @param requestParam
     * @param observer
     */
    public static void getRegister(RequestParam requestParam, final Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.REGISTER);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("phone", requestParam.getPhone());
        params.addBodyParameter("password", requestParam.getPassword());
        params.addBodyParameter("validateCode", requestParam.getValidateCode());

        params.addBodyParameter("name", requestParam.getValidateCode());

        if (requestParam.getSchool_id() > -1) {
            params.addBodyParameter("school_id", requestParam.getSchool_id() + "");
        }

        if (requestParam.getSecond_school_id() > -1) {
            params.addBodyParameter("second_school_id", requestParam.getSecond_school_id() + "");
        }
        if (requestParam.getSpecialty_id() > -1) {
            params.addBodyParameter("specialty_id", requestParam.getSpecialty_id() + "");
        }

        if (!TextUtils.isEmpty(requestParam.getGradeStr())) {

            params.addBodyParameter("gradeStr", requestParam.getGradeStr());
        }
        if (!TextUtils.isEmpty(requestParam.getGradeStr())) {
            params.addBodyParameter("classStr", requestParam.getClassStr());
        }


        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new ResultParam()));
    }

    /**
     * 18.用户修改密码接口
     *
     * @param requestParam
     * @param observer
     */
    public static void getChangePwd(RequestParam requestParam, final Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.CHANGE_PWD);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("phone", requestParam.getPhone());
        params.addBodyParameter("password", requestParam.getPassword());
        params.addBodyParameter("validateCode", requestParam.getValidateCode());
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new ResultParam()));
    }


    /**
     * 上传头像
     *
     * @param requestParam
     * @param observer
     */
    public static void upload_head(RequestParam requestParam, Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.UPLOAD_HEAD);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("head", requestParam.getHead());
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new UploadHeadParam()));
    }

    /**
     * 修改昵称
     *
     * @param requestParam
     * @param observer
     */
    public static void upload_nickname(RequestParam requestParam, Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.UPLOAD_NICKNAME);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("nickname", requestParam.getNickname());
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new ResultParam()));
    }


    /**
     * @param observer
     */
    public static void getSchedule(Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.SCHEDULE_LIST);
        params.setConnectTimeout(15*1000);
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new ScheduleResultParam()));
    }


    /**
     * 获取学校
     *
     * @param observer
     */
    public static void getSchool(Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.SCHOOL_LIST);
        params.setConnectTimeout(15*1000);
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new SchoolResultParam()));
    }


    /**
     * 获取学校列表
     *
     * @param requestParam
     * @param observer
     */
    public static void getSchoolList(RequestParam requestParam, Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.SCHOOL_LIST_2);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("orgId", requestParam.getOrgId() + "");
        params.addBodyParameter("pageOffset", requestParam.getPageOffset() + "");
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new SchoolListResultParam()));
    }

    /**
     * 获取学校详情
     *
     * @param requestParam
     * @param observer
     */
    public static void getSchoolDetail(RequestParam requestParam, Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.SCHOOL_LIST_DETAIL);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("id", requestParam.getOrgId() + "");
        params.addBodyParameter("browse", requestParam.getBrowse() + "");
        params.addBodyParameter("praise", requestParam.getPraise() + "");
        params.addBodyParameter("comment", requestParam.getComment() + "");
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new SchoolDetailParam()));
    }


    /**
     * 上传学校风采
     *
     * @param requestParam
     * @param observer
     */
    public static void upload_style(RequestParam requestParam, Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.UPLOAD_MSG);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("images", requestParam.getImages()+"");
        params.addBodyParameter("details", requestParam.getDetails());
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new UploadResult()));
    }

    /**
     * 提交答题
     *
     * @param observer
     */
    public static void post_score(RequestParam requestParam, Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.POST_SCORE);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("score", requestParam.getScore() + "");
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new ResultParam()));
    }

    /**
     * 注册时的学校列表
     *
     * @param requestParam
     * @param observer
     */
    public static void register_school_list(RequestParam requestParam, Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.GET_SCHOOL_LIST);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("type", requestParam.getType() + "");
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new SchoolList()));
    }


    /**
     * 设置用户学校
     *
     * @param requestParam
     * @param observer
     */
    public static void setModify(RequestParam requestParam, Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.SET_MODIFY);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("name", requestParam.getName());
        if (requestParam.getSchool_id() > -1) {
            params.addBodyParameter("school_id", requestParam.getSchool_id() + "");
        }
        if (requestParam.getSecond_school_id() > -1) {
            params.addBodyParameter("second_school_id", requestParam.getSecond_school_id() + "");
        }

        if (requestParam.getSpecialty_id() > -1) {
            params.addBodyParameter("specialty_id", requestParam.getSpecialty_id() + "");
        }

        if (!TextUtils.isEmpty(requestParam.getGradeStr())) {
            params.addBodyParameter("gradeStr", requestParam.getGradeStr() + "");
        }
        if (!TextUtils.isEmpty(requestParam.getClassStr())) {
            params.addBodyParameter("classStr", requestParam.getClassStr() + "");
        }

        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new ResultParam()));
    }

    /**
     * 获取专业
     *
     * @param requestParam
     * @param observer
     */
    public static void register_school_specialty(RequestParam requestParam, Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.GET_SPECIALTY);
        params.setConnectTimeout(15*1000);
        params.addBodyParameter("school_id", requestParam.getSchool_id() + "");
        if (requestParam.getSecond_school_id() > -1) {
            params.addBodyParameter("second_school_id", requestParam.getSecond_school_id() + "");
        }

        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new SchoolSpecialtyResultParam()));
    }

    /**
     * 退出登录
     *
     * @param observer
     */
    public static void logout(Observer observer) {
        RequestParams params = new RequestParams(RequesterBase.LOGOUT);
        params.setConnectTimeout(15*1000);
        x.http().request(HttpMethod.POST, params, new CallbackUtils(observer, new ResultParam()));
    }

}
