package com.defense.http.request_new;

import android.util.Log;

import com.defense.http.util.GsonUtils;

import org.xutils.common.Callback;

import java.lang.reflect.Type;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by simon on 17/3/23.
 */

public class CallbackUtils implements Callback.CommonCallback<String> {

    private Observer observer;
    private Object object;
    public CallbackUtils(Observer o, Object obj){
        this.observer = o;
        this.object = obj;
    }

    @Override
    public void onSuccess(String o) {
        Object obj = GsonUtils.asEntity(o, object.getClass());

        observer.update(null, obj);
    }

    @Override
    public void onError(Throwable throwable, boolean b) {
        observer.update(null, null);
    }

    @Override
    public void onCancelled(CancelledException e) {
        observer.update(null, null);
    }

    @Override
    public void onFinished() {

    }
}
