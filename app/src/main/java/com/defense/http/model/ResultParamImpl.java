package com.defense.http.model;

/**
 * Created by Administrator on 2016/8/9.
 */

public interface ResultParamImpl {
    public Object getData();
    public boolean isStatus();
    public boolean isUnLoginStatus();
    public String getErrorsStr();
    public String getMessage();
}
