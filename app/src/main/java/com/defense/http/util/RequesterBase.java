package com.defense.http.util;

/**
 * Created by Administrator on 2016/8/9.
 */

public class RequesterBase {
//    public static final String API_Root = "http://139.159.221.244:8080/guofang/";
    public static final String API_Root = "http://112.74.169.218:8080/guofang/";
//    public static final String API_Root = "http://192.168.42.239:8080/guofang/";

    //1.	用户登录接口
    public static final String LOGIN = API_Root + "ws/user/login.jhtml";
    public static final String LOGOUT = API_Root + "ws/se/user/login_out.jhtml";
    //1.	用户获取验证码接口
    public static final String VALIDATE_CODE = API_Root + "ws/user/validate_code.jhtml";
    //1.	用户注册接口
    public static final String REGISTER = API_Root + "ws/user/reg.jhtml";
    //18.	用户修改密码接口
    public static final String CHANGE_PWD = API_Root + "ws/user/change_pwd.jhtml";

    //2.	获取未登陆时栏目列表（未登陆国防知识子列表）或栏目详情
    public static final String NO_LOGIN_COLUMN_LIST = API_Root + "ws/module/list.jhtml";

    //获取登陆后按用户身份特定栏目子列表（登陆后国防知识子列表）或栏目详情
    public static final String TRADE_LIST = API_Root + "ws/module/list.jhtml";

    //3.	栏目内容列表接口
    public static final String CONTENT_LIST = API_Root + "ws/content/list.jhtml";
    //4.	内容详情接口
    public static final String CONTENT_DETAIL = API_Root + "ws/content/detail.jhtml";

    //5.	获取文章（国防知识）测试题
    public static final String CONTENT_DETAIL_ANSWER = API_Root + "ws/examine/content_list.jhtml";


    //15.	获取教官列表
    public static final String INSTRUCTOR_LIST = API_Root + "ws/military/instructor/list.jhtml";

    //8.	获取文章点赞列表
    public static final String PRAISE_LIST = API_Root + "ws/content/praise/list.jhtml";
    //5.	获取文章评论列表详情
    public static final String COMMENT_LIST = API_Root + "ws/content/comment/list.jhtml";
    //6.	保存评论
    public static final String SAVE_COMMENT_LIST = API_Root + "ws/content/comment/save.jhtml";
    //9.	点赞保存
    public static final String SAVE_PRAISE = API_Root + "ws/content/praise/save.jhtml";


    //1.	获取类别（理论）考核题
    public static final String THEORY_LIST = API_Root + "ws/examine/trade_list.jhtml";

    //用户上传头像
    public static final String UPLOAD_HEAD = API_Root + "ws/user/upload_head.jhtml";
    //用户修改昵称
    public static final String UPLOAD_NICKNAME = API_Root + "ws/user/update_info.jhtml";

    //获取军事训练计划
    public static final String SCHEDULE_LIST = API_Root + "ws/military/schedule.jhtml";

    //获取学校
    public static final String SCHOOL_LIST = API_Root + "ws/military/school_list.jhtml";

    //获取学校列表
    public static final String SCHOOL_LIST_2 = API_Root + "ws/military/train_list.jhtml";
    //获取学校详情
    public static final String SCHOOL_LIST_DETAIL = API_Root + "ws/military/train_detail.jhtml";
    //上传军事风采
    public static final String UPLOAD_MSG = API_Root + "ws/military/upload_msg.jhtml";
    //军事风采详情
    public static final String TRAIN_STYLE_DETAIL = API_Root + "ws/military/train_detail.jhtml";

    //提交答题
    public static final String POST_SCORE = API_Root + "ws/examine/save_score.jhtml";

    //获取学校列表
    public static final String GET_SCHOOL_LIST = API_Root + "ws/user/get_school.jhtml";
    //获取专业
    public static final String GET_SPECIALTY = API_Root + "ws/user/get_specialty.jhtml";

    //修改用户信息
    public static final String SET_MODIFY = API_Root + "ws/user/modify.jhtml";


}
