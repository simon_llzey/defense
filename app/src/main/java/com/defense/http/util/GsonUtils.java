package com.defense.http.util;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/9/28.
 */

public class GsonUtils {
    public static <Entity> Entity asEntity(String json, Type typeOfT) {
        try {
            Log.e("NetResult", json);
            Gson gson = new Gson();
            return gson.fromJson(json, typeOfT);
        } catch (Exception e ) {
            e.printStackTrace();
            return null;
        }
    }

    public static String toJson(Object list){
        try {
            Gson gson = new Gson();
            return gson.toJson(list);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("gson", "json错误"+e.getMessage());
            return null;
        }
    }

    /**
     * 转成list
     *
     * @param gsonString
     * @param cls
     * @return
     */
    public static <T> List<T> GsonToList(String gsonString, Class<T> cls) {
        List<T> list = new ArrayList<T>();
        try {
            Gson gson = new Gson();
            JsonArray arry = new JsonParser().parse(gsonString).getAsJsonArray();
            for (JsonElement jsonElement : arry) {
                list.add(gson.fromJson(jsonElement, cls));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;

    }

}
