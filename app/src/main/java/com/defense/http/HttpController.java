package com.defense.http;

import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.defense.custom.OverallApplication;
import com.defense.http.listener.StringErrorListener;
import com.defense.http.model.ResultParam;

/**
 * Created by Administrator on 2016/8/9.
 */

public class HttpController {
    protected static int POST = Request.Method.POST;
    protected static int GET = Request.Method.GET;
    private static HttpController controller;

    protected static String unErrorString = "发生未知错误";

    protected static Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError volleyError) {

        }
    };

    protected static StringErrorListener stringErrorListener  = new StringErrorListener();

    private static HttpController getController() {
        if (controller == null) {
            controller = new HttpController();
        }
        return controller;
    }

    protected static void reportError(ResultParam rr) {
        if (rr != null) {
            reportError(rr.getErrorsStr());
        } else {
            reportError("Json 数据转换错误！");
        }
    }

    protected static void reportError(String err) {
        Toast.makeText(OverallApplication.getContext(), err, Toast.LENGTH_LONG).show();
    }
}
