package com.defense.http;


import com.defense.http.services.RequestService;
import com.defense.http.util.RequesterBase;
import com.defense.model.LoginParam;
import com.defense.model.LoginResultParam;
import com.defense.model.RequestParam;
import com.defense.model.TradeRequestParam;
import com.defense.model.TradeResultParam;
import com.defense.model.UploadHeadParam;

import java.util.Observer;

import static com.android.volley.Request.Method.POST;

/**
 * Created by Administrator on 2016/8/9.
 */

public class UserController extends HttpController {
    public static void loginToMain(LoginParam lp, Observer observer) {
        //调用中间层，进行数据请求
//        RequestService.sendRequest(POST, RequesterBase.API_Login, observer, lp, new Object());
    }

    /**
     * 登录
     * @param param
     * @param observer
     */
    public static void login(LoginParam param, Observer observer){
        RequestService.sendRequest(POST, RequesterBase.LOGIN, observer, param, LoginResultParam.class);
    }

    /**
     * 获取未登陆时栏目列表
     * @param param
     * @param observer
     */
    public static void getNO_lOGIN_LIST(LoginParam param, Observer observer){
        RequestService.sendRequest(POST, RequesterBase.NO_LOGIN_COLUMN_LIST, observer, param, LoginResultParam.class);
    }


    /**
     * 获取登陆后按用户身份特定栏目子列表
     * @param param
     * @param observer
     */
    public static void getTradeListData(TradeRequestParam param, Observer observer){
        RequestService.sendRequest(POST, RequesterBase.TRADE_LIST, observer, param, TradeResultParam.class);
    }

}
