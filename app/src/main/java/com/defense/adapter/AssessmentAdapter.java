package com.defense.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.listener.OnRecyclerClickListener;
import com.defense.model.AnswerResultParam;

import java.util.List;

/**
 * Created by Administrator on 2017/3/14.
 * 理论考核列表
 */

public class AssessmentAdapter extends RecyclerView.Adapter<AssessmentAdapter.ViewHolder> {

    private Context mContext;
    private List<AnswerResultParam.Data.DataBean.OptionListBean> mList;
    private OnRecyclerClickListener onRecyclerClickListener;

    public AssessmentAdapter(Context context, List<AnswerResultParam.Data.DataBean.OptionListBean> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AssessmentAdapter.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.assessment_list_info, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {


        AnswerResultParam.Data.DataBean.OptionListBean optionListBean = mList.get(position);

        holder.tv_name.setText(optionListBean.getOption());

        if (optionListBean.isSelect()){
            showImg(holder.img_sel, R.drawable.exam_selected);
        }


        holder.lin_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onRecyclerClickListener != null){
                    onRecyclerClickListener.onItemListener(position);
                }
                /*for (AnswerResultParam.DataBean.OptionListBean listBean : mList){
                    listBean.setSelect(false);
                }
                mList.get(position).setSelect(true);
                notifyDataSetChanged();*/
            }
        });

    }
    private void showImg(ImageView imageView, int id){
        Glide.with(mContext).
                load(id).
                asBitmap().
                into(imageView);
    }
    @Override
    public int getItemCount() {
        return mList.size();
    }


    public void setOnRecyclerClickListener(OnRecyclerClickListener onRecyclerClickListener) {
        this.onRecyclerClickListener = onRecyclerClickListener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView img_sel;
        private TextView tv_name;
        private LinearLayout lin_content;

        public ViewHolder(View itemView) {
            super(itemView);
            img_sel = (ImageView) itemView.findViewById(R.id.img_sel);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            lin_content = (LinearLayout) itemView.findViewById(R.id.lin_content);


        }
    }
}
