package com.defense.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.model.CategoryInfo;
import com.defense.model.TradeData;
import com.defense.view.fragment.KnowledgeFrag;

import java.util.List;

/**
 * Created by Administrator on 2017/3/7.
 * 国防知识分类
 */

public class KnowledgeCategoryAdapter extends BaseAdapter {

    private List<TradeData> mList;
    private Context mContext;

    public KnowledgeCategoryAdapter(Context context, List<TradeData> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.category_grid_info, null);
            viewHolder.img_head = (ImageView) convertView.findViewById(R.id.img_head);
            viewHolder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tv_name.setText(mList.get(position).getShowname());
        Glide.with(mContext).
                load(mList.get(position).getIcon()).
                asBitmap().
                placeholder(R.mipmap.def_1).
                into(viewHolder.img_head);

        return convertView;
    }

    private class ViewHolder {
        private TextView tv_name;
        private ImageView img_head;
    }

}
