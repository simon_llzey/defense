package com.defense.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.model.TradeData;

import java.util.List;

/**
 * Created by simon on 17/3/22.
 * 获取登陆后按用户身份特定栏目子列表
 */

public class TradeListAdapter extends BaseAdapter {

    private Context mContext;
    private List<TradeData> mList;
    public TradeListAdapter(Context context, List<TradeData> list){
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(mContext).inflate(R.layout.trade_list_info, null);
        TextView tv_name = (TextView)convertView.findViewById(R.id.tv_name);
        ImageView img_bottom = (ImageView)convertView.findViewById(R.id.img_bottom);
        tv_name.setText(mList.get(position).getName());


        return convertView;
    }
}
