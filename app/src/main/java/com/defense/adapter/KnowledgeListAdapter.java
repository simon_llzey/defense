package com.defense.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.defense.R;
import com.defense.model.ContentListParam;
import com.defense.model.DemoInfo;
import com.defense.model.TradeData;
import com.defense.view.ArticleActivity;

import java.util.List;

/**
 * Created by Administrator on 2017/3/8.
 * 国防知识登录后列表
 */

public class KnowledgeListAdapter extends BaseAdapter {

    private Context mContext;
    private List<TradeData>  mList;

    public KnowledgeListAdapter(Context context, List<TradeData> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {


        } /*else {
            viewHolder = (ViewHolder) convertView.getTag();
        }*/
        Log.e("adapter", "当前位置=" + (position % 2));
        viewHolder = new ViewHolder();

        if (position % 2 == 1) {
            convertView = showLeft(viewHolder);
        } else {
            convertView = showRight(viewHolder);
        }

        if (position == 0){
            viewHolder.lin_other.setVisibility(View.INVISIBLE);
        }

        viewHolder.tv_title.setText(mList.get(position).getShowname());
        viewHolder.tv_info.setText(mList.get(position).getIntro());

        viewHolder.lin_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent articIntent = new Intent(mContext, ArticleActivity.class);
                articIntent.putExtra("id", mList.get(position).getId());
                mContext.startActivity(articIntent);
            }
        });

        return convertView;
    }

    private class ViewHolder {
        private ImageView img_head;
        private TextView tv_title;
        private TextView tv_info;
        private LinearLayout lin_content;
        private LinearLayout lin_other;

    }


    private View showLeft(ViewHolder viewHolder) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.knowledge_list_info_left, null);
        viewHolder.img_head = (ImageView) view.findViewById(R.id.img_head_left);
        viewHolder.lin_content = (LinearLayout) view.findViewById(R.id.lin_content_left);
        viewHolder.lin_other = (LinearLayout) view.findViewById(R.id.lin_other);
        viewHolder.tv_title = (TextView) view.findViewById(R.id.tv_title_left);
        viewHolder.tv_info = (TextView) view.findViewById(R.id.tv_info_left);
        return view;
    }

    private View showRight(ViewHolder viewHolder) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.knowledge_list_info, null);
        viewHolder.img_head = (ImageView) view.findViewById(R.id.img_head);
        viewHolder.lin_content = (LinearLayout) view.findViewById(R.id.lin_content);
        viewHolder.lin_other = (LinearLayout) view.findViewById(R.id.lin_other);
        viewHolder.tv_title = (TextView) view.findViewById(R.id.tv_title);
        viewHolder.tv_info = (TextView) view.findViewById(R.id.tv_info);

        return view;
    }

}
