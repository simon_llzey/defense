package com.defense.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.model.ContentListParam;
import com.defense.utils.SizeUtils;

import java.util.List;

/**
 * Created by simon on 17/4/10.
 * 社会实践
 */

public class PracticeAdapter extends BaseAdapter {

    private List<ContentListParam> mList;
    private Context mContext;

    int width = 0;

    public PracticeAdapter(Context context, List<ContentListParam> list) {
        this.mContext = context;
        this.mList = list;
        width = SizeUtils.getWidth(context);
    }


    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.practice_list_info, null);
            viewHolder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
            viewHolder.tv_intro = (TextView) convertView.findViewById(R.id.tv_intro);
            viewHolder.img_head = (ImageView) convertView.findViewById(R.id.img_head);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Glide.with(mContext).
                load(mList.get(position).getImage()).
                asBitmap().
                placeholder(R.mipmap.def_1).
                into(viewHolder.img_head);

        viewHolder.tv_title.setText(mList.get(position).getTitle());
        viewHolder.tv_intro.setText(mList.get(position).getIntro());
        return convertView;
    }


    private class ViewHolder {
        private TextView tv_title;
        private TextView tv_intro;
        private ImageView img_head;
    }

}
