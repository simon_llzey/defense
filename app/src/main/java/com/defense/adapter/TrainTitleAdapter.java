package com.defense.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by Administrator on 2017/3/9.
 */

public class TrainTitleAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> mList;
    private List<String> mTitle;
    private Context mContext;

    public TrainTitleAdapter(Context context, FragmentManager manager, List<Fragment> fragments, List<String> titles){
        super(manager);
        this.mContext = context;
        this.mList = fragments;
        this.mTitle = titles;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Fragment getItem(int position) {
        return mList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitle.get(position %mTitle.size());
    }


     /*public View getTabView(int position) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        TextView tv = (TextView) v.findViewById(R.id.tv_name);
        tv.setText(mTitle.get(position));
        return v;
    }*/
}