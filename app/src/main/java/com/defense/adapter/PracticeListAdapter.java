package com.defense.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.model.ContentListParam;
import com.defense.view.HotDetailActivity;

import java.util.List;

/**
 * Created by simon on 17/4/11.
 * 社会实践列表
 */

public class PracticeListAdapter extends RecyclerView.Adapter<PracticeListAdapter.MyViewHolder> {

    private Context mContext;
    private List<ContentListParam> mList;

    public PracticeListAdapter(Context context, List<ContentListParam> list) {
        this.mContext = context;
        this.mList = list;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PracticeListAdapter.MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.practice_list_info, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Glide.with(mContext).
                load(mList.get(position).getImage()).
                asBitmap().
                placeholder(R.drawable.def_1).
                into(holder.img_head);

        holder.tv_title.setText(mList.get(position).getTitle());
        holder.tv_intro.setText(mList.get(position).getIntro());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;
        private TextView tv_intro;
        private ImageView img_head;
        private LinearLayout lin_content;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            tv_intro = (TextView) itemView.findViewById(R.id.tv_intro);
            img_head = (ImageView) itemView.findViewById(R.id.img_head);
            lin_content = (LinearLayout) itemView.findViewById(R.id.lin_content);
            lin_content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent detailIntent = new Intent(mContext, HotDetailActivity.class);
                    detailIntent.putExtra("id", mList.get(getAdapterPosition()).getId());
                    mContext.startActivity(detailIntent);
                }
            });
        }

    }
}
