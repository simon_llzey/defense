package com.defense.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.listener.OnRecyclerClickListener;
import com.defense.model.ContentListParam;
import com.defense.view.CommentActivity;
import com.joooonho.SelectableRoundedImageView;

import java.util.List;

/**
 * Created by Administrator on 2017/3/14.
 * 国防课堂列表
 */

public class ClassRoomAdapter extends RecyclerView.Adapter<ClassRoomAdapter.RoomViewHolder> {

    private Context mContext;
    private OnRecyclerClickListener clickListener;
    private List<ContentListParam> mList;

    public ClassRoomAdapter(Context context, List<ContentListParam> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public RoomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ClassRoomAdapter.RoomViewHolder(LayoutInflater.from(mContext).inflate(R.layout.classroom_list_info, parent, false));
    }

    @Override
    public void onBindViewHolder(RoomViewHolder holder, int position) {
        Glide.with(mContext).
                load(mList.get(position).getImage()).
                asBitmap().
                placeholder(R.mipmap.def_3).
                into(holder.img_detail);

       holder.tv_intro.setText(mList.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    /**
     * 点击事件
     *
     * @param clickListener
     */
    public void setClickListener(OnRecyclerClickListener clickListener) {
        this.clickListener = clickListener;
    }

    class RoomViewHolder extends RecyclerView.ViewHolder {
        private SelectableRoundedImageView img_detail;
        private SelectableRoundedImageView img_head;
        private SelectableRoundedImageView img_head2;
        private LinearLayout lin_detail;
        private LinearLayout lin_comment;
        private TextView tv_intro;

        public RoomViewHolder(View itemView) {
            super(itemView);
            img_detail = (SelectableRoundedImageView) itemView.findViewById(R.id.img_detail);
            img_head = (SelectableRoundedImageView) itemView.findViewById(R.id.img_head);
            img_head2 = (SelectableRoundedImageView) itemView.findViewById(R.id.img_head2);
            lin_detail = (LinearLayout) itemView.findViewById(R.id.lin_detail);
            lin_comment = (LinearLayout) itemView.findViewById(R.id.lin_comment);
            tv_intro = (TextView) itemView.findViewById(R.id.tv_intro);
            lin_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListener != null) {
                        clickListener.onItemListener(getAdapterPosition());
                    }
                }
            });

            lin_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.startActivity(new Intent(mContext, CommentActivity.class));
                }
            });
        }

    }
}
