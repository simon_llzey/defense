package com.defense.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.model.ContentListParam;
import com.defense.utils.DateUtils;
import com.defense.utils.SizeUtils;

import java.util.List;

/**
 * Created by Administrator on 2017/3/16.
 */

public class HotConcernGridAdapter extends BaseAdapter {

    private Context mContext;
    private List<ContentListParam> mList;
    private int width = 0;

    public HotConcernGridAdapter(Context context, List<ContentListParam> list) {
        this.mContext = context;
        this.mList = list;
        width = SizeUtils.getWidth(context);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.hot_concern_list_info, null);
            viewHolder.img_head = (ImageView) convertView.findViewById(R.id.img_head);
            viewHolder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
            viewHolder.tv_time = (TextView) convertView.findViewById(R.id.tv_time);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width / 2, width / 3);
        viewHolder.img_head.setLayoutParams(params);
        Glide.with(mContext).
                load(mList.get(position).getImage()).
                asBitmap().
                placeholder(R.mipmap.def_1).
                into(viewHolder.img_head);

        viewHolder.tv_name.setText(mList.get(position).getTitle());
        viewHolder.tv_time.setText(DateUtils.yearMonthDay(mList.get(position).getCreate_time()));
        return convertView;
    }

    private class ViewHolder {
        private ImageView img_head;
        private TextView tv_name;
        private TextView tv_time;
    }
}
