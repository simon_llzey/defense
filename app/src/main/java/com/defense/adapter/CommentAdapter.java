package com.defense.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.defense.R;
import com.defense.model.CommentParam;

import java.util.List;

/**
 * Created by simon on 17/3/28.
 * 评论列表
 */

public class CommentAdapter extends BaseAdapter {

    private Context mContext;
    private List<CommentParam.DataBean> mList;


    String comeback = " 回复 ";

    public CommentAdapter(Context context, List<CommentParam.DataBean> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.comment_list_info1, null);
            viewHolder.tv_member = (TextView) convertView.findViewById(R.id.tv_member);
            viewHolder.tv_content = (TextView) convertView.findViewById(R.id.tv_content);
            viewHolder.lin_reply = (LinearLayout) convertView.findViewById(R.id.lin_reply);
            viewHolder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
            viewHolder.tv_p_name = (TextView) convertView.findViewById(R.id.tv_p_name);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        String p_name = mList.get(position).getP_name();
        String m_name = mList.get(position).getMember_name();


        if (!TextUtils.isEmpty(p_name)) {
            SpannableStringBuilder ssb = new SpannableStringBuilder(p_name + comeback + m_name);
            ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#556b95")), 0, p_name.length(),
                    Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
            ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#2b2b2b")), p_name.length(), p_name.length() + comeback.length(),
                    Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
            ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#556b95")), p_name.length() + comeback.length(), p_name.length() + comeback.length() + m_name.length(),
                    Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
            viewHolder.tv_member.setText(ssb);
        } else {
            viewHolder.tv_member.setText(mList.get(position).getMember_name());
        }

        viewHolder.tv_content.setText(mList.get(position).getDetails());


        return convertView;
    }


    private class ViewHolder {
        private TextView tv_member;
        private TextView tv_content;
        private LinearLayout lin_reply;
        private TextView tv_name;
        private TextView tv_p_name;
    }
}
