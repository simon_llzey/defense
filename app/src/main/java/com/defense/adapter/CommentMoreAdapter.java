package com.defense.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.model.CommentParam;
import com.joooonho.SelectableRoundedImageView;

import java.util.List;

/**
 * Created by simon on 17/3/29.
 * 文章更多评论
 */

public class CommentMoreAdapter extends RecyclerView.Adapter<CommentMoreAdapter.MyViewHolder> {


    private Context mContext;
    private List<CommentParam.DataBean> mList;

    public CommentMoreAdapter(Context context, List<CommentParam.DataBean> list) {
        this.mContext = context;
        this.mList = list;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CommentMoreAdapter.MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.comment_more_list_info, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.tv_name.setText(mList.get(position).getMember_name());
        holder.tv_content.setText(mList.get(position).getDetails());

        /*Glide.with(mContext).
                load(mList.get(position).get()).
                asBitmap().
                into(holder.img_head);*/
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private SelectableRoundedImageView img_head;
        private TextView tv_name;
        private TextView tv_content;


        public MyViewHolder(View itemView) {
            super(itemView);

            img_head = (SelectableRoundedImageView) itemView.findViewById(R.id.img_head);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_content = (TextView) itemView.findViewById(R.id.tv_content);

        }
    }
}
