package com.defense.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.model.ContentListParam;
import com.defense.model.SchoolResultParam;
import com.defense.utils.SizeUtils;

import java.util.List;

/**
 * Created by simon on 17/4/17.
 * 军事风采-学校列表
 */

public class SchoolListAdapter extends BaseAdapter {

    private Context mContext;
    private List<SchoolResultParam.SchoolBean.ImglistBean> mList;
    private int width = 0;

    public SchoolListAdapter(Context context, List<SchoolResultParam.SchoolBean.ImglistBean> list) {
        this.mContext = context;
        this.mList = list;
        width = SizeUtils.getWidth(context);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.school_list_info, null);
            viewHolder.img_head = (ImageView) convertView.findViewById(R.id.img_head);
            viewHolder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Glide.with(mContext).
                load(mList.get(position).getLogo()).
                asBitmap().
                placeholder(R.mipmap.def_1).
                into(viewHolder.img_head);

        viewHolder.tv_name.setText(mList.get(position).getName());

        return convertView;
    }

    private class ViewHolder {
        private ImageView img_head;
        private TextView tv_name;
    }
}
