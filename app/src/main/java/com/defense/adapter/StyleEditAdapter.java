package com.defense.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.defense.R;

import java.util.List;

/**
 * Created by simon on 17/4/27.
 * 编辑图片列表
 */

public class StyleEditAdapter extends BaseAdapter {

    private Context mContext;
    private List<String> mList;

    public StyleEditAdapter(Context context, List<String> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.style_edit_list, null);
            viewHolder.mImage = (ImageView) convertView.findViewById(R.id.img_head);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Glide.with(mContext).
                load(mList.get(position)).
                asBitmap().
                into(viewHolder.mImage);

        return convertView;
    }

    private class ViewHolder {
        private ImageView mImage;
    }

}
