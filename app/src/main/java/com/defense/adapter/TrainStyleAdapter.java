package com.defense.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.http.util.GsonUtils;
import com.defense.listener.OnRecyclerClickListener;
import com.defense.model.SchoolListResultParam;
import com.defense.view.ImagePageActivity;
import com.defense.view.TrainStyleListActivity;
import com.joooonho.SelectableRoundedImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/3/9.
 * 军训风采列表
 */

public class TrainStyleAdapter extends RecyclerView.Adapter<TrainStyleAdapter.MyViewHolder> {

    private Context mContext;
    private OnRecyclerClickListener onRecyclerClickListener;
    private List<SchoolListResultParam.DataBean.ImglistBean> mList;

    private TrainStyleListAdapter trainStyleListAdapter;

    public TrainStyleAdapter(Context context, List<SchoolListResultParam.DataBean.ImglistBean> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.train_style_list, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        Log.e("train", "----->当前路径===" + TrainStyleListActivity.getInstance.getPath() + mList.get(position).getPhoto());

        Glide.with(mContext).
                load(TrainStyleListActivity.getInstance.getPath() + mList.get(position).getPhoto()).
                asBitmap().
                placeholder(R.drawable.def_2).
                into(holder.img_head);


        List<String> details = GsonUtils.GsonToList(mList.get(position).getDetails(), String.class);
        List<String> lists = new ArrayList<>();

        if (details != null && details.size() > 0) {
            for (String detail : details) {
                lists.add(TrainStyleListActivity.getInstance.getPath() + detail);
            }

        }

        trainStyleListAdapter = (new TrainStyleListAdapter(mContext, lists));
        holder.recycler_view_list.setAdapter(trainStyleListAdapter);

        trainStyleListAdapter.setOnRecyclerClickListener(new OnRecyclerClickListener() {
            @Override
            public void onItemListener(int position2) {

                Intent intent = new Intent(mContext, ImagePageActivity.class);
                intent.putExtra("style_id", mList.get(position).getId());
                mContext.startActivity(intent);

            }

            @Override
            public void onItemMultiListener(int position, boolean isCheck) {

            }
        });

        holder.tv_nickname.setText(mList.get(position).getNickname());
        holder.tv_intro.setText(mList.get(position).getIntro());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public void setOnRecyclerClickListener(OnRecyclerClickListener onRecyclerClickListener) {
        this.onRecyclerClickListener = onRecyclerClickListener;
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        //        private ImageView img_1;
        private ImageView img_head;
        private TextView tv_nickname;
        private TextView tv_intro;
        private RecyclerView recycler_view_list;
        private LinearLayout lin_view;

        public MyViewHolder(View itemView) {
            super(itemView);
//            img_1 = (ImageView) itemView.findViewById(R.id.img_1);
            img_head = (SelectableRoundedImageView) itemView.findViewById(R.id.img_head);
            tv_nickname = (TextView) itemView.findViewById(R.id.tv_nickname);
            tv_intro = (TextView) itemView.findViewById(R.id.tv_intro);
            recycler_view_list = (RecyclerView) itemView.findViewById(R.id.recycler_view_list);
            lin_view = (LinearLayout) itemView.findViewById(R.id.lin_view);

            recycler_view_list.setLayoutManager(new GridLayoutManager(mContext, 3));

            lin_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onRecyclerClickListener != null) {
                        onRecyclerClickListener.onItemListener(getAdapterPosition());
                    }
                }
            });
        }


    }
}
