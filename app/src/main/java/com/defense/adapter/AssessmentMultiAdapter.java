package com.defense.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.listener.OnRecyclerClickListener;
import com.defense.model.AnswerResultParam;

import java.util.List;

/**
 * Created by simon on 17/5/9.
 */

public class AssessmentMultiAdapter extends RecyclerView.Adapter<AssessmentMultiAdapter.ViewHolder> {

    private Context mContext;
    private List<AnswerResultParam.Data.DataBean.OptionListBean> mList;
    private OnRecyclerClickListener onRecyclerClickListener;

    //答对的数量
    private int yes_num = 0;

    //最后提交时是否正确
    private boolean isYes = false;

    //正确的数量
    private int num = 0;
    //选中的数量
    private int select_num = 0;

    public int getYes_num() {
        return yes_num;
    }

    public void setYes_num(int yes_num) {
        this.yes_num = yes_num;
    }

    public int getSelect_num() {
        return select_num;
    }

    public void setSelect_num(int select_num) {
        this.select_num = select_num;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public AssessmentMultiAdapter(Context context, List<AnswerResultParam.Data.DataBean.OptionListBean> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.assessment_list_multi_info, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        AnswerResultParam.Data.DataBean.OptionListBean optionListBean = mList.get(position);
        int sign = optionListBean.getSign();
        if (sign == 1) {
            num++;
        }

        Log.e("sign", "有几道对的题=" + num);

        holder.tv_name.setText(optionListBean.getOption());

        holder.lin_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.cb_select.isChecked()) {
                    holder.cb_select.setChecked(false);
                    if (select_num > 0)
                        select_num--;
                } else {
                    select_num++;
                    holder.cb_select.setChecked(true);
                }

                setSelect_num(select_num);

                if (mList.get(position).getSign() == 1 && holder.cb_select.isChecked()) {
                    yes_num++;
                    setYes_num(yes_num);

                }

                if (onRecyclerClickListener != null)
                    onRecyclerClickListener.onItemMultiListener(position, holder.cb_select.isChecked());
            }
        });

        holder.cb_select.setFocusable(false);
        holder.cb_select.setFocusableInTouchMode(false);

        /*holder.cb_select.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });*/

    }

    private void showImg(ImageView imageView, int id) {
        Glide.with(mContext).
                load(id).
                asBitmap().
                into(imageView);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public void setOnRecyclerClickListener(OnRecyclerClickListener onRecyclerClickListener) {
        this.onRecyclerClickListener = onRecyclerClickListener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private CheckBox cb_select;
        private TextView tv_name;
        private LinearLayout lin_content;

        public ViewHolder(View itemView) {
            super(itemView);
            cb_select = (CheckBox) itemView.findViewById(R.id.cb_select);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            lin_content = (LinearLayout) itemView.findViewById(R.id.lin_content);


        }
    }
}
