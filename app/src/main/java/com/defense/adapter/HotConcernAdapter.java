package com.defense.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.utils.SizeUtils;

/**
 * Created by Administrator on 2017/3/15.
 */

public class HotConcernAdapter extends RecyclerView.Adapter<HotConcernAdapter.ViewHolder> {

    private Context mContext;
    private int width = 0;

    public HotConcernAdapter(Context context) {
        this.mContext = context;

        width = SizeUtils.getWidth(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HotConcernAdapter.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.hot_concern_list_info, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width / 2, width / 3);
        holder.img_head.setLayoutParams(params);
        Glide.with(mContext).
                load("http://h9.86.cc/walls/20160901/1440x900_6ccbee7022b4c05.jpg").
                asBitmap().
                into(holder.img_head);
    }

    @Override
    public int getItemCount() {
        return 15;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_head;
        private TextView tv_name;
        private TextView tv_time;


        public ViewHolder(View itemView) {
            super(itemView);

            img_head = (ImageView) itemView.findViewById(R.id.img_head);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_time = (TextView) itemView.findViewById(R.id.tv_time);

        }
    }
}