package com.defense.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.listener.OnRecyclerClickListener;
import com.defense.model.InstructorResultParam;
import com.joooonho.SelectableRoundedImageView;

import java.util.List;

/**
 * Created by Administrator on 2017/3/9.
 * 军训指南-教官列表
 */

public class TrainGuideAdapter extends RecyclerView.Adapter<TrainGuideAdapter.ViewHolder> {

    private Context mContext;

    private List<InstructorResultParam.InstructorData> mList;
    private OnRecyclerClickListener onRecyclerClickListener;

    public TrainGuideAdapter(Context context, List<InstructorResultParam.InstructorData> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.train_guide_list, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Glide.with(mContext).
                load(mList.get(position).getPic()).
                asBitmap().
                placeholder(R.mipmap.def_1).
                into(holder.img_head);

        holder.tv_name.setText(mList.get(position).getName());
    }


    public void setOnRecyclerClickListener(OnRecyclerClickListener onRecyclerClickListener) {
        this.onRecyclerClickListener = onRecyclerClickListener;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private SelectableRoundedImageView img_head;
        private TextView tv_name;
        private LinearLayout lin_content;

        public ViewHolder(View itemView) {
            super(itemView);

            img_head = (SelectableRoundedImageView) itemView.findViewById(R.id.img_head);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            lin_content = (LinearLayout) itemView.findViewById(R.id.lin_content);

            lin_content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onRecyclerClickListener != null) {
                        onRecyclerClickListener.onItemListener(getAdapterPosition());
                    }
                }
            });

        }


    }
}
