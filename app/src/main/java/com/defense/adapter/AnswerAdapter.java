package com.defense.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.listener.OnRecyclerClickListener;
import com.defense.model.AnswerResultParam;

import java.util.List;

/**
 * Created by simon on 17/3/23.
 * 1.	获取文章（国防知识）测试题--列表
 */

public class AnswerAdapter extends BaseAdapter {


    private Context mContext;
    private List<AnswerResultParam.Data.DataBean.OptionListBean> mList;



    public AnswerAdapter(Context context, List<AnswerResultParam.Data.DataBean.OptionListBean> list) {
        this.mContext = context;
        this.mList = list;
    }


    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null){
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.assessment_list_multi_info, null);
            viewHolder.cb_select = (CheckBox) convertView.findViewById(R.id.cb_select);
            viewHolder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
            viewHolder.lin_content = (LinearLayout) convertView.findViewById(R.id.lin_content);

            convertView.setTag(viewHolder);

        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.tv_name.setText(mList.get(position).getOption());




        /*viewHolder.lin_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.cb_select.isChecked()) {
                    viewHolder.cb_select.setChecked(false);
                    if (select_num > 0)
                        select_num--;
                } else {
                    select_num++;
                    viewHolder.cb_select.setChecked(true);
                }

                setSelect_num(select_num);

                if (optionListBean.getSign() == 1 && viewHolder.cb_select.isChecked()) {
                    yes_num++;
                    setYes_num(yes_num);

                }

            }
        });*/

        viewHolder.cb_select.setFocusable(false);
        viewHolder.cb_select.setFocusableInTouchMode(false);
        return convertView;
    }

    private class ViewHolder{
        private CheckBox cb_select;
        private TextView tv_name;
        private LinearLayout lin_content;
    }

    private void showImg(ImageView imageView, int id){
        Glide.with(mContext).
                load(id).
                asBitmap().
                into(imageView);
    }

}
