package com.defense.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.defense.R;
import com.defense.model.ContentListParam;
import com.defense.model.TradeData;

import java.util.List;

/**
 * Created by simon on 17/3/29.]
 * 国防在线-文章详情-1级分类
 */

public class ActicleSpinnerAdapter2 extends BaseAdapter {
    private Context mContext;
    private List<ContentListParam> mList;

    public ActicleSpinnerAdapter2(Context context, List<ContentListParam> list) {
        this.mContext = context;
        this.mList = list;
    }


    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.spinner_list_infp, null);
            viewHolder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tv_title.setText(mList.get(position).getTitle());

        return convertView;
    }

    private class ViewHolder {
        private TextView tv_title;
    }

}
