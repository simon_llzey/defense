package com.defense.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.listener.OnRecyclerClickListener;
import com.defense.view.TrainStyleListActivity;

import java.util.List;

/**
 * Created by simon on 2018/5/14.
 * 朋友圈展示列表
 */

public class TrainStyleListAdapter extends RecyclerView.Adapter<TrainStyleListAdapter.MyViewHolder> {

    private Context mContext;
    private OnRecyclerClickListener onRecyclerClickListener;
    private List<String> mList;

    public TrainStyleListAdapter(Context context, List<String> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.train_style_list_2, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        Glide.with(mContext).
                load( mList.get(position)).
                asBitmap().
                placeholder(R.drawable.def_2).
                into(holder.img_head);

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public void setOnRecyclerClickListener(OnRecyclerClickListener onRecyclerClickListener) {
        this.onRecyclerClickListener = onRecyclerClickListener;
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        //        private ImageView img_1;
        private ImageView img_head;
        private LinearLayout lin_view;

        public MyViewHolder(View itemView) {
            super(itemView);
            img_head = (ImageView) itemView.findViewById(R.id.img_list);
            lin_view = (LinearLayout) itemView.findViewById(R.id.lin_view);
            lin_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onRecyclerClickListener != null) {
                        onRecyclerClickListener.onItemListener(getAdapterPosition());
                    }
                }
            });
        }


    }
}
