package com.defense;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.defense.custom.snapscrollview.ProductContentPage;
import com.defense.custom.snapscrollview.ProductDetailInfoPage;
import com.defense.custom.snapscrollview.SnapPageLayout;
import com.defense.utils.PermisUtils;
import com.defense.view.MainFragment;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

public class MainActivity extends Activity {

    @ViewInject(R.id.flipLayout)
    private SnapPageLayout snapPageLayout;
    private ProductContentPage bottomPage = null;
    private ProductDetailInfoPage topPage = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        x.view().inject(this);
        PermisUtils.verifyStoragePermissions(this);

        View topView = LayoutInflater.from(this).inflate(R.layout.snap_layout_view, null);
        View bottomView = LayoutInflater.from(this).inflate(R.layout.snap_layout_view_bottom, null);
        snapPageLayout = (SnapPageLayout) findViewById(R.id.flipLayout);
        topPage = new ProductDetailInfoPage(this, topView);
        bottomPage = new ProductContentPage(this, bottomView);
        snapPageLayout.setSnapPages(topPage, bottomPage);

        startActivity(new Intent(MainActivity.this, MainFragment.class));
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        boolean writeAccepted = false;
        switch (requestCode) {
            case 1:
                writeAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (writeAccepted) {

                }
                break;
        }
    }

}
