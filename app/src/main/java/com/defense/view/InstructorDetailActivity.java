package com.defense.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.adapter.CommentAdapter;
import com.defense.custom.CustomDialog;
import com.defense.custom.MyListView;
import com.defense.custom.OverallApplication;
import com.defense.http.request_new.RequestManager;
import com.defense.model.CommentParam;
import com.defense.model.InstructorDetailParam;
import com.defense.model.PraiseParam;
import com.defense.model.SaveCommentParam;
import com.defense.model.TradeRequestParam;
import com.defense.utils.IntentUtils;
import com.defense.utils.LoginUtils;
import com.defense.utils.StatusBarCompat;
import com.defense.utils.ToastUtils;
import com.defense.utils.WebViewUtils;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 教官详情
 */
public class InstructorDetailActivity extends Activity implements View.OnClickListener {

    @ViewInject(R.id.lin_back)
    private LinearLayout lin_back;

    @ViewInject(R.id.tv_name)
    private TextView tv_name;
    @ViewInject(R.id.tv_intro)
    private TextView tv_intro;
    @ViewInject(R.id.web_content)
    private WebView web_content;
    @ViewInject(R.id.img_head)
    private ImageView img_head;

    @ViewInject(R.id.ed_comment)
    private EditText ed_comment;
    @ViewInject(R.id.lin_like)
    private LinearLayout lin_like;
    @ViewInject(R.id.instructor_scrollview)
    private PullToRefreshScrollView instructor_scrollview;

    //点赞列表
    @ViewInject(R.id.tv_praise)
    private TextView tv_praise;
    private List<PraiseParam.PraiseData> praiseDatas = new ArrayList<PraiseParam.PraiseData>();

    //评论列表
    @ViewInject(R.id.lv_comment)
    private MyListView lv_comment;
    private List<CommentParam.DataBean> comments = new ArrayList<CommentParam.DataBean>();
    private CommentAdapter commentAdapter;

    //教官详情
    private InstructorDetailParam.DataBean.ContentBean contentBean;
    //
    private InstructorDetailParam.DataBean dataBean;
    //教官id
    private int cid = -1;
    //评论者id
    private int pid = -1;

    private Dialog dialog;

    //记录是否被赞
    private boolean isPraise = false;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {

                case 0:
                    dataBean = (InstructorDetailParam.DataBean) msg.obj;
                    if (dataBean != null) {

                        Log.e("instructor", "是否被赞=" + dataBean.isPraiseSign());
                        contentBean = dataBean.getContent();
                        if (contentBean != null) {
                            tv_name.setText(contentBean.getTitle());
                            tv_intro.setText(contentBean.getIntro());
                            Glide.with(InstructorDetailActivity.this).
                                    load(contentBean.getImage()).
                                    asBitmap().
                                    placeholder(R.drawable.def_1).
                                    into(img_head);
                            WebViewUtils.optionHtmlWebView(contentBean.getDetails(), web_content);
                        }
                    }

                    break;

                case 1:
                    String praises = "";
                    int len = 0;
                    if (praiseDatas.size() > 0) {
                        for (PraiseParam.PraiseData data : praiseDatas) {
                            len++;
                            if (len == praiseDatas.size()) {
                                praises += data.getNick_name() + "";
                            } else {
                                praises += data.getNick_name() + "、";
                            }
                        }
                        tv_praise.setText(praises);
                    }
                    break;

                case 2:
                    if (comments.size() > 0) {
                        commentAdapter.notifyDataSetChanged();
                        lv_comment.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                showReply(comments.get(position).getMember_id());
                            }
                        });
                    }
                    break;
            }


        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_instructor_detail);
        x.view().inject(this);
        StatusBarCompat.translucentStatusBar(this);
        cid = getIntent().getIntExtra("id", -1);
        dialog = CustomDialog.createLoadingDialog(InstructorDetailActivity.this);
        dialog.show();

        instructor_scrollview.setMode(PullToRefreshBase.Mode.BOTH);
        instructor_scrollview.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ScrollView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ScrollView> refreshView) {
                getCommentList(cid, false);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ScrollView> refreshView) {
                getCommentList(cid, true);
            }
        });


        commentAdapter = new CommentAdapter(InstructorDetailActivity.this, comments);
        lv_comment.setAdapter(commentAdapter);

        init();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    private void init() {

        if (cid > -1) {
            getData(cid);
            getPraiseList(cid);
            getCommentList(cid, false);
        } else {
            ToastUtils.show("加载失败");
            if (dialog != null)
                dialog.dismiss();
        }

        /*ed_comment.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER){
                    String result = ed_comment.getText().toString();

                    ToastUtils.show(result);

                }
                return false;
            }
        });*/
        ed_comment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {//EditorInfo.IME_ACTION_SEARCH、EditorInfo.IME_ACTION_SEND等分别对应EditText的imeOptions属性
                    //TODO回车键按下时要执行的操作
                    String result = ed_comment.getText().toString();
                    if (!TextUtils.isEmpty(result) && contentBean != null) {
                        if (LoginUtils.isLogin(InstructorDetailActivity.this)) {
                            saveComment(result, contentBean.getId(), pid);
                        }
                    } else {
                        ToastUtils.show("请填写评论内容");
                    }

                }
                return false;
            }
        });
        lin_back.setOnClickListener(InstructorDetailActivity.this);
        lin_like.setOnClickListener(InstructorDetailActivity.this);

    }


    public void showReply(int id) {
        this.pid = id;
        ed_comment.setFocusable(true);
        ed_comment.requestFocus();

        InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        im.toggleSoftInput(0, InputMethodManager.SHOW_FORCED);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_back:
                InputMethodManager imm = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                InstructorDetailActivity.this.finish();
                break;
            case R.id.lin_like:
                if (LoginUtils.isLogin(InstructorDetailActivity.this)) {
                    if (dataBean != null) {
                        if (!dataBean.isPraiseSign()) {
                            if (!isPraise) {
                                setLike(contentBean.getId());
                            } else {
                                ToastUtils.show("您已经赞过");
                            }

                        } else {
                            ToastUtils.show("您已经赞过");
                        }
                    }
                }

                break;
        }
    }


    /**
     * 获取教官详情
     *
     * @param id
     */
    private void getData(int id) {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setId(id);
        RequestManager.getInstructorDetail(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                InstructorDetailParam detailParam = (InstructorDetailParam) arg;
                if (detailParam != null && detailParam.isStatus()) {

                    if (detailParam.getData() != null) {
                        InstructorDetailParam.DataBean data = detailParam.getData();
                        if (data != null) {
                            Message message = Message.obtain();
                            message.what = 0;
                            message.obj = data;
                            mHandler.sendMessage(message);

                        }

                    }

                }

                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

    }


    /**
     * 1.	获取点赞列表
     *
     * @param cid
     */
    private void getPraiseList(int cid) {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setcId(cid);
        RequestManager.getPraiseList(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                PraiseParam praiseParam = (PraiseParam) arg;
                if (praiseParam != null && praiseParam.isStatus()) {
                    List<PraiseParam.PraiseData> datas = praiseParam.getData();
                    if (datas != null) {
                        praiseDatas.clear();
                        for (PraiseParam.PraiseData praiseData : datas) {
                            praiseDatas.add(praiseData);
                        }
                        mHandler.sendEmptyMessage(1);
                    }
                }

            }
        });
    }


    /**
     * 5.	获取文章评论列表详情
     *
     * @param cid
     */
    private void getCommentList(int cid, final boolean isLoad) {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setcId(cid);
        int offset = 0;
        if (isLoad) {
            if (comments != null && comments.size() > 0) {
                offset = comments.size();
            }
            requestParam.setPageOffset(offset);
        } else {
            requestParam.setPageOffset(offset);
            comments.clear();
        }


        RequestManager.getCommentList(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                CommentParam commentParam = (CommentParam) arg;
                if (commentParam != null && commentParam.isStatus()) {
                    List<CommentParam.DataBean> dataBeens = commentParam.getData();
                    if (dataBeens != null) {
                        if (dataBeens.size() > 0) {
                            for (CommentParam.DataBean dataBean : dataBeens) {
                                comments.add(dataBean);
                            }
                            mHandler.sendEmptyMessage(2);
                        } else {
                            if (isLoad)
                                ToastUtils.show("没有更多数据");
                            else
                                ToastUtils.show("暂无数据");
                        }
                    }
                }else {
                    ToastUtils.show("暂无数据");
                }

                if (instructor_scrollview != null) {
                    instructor_scrollview.onRefreshComplete();
                }

            }
        });
    }


    /**
     * 保存评论
     *
     * @param detail
     * @param id
     */
    private void saveComment(String detail, final int id, int pid) {
        dialog = CustomDialog.createLoadingDialog(InstructorDetailActivity.this);
        dialog.show();
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setDetails(detail);
        requestParam.setcId(id);
        requestParam.setPid(pid);
        RequestManager.saveComment(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                SaveCommentParam commentParam = (SaveCommentParam) arg;
                if (commentParam != null) {
                    if (commentParam.isStatus()) {
                        ToastUtils.show("评论成功");
                        ed_comment.setText("");
                        getCommentList(id, true);
                    }
                    if (commentParam.getStatus().equals("209")) {
                        ToastUtils.show("请先登录");
                        IntentUtils.jumpToLogin(InstructorDetailActivity.this);
                    }

                } else {
                    ToastUtils.show("评论失败");
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }
        });

    }


    /**
     * 保存点赞
     *
     * @param id
     */
    private void setLike(final int id) {
        dialog = CustomDialog.createLoadingDialog(InstructorDetailActivity.this);
        dialog.show();
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setcId(id);
        RequestManager.savePraise(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                SaveCommentParam commentParam = (SaveCommentParam) arg;
                if (dialog != null) {
                    dialog.dismiss();
                }
                if (commentParam != null) {
                    if (commentParam.isStatus()) {
                        isPraise = true;
                        ToastUtils.show("点赞成功");
                        getPraiseList(cid);
                    }
                    if (commentParam.getStatus().equals("209")) {
                        ToastUtils.show("请先登录");
                        IntentUtils.jumpToLogin(InstructorDetailActivity.this);
                    }
                } else {
                    ToastUtils.show("点赞失败");
                }

            }
        });
    }


}
