package com.defense.view;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.defense.R;
import com.defense.http.request_new.RequestManager;
import com.defense.model.ContentDetailParam;
import com.defense.model.TradeRequestParam;
import com.defense.utils.ToastUtils;
import com.defense.utils.WebViewUtils;
import com.just.agentwebX5.AgentWeb;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.Observable;
import java.util.Observer;

public class HotDetailActivity extends Activity implements View.OnClickListener {
    @ViewInject(R.id.lin_back)
    private LinearLayout lin_back;
    @ViewInject(R.id.tv_title)
    private TextView tv_title;
    @ViewInject(R.id.web_content)
    private WebView web_content;
    @ViewInject(R.id.lin_bottom)
    private LinearLayout lin_bottom;
    @ViewInject(R.id.lin_root)
    private LinearLayout lin_root;

    ContentDetailParam.DataBean dataBean;

    //文章列表id
    private int id = -1;

    private AgentWeb mAgentWebX5;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    dataBean = (ContentDetailParam.DataBean) msg.obj;
                    if (dataBean != null) {
                        if (dataBean.getContent() != null) {
                            tv_title.setText(dataBean.getContent().getTitle());
//                            WebViewUtils.optionHtmlWebView(dataBean.getContent().getDetails(), web_content);
                            mAgentWebX5 = WebViewUtils.optionHtmlWebView(HotDetailActivity.this, dataBean.getContent().getDetails(), lin_root);
                            web_content.setWebViewClient(new MyWebViewClient());

                        }
                    }

                    break;

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_hot_detail);
        x.view().inject(this);

        id = getIntent().getIntExtra("id", -1);
        if (id > -1) {
            getData(id);
        } else {
            ToastUtils.show("文章加载失败");
        }


        lin_back.setOnClickListener(HotDetailActivity.this);
    }


    @Override
    protected void onPause() {
        if (mAgentWebX5 != null)
            mAgentWebX5.getWebLifeCycle().onPause();
        super.onPause();
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            lin_bottom.setVisibility(View.VISIBLE);
        } else {
            lin_bottom.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        if (mAgentWebX5 != null)
            mAgentWebX5.getWebLifeCycle().onResume();
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mAgentWebX5 != null)
            mAgentWebX5.uploadFileResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onDestroy() {
        //mAgentWebX5.destroy();
        if (mAgentWebX5 != null)
            mAgentWebX5.getWebLifeCycle().onDestroy();
        super.onDestroy();
    }

    /**
     * @param id
     */
    private void getData(int id) {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setId(id);
        requestParam.setBrowse(1);
        requestParam.setPraise(0);
        RequestManager.getContentDetail(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {

                ContentDetailParam contentDetailParam = (ContentDetailParam) arg;
                if (contentDetailParam != null && contentDetailParam.isStatus()) {

                    ContentDetailParam.DataBean data = contentDetailParam.getData();
                    if (data != null) {
                        Message message = Message.obtain();
                        message.what = 0;
                        message.obj = data;
                        mHandler.sendMessage(message);
                    }
                }


            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_back:
                HotDetailActivity.this.finish();
                break;
        }
    }


    //Web视图
    private class MyWebViewClient extends WebViewClient {
        public boolean shouldOverrideUrlLoading(WebView view, String url) {


            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            view.loadUrl(fullScreenByJs(url));
        }
    }

    /**
     * Js注入
     *
     * @param url 加载的网页地址
     * @return 注入的js内容，若不是需要适配的网址则返回空javascript
     */
    public static String fullScreenByJs(String url) {
        String refer = referParser(url);
        if (null != refer) {
//            return "javascript:document.getElementsByClassName('" + referParser(url) + "')[0].addEventListener('click',function(){local_obj.playing();return false;});";
            return "javascript:document.getElementsByClassName('" + refer + "')[0].addEventListener('click',function(){onClick.fullscreen();return false;});";
        } else {
            return "javascript:";
        }
    }

    /**
     * 对不同的视频网站分析相应的全屏控件
     *
     * @param url 加载的网页地址
     * @return 相应网站全屏按钮的class标识
     */
    public static String referParser(String url) {
        if (url.contains("letv")) {
            return "hv_ico_screen";               //乐视Tv
        } else if (url.contains("youku")) {
            return "x-zoomin";                    //优酷
        } else if (url.contains("bilibili")) {
            return "icon-widescreen";             //bilibili
        } else if (url.contains("qq")) {
            return "tvp_fullscreen_button";       //腾讯视频
        }

        return null;
    }

}
