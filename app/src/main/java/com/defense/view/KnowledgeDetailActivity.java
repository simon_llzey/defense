package com.defense.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.defense.R;
import com.defense.adapter.ActicleSpinnerAdapter;
import com.defense.adapter.ActicleSpinnerAdapter1;
import com.defense.adapter.ActicleSpinnerAdapter2;
import com.defense.adapter.AnswerAdapter;
import com.defense.custom.MyListView;
import com.defense.http.request_new.RequestManager;
import com.defense.model.AnswerResultParam;
import com.defense.model.ContentDetail;
import com.defense.model.ContentDetailParam;
import com.defense.model.ContentListParam;
import com.defense.model.ContentListResultParam;
import com.defense.model.TradeData;
import com.defense.model.TradeRequestParam;
import com.defense.model.TradeResultParam;
import com.defense.utils.ToastUtils;
import com.defense.utils.WebViewUtils;
import com.defense.view.fragment.TrainAssessmentFrag;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 国防知识详情列表
 */
@ContentView(R.layout.activity_knowledge_detail)
public class KnowledgeDetailActivity extends Activity implements View.OnClickListener {
    //1、2级分类
    @ViewInject(R.id.spinner_1)
    private Spinner spinner_1;//章列表
    @ViewInject(R.id.spinner_2)
    private Spinner spinner_2;//节列表
    @ViewInject(R.id.spinner_3)
    private Spinner spinner_3;//内容列表

    @ViewInject(R.id.lin_back)
    private LinearLayout lin_back;
    @ViewInject(R.id.tv_title)
    private TextView tv_title;
    @ViewInject(R.id.web_content)
    private WebView web_content;

    @ViewInject(R.id.lv_answer)
    private MyListView lv_answer;
    @ViewInject(R.id.tv_answer_title)
    private TextView tv_answer_title;

    @ViewInject(R.id.tv_index)
    private TextView tv_index;

    @ViewInject(R.id.tv_next)
    private TextView tv_next;


    private AnswerAdapter answerAdapter;
    //测试题列表
    private List<AnswerResultParam.Data.DataBean> dataBeanList = new ArrayList<AnswerResultParam.Data.DataBean>();
//    private List<AnswerResultParam.DataBean.OptionListBean> optionList = new ArrayList<AnswerResultParam.DataBean.OptionListBean>();

//    private Dialog dialog;

    //栏目内容id
    private int id = -1;


    //身份列表
    private List<TradeData> tradeDatas1 = new ArrayList<TradeData>();
    private ActicleSpinnerAdapter spinnerAdapter1;

    //栏目内容列表
    private List<TradeData> tradeDatas2 = new ArrayList<TradeData>();
    private ActicleSpinnerAdapter1 spinnerAdapter2;

    private List<ContentListParam> contentLists = new ArrayList<ContentListParam>();
    private ActicleSpinnerAdapter2 spinnerAdapter3;


    private ContentDetail contentDetail = null;


    //记录当前答题的位置和一共多少题
    private int current = 0;
    private int totalRecord = 0;

    //记录答对的题数
    private int yes = 0;
    private boolean isYes = false;

    //记录是否选中答案
    private boolean isSelect = false;
    //答题列表
    private List<AnswerResultParam.Data.DataBean.OptionListBean> answers = null;


    //答对的数量
    private int yes_num = 0;
    //选中的数量
    private int select_num = 0;
    //正确的数量
    private int num = 0;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }


    public int getYes_num() {
        return yes_num;
    }

    public void setYes_num(int yes_num) {
        this.yes_num = yes_num;
    }

    public int getSelect_num() {
        return select_num;
    }

    public void setSelect_num(int select_num) {
        this.select_num = select_num;
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 0:
                    if (dataBeanList != null)
                        dataBeanList.clear();

                    contentDetail = (ContentDetail) msg.obj;
                    if (contentDetail != null) {
                        tv_title.setText(contentDetail.getTitle());
                        getAnswer(contentDetail.getId());
                        WebViewUtils.optionHtmlWebView(contentDetail.getDetails(), web_content);
                    }
                    break;

                case 1:
                    if ((current + 1) == totalRecord) {
                        tv_next.setText("提交");
                    }

                    num = 0;
                    select_num = 0;
                    yes_num = 0;

                    if (dataBeanList != null && dataBeanList.size() > 0) {
                        AnswerResultParam.Data.DataBean dataBean = dataBeanList.get(current);
                        if (dataBean != null) {
                            tv_index.setText("您已经做了" + (current + 1) + "道题了，" + (current + 1) + "/" + totalRecord + "次");
                            tv_answer_title.setText(dataBean.getQuestion());
                            answers = dataBean.getOption_list();
                            Collections.shuffle(answers);
                            if (answers != null) {
                                answerAdapter = new AnswerAdapter(KnowledgeDetailActivity.this, answers);
                                lv_answer.setAdapter(answerAdapter);

                                for (int y = 0; y < answers.size(); y++) {
                                    int sign = answers.get(y).getSign();
                                    if (sign == 1) {
                                        num++;
                                    }
                                }

                                lv_answer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        int sign = answers.get(position).getSign();
                                        CheckBox cb_select = (CheckBox) view.findViewById(R.id.cb_select);
                                        if (cb_select.isChecked()) {
                                            cb_select.setChecked(false);
                                            if (select_num > 0)
                                                select_num--;
                                        } else {
                                            select_num++;
                                            cb_select.setChecked(true);
                                        }

                                        setSelect_num(select_num);

                                        if (sign == 1 && cb_select.isChecked()) {
                                            yes_num++;
                                            setYes_num(yes_num);
                                        }
                                    }
                                });
                            }
                        }
                    }
                    break;

                case 2:
                    if (tradeDatas1 != null && tradeDatas1.size() > 0) {
                        spinnerAdapter1.notifyDataSetChanged();
                    } else {
                        spinnerAdapter1.notifyDataSetChanged();
                    }
                    break;

                case 3:

                    if (tradeDatas2 != null && tradeDatas2.size() > 0) {
                        spinnerAdapter2.notifyDataSetChanged();
                    } else {
                        spinnerAdapter2.notifyDataSetChanged();
                        ToastUtils.show("没有数据");
                    }
                    break;

                case 4:
                    if (contentLists != null && contentLists.size() > 0) {
                        spinnerAdapter3.notifyDataSetChanged();

                    } else {
                        spinnerAdapter3.notifyDataSetChanged();
                        ToastUtils.show("没有数据");
                    }
                    break;


            }


        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        x.view().inject(this);


        init();

    }

    private void init() {
        //上级id
        id = getIntent().getIntExtra("id", -1);


        spinnerAdapter1 = new ActicleSpinnerAdapter(KnowledgeDetailActivity.this, tradeDatas1);
        spinner_1.setAdapter(spinnerAdapter1);

        spinnerAdapter2 = new ActicleSpinnerAdapter1(KnowledgeDetailActivity.this, tradeDatas2);
        spinner_2.setAdapter(spinnerAdapter2);

        spinnerAdapter3 = new ActicleSpinnerAdapter2(KnowledgeDetailActivity.this, contentLists);
        spinner_3.setAdapter(spinnerAdapter3);

        getTradeListData(1, id);

        spinner_1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (tradeDatas1 != null && tradeDatas1.size() > 0) {
                    getTradeListData(2, tradeDatas1.get(position).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                getContentListData(tradeDatas2.get(position).getId());

//                getDetail(contentLists.get(position).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                getDetail(contentLists.get(position).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        lin_back.setOnClickListener(KnowledgeDetailActivity.this);
        tv_next.setOnClickListener(KnowledgeDetailActivity.this);
    }


    /**
     * 获取内容详情信息
     */
    private void getDetail(int id) {
//        dialog = CustomDialog.createLoadingDialog(ArticleActivity.this);
//        dialog.show();
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setId(id);
        RequestManager.getContentDetail(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                ContentDetailParam detailParam = (ContentDetailParam) arg;
                if (detailParam != null && detailParam.isStatus()) {

                    ContentDetailParam.DataBean dataBean = detailParam.getData();
                    if (dataBean != null) {
                        ContentDetail contentDetail = dataBean.getContent();

                        if (contentDetail != null) {
                            Message message = Message.obtain();
                            message.what = 0;
                            message.obj = contentDetail;
                            mHandler.sendMessage(message);
                        }
                    }
                }
            }
        });
    }


    /**
     * 获取答题
     *
     * @param cid
     */
    private void getAnswer(int cid) {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setcId(cid);

        RequestManager.getDetailAnswer(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                AnswerResultParam resultParam = (AnswerResultParam) arg;
                if (resultParam != null && resultParam.isStatus()) {
                    if (resultParam.getData() != null) {
                        List<AnswerResultParam.Data.DataBean> dataBeens = resultParam.getData().getData();
                        if (dataBeens != null) {
                            totalRecord = dataBeens.size();
                            for (AnswerResultParam.Data.DataBean dataBean : dataBeens) {
                                dataBeanList.add(dataBean);
                            }

                        }
                    }
                }

                mHandler.sendEmptyMessage(1);
            }
        });
    }

    /**
     * 栏目内容列表接口
     */
    public void getContentListData(int id) {

        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setModule(id);
        RequestManager.getContentList(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                ContentListResultParam resultParam = (ContentListResultParam) arg;

                contentLists.clear();
                if (resultParam != null && resultParam.isStatus()) {
                    List<ContentListParam> lists = resultParam.getData();
                    if (lists != null && lists.size() > 0) {
                        for (ContentListParam contentListParam : lists) {
                            contentLists.add(contentListParam);
                        }
                    }
                }
                mHandler.sendEmptyMessage(4);

            }
        });
    }

    /**
     * 获取登陆后按用户身份特定栏目子列表
     */
    private void getTradeListData(final int index, int id) {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setPid(id);

        RequestManager.getTradeListData(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                TradeResultParam resultParam = (TradeResultParam) arg;
                if (resultParam != null) {
                    List<TradeData> datas = resultParam.getData();
                    if (datas != null) {
                        if (tradeDatas1 != null) {
                            tradeDatas1.clear();
                        }

                        if (tradeDatas2 != null) {
                            tradeDatas2.clear();
                        }

                        for (TradeData data : datas) {
                            if (index == 1) {
                                tradeDatas1.add(data);
                            } else {
                                tradeDatas2.add(data);
                            }

                        }
                        if (index == 1) {
                            mHandler.sendEmptyMessage(2);
                        } else {
                            mHandler.sendEmptyMessage(3);
                        }

                    }
                }
            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_back:
                KnowledgeDetailActivity.this.finish();
                break;
            case R.id.tv_next:

                if (getSelect_num() > 0) {

                    if (getSelect_num() == getNum() && getYes_num() == getNum()) {
                        yes++;
                    } else {
                    }

//                    Log.e("exam", "选中数=" + getSelect_num() + "，正确数=" + getNum() + ",yes_num=" + getYes_num());
                    current++;
                    if (current < totalRecord) {
                        mHandler.sendEmptyMessage(1);
                    } else {
                        jump();
                    }

                } else {
                    ToastUtils.show("请选择答案");
                    return;
                }
                break;
        }
    }

    private void jump() {
        Intent intent = new Intent(KnowledgeDetailActivity.this, PostAnswerActivity.class);
        intent.putExtra("yes", yes);
        intent.putExtra("totalRecord", totalRecord);
        intent.putExtra("title", TrainAssessmentFrag.title_simulation);
        startActivity(intent);
        KnowledgeDetailActivity.this.finish();

    }
}
