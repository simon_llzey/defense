package com.defense.view.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.defense.R;
import com.defense.adapter.AssessmentAdapter;
import com.defense.custom.CustomDialog;
import com.defense.custom.OverallApplication;
import com.defense.http.request_new.RequestManager;
import com.defense.listener.OnRecyclerClickListener;
import com.defense.model.AnswerResultParam;
import com.defense.model.LoginResultParam;
import com.defense.model.RequestParam;
import com.defense.model.TradeRequestParam;
import com.defense.model.UserInfo;
import com.defense.utils.IntentUtils;
import com.defense.utils.ToastUtils;
import com.defense.view.ExamMultiselectActivity;
import com.defense.view.PostAnswerActivity;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 军事理论考核
 */
@ContentView(R.layout.fragment_train_assessment)
public class TrainAssessmentFrag extends Fragment implements View.OnClickListener {
    @ViewInject(R.id.train_recyc_view)
    private RecyclerView recyclerView;
    private AssessmentAdapter adapter;
    @ViewInject(R.id.assessment_title)
    private TextView assessment_title;
    private List<AnswerResultParam.Data.DataBean> dataBeanList = new ArrayList<AnswerResultParam.Data.DataBean>();

    @ViewInject(R.id.tv_next)
    private TextView tv_next;
    @ViewInject(R.id.tv_index)
    private TextView tv_index;

    @ViewInject(R.id.tv_simulation)
    private TextView tv_simulation;
    @ViewInject(R.id.lin_formal)
    private LinearLayout lin_formal;
    @ViewInject(R.id.lin_content)
    private LinearLayout lin_content;
    @ViewInject(R.id.lin_select)
    private LinearLayout lin_select;

    private List<AnswerResultParam.Data.DataBean.OptionListBean> answers = null;


    private LoginResultParam.UserData.InfoBean userInfo;

    private Dialog dialog;

    //记录当前答题的位置和一共多少题
    private int current = 0;
    private int totalRecord = 0;

    public static final String title_formal = "正式考试";
    public static final String title_simulation = "模拟考试";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return x.view().inject(this, inflater, container);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }


    private void init() {
        if (OverallApplication.userData != null) {
            userInfo = OverallApplication.userData.getInfo();
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        tv_next.setOnClickListener(TrainAssessmentFrag.this);
        tv_simulation.setOnClickListener(TrainAssessmentFrag.this);
        lin_formal.setOnClickListener(TrainAssessmentFrag.this);


    }


    /**
     * 获取答题列表
     */
    private void getData() {
        RequestParam requestParam = new RequestParam();

        dialog = CustomDialog.createLoadingDialog(getActivity());
        dialog.show();

        RequestManager.getTheoryList(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                AnswerResultParam resultParam = (AnswerResultParam) arg;

                if (resultParam != null && resultParam.isStatus()) {

                    if (resultParam.getData() != null) {
                        List<AnswerResultParam.Data.DataBean> dataBeens = resultParam.getData().getData();
                        if (dataBeens != null) {
                            totalRecord = dataBeens.size();
                            for (AnswerResultParam.Data.DataBean dataBean : dataBeens) {
                                dataBeanList.add(dataBean);
                            }
                        }

                    }
                }

                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_simulation:
                jump(title_simulation);
                break;

            case R.id.lin_formal:
                if (userInfo != null) {
                    if (userInfo.getNumber() >=2)
                    {
                        ToastUtils.show("您已经考完" + userInfo.getNumber() + "次");
                        return;
                    }

                }
                jump(title_formal);
                break;
        }
    }

    private void jump(String title) {
//        Intent intent = new Intent(getActivity(), ExamActivity.class);
        Intent intent = new Intent(getActivity(), ExamMultiselectActivity.class);
        intent.putExtra("title", title);
        startActivity(intent);
    }

}
