package com.defense.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.adapter.TrainGuideAdapter;
import com.defense.http.request_new.RequestManager;
import com.defense.listener.OnRecyclerClickListener;
import com.defense.model.InstructorResultParam;
import com.defense.model.TradeRequestParam;
import com.defense.model.TrainGuideParam;
import com.defense.view.HotDetailActivity;
import com.defense.view.InstructorDetailActivity;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 军事指南
 */
@ContentView(R.layout.fragment_train_guide)
public class TrainGuideFrag extends Fragment implements View.OnClickListener {

    @ViewInject(R.id.rv_guide)
    private RecyclerView rv_guide;
    private StaggeredGridLayoutManager layoutManager;
    private TrainGuideAdapter guideAdapter;
    //教官列表
    private List<InstructorResultParam.InstructorData> instructorDatas = new ArrayList<InstructorResultParam.InstructorData>();


    //管理规定
    private List<TrainGuideParam.DataBean> guideDatas = new ArrayList<TrainGuideParam.DataBean>();
    @ViewInject(R.id.img_rule)
    private ImageView img_rule;
    @ViewInject(R.id.tv_rule)
    private TextView tv_rule;

    //军训指南-军训常识
    private List<TrainGuideParam.DataBean> guideDatas2 = new ArrayList<TrainGuideParam.DataBean>();
    @ViewInject(R.id.img_common_sense)
    private ImageView img_common_sense;
    @ViewInject(R.id.tv_common_sense)
    private TextView tv_common_sense;

    @ViewInject(R.id.lin_rule)
    private LinearLayout lin_rule;
    @ViewInject(R.id.lin_sense)
    private LinearLayout lin_sense;


    private boolean isLoadView = false;


    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    guideAdapter = new TrainGuideAdapter(getActivity(), instructorDatas);
                    rv_guide.setAdapter(guideAdapter);

                    guideAdapter.setOnRecyclerClickListener(new OnRecyclerClickListener() {
                        @Override
                        public void onItemListener(int position) {
                            Intent instuctorIntent = new Intent(getActivity(), InstructorDetailActivity.class);
                            instuctorIntent.putExtra("id", instructorDatas.get(position).getContent_id());
                            startActivity(instuctorIntent);
                        }

                        @Override
                        public void onItemMultiListener(int position, boolean isCheck) {

                        }
                    });
                    break;

                case 1:
                    //管理规定

                    if (guideDatas.size() > 0) {

                        TrainGuideParam.DataBean dataBean = guideDatas.get(0);
                        Glide.with(getActivity()).
                                load(dataBean.getImage()).
                                asBitmap().
                                placeholder(R.mipmap.def_1).
                                into(img_rule);

                        tv_rule.setText(dataBean.getIntro());
                    }

                    break;

                case 2:
                    //军训常识
                    if (guideDatas2.size() > 0) {

                        TrainGuideParam.DataBean dataBean = guideDatas2.get(0);
                        Glide.with(getActivity()).
                                load(dataBean.getImage()).
                                asBitmap().
                                placeholder(R.mipmap.def_1).
                                into(img_common_sense);

                        tv_common_sense.setText(dataBean.getIntro());
                    }
                    break;
            }

        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return x.view().inject(this, inflater, container);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //如果view被创建过，数据还在直接更新view

        init();

        isLoadView = true;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void init() {

        lin_rule.setOnClickListener(TrainGuideFrag.this);
        lin_sense.setOnClickListener(TrainGuideFrag.this);

        rv_guide.setHasFixedSize(true);
        int spanCount = 1; // 只显示一行
        layoutManager = new StaggeredGridLayoutManager(spanCount, StaggeredGridLayoutManager.HORIZONTAL);
        rv_guide.setLayoutManager(layoutManager);

        if (instructorDatas.size() > 0 || guideDatas.size() > 0 || guideDatas2.size() > 0) {
            if (instructorDatas != null && instructorDatas.size() > 0) {
                mHandler.sendEmptyMessage(0);
            }
            if (guideDatas != null && guideDatas.size() > 0) {
                mHandler.sendEmptyMessage(1);
            }
            if (guideDatas2 != null && guideDatas2.size() > 0) {
                mHandler.sendEmptyMessage(2);
            }


            return;
        }

        getInstructorList();

        getGuide1(30);
        getGuide2(31);


    }


    /**
     * 获取教官列表
     */
    private void getInstructorList() {

        RequestManager.getInstructorList(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                InstructorResultParam resultParam = (InstructorResultParam) arg;
                if (resultParam != null) {
                    List<InstructorResultParam.InstructorData> datas = resultParam.getData();
                    if (datas != null) {
                        instructorDatas.clear();
                        for (InstructorResultParam.InstructorData data : datas) {
                            instructorDatas.add(data);
                        }
                        mHandler.sendEmptyMessage(0);
                    }
                }

            }
        });
    }


    /**
     * 军训指南-管理规定
     *
     * @param module
     */
    private void getGuide1(int module) {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setModule(module);
        RequestManager.getTrainGuide_1(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                TrainGuideParam guideParam = (TrainGuideParam) arg;
                if (guideParam != null && guideParam.isStatus()) {

                    if (guideParam.getData() != null) {

                        List<TrainGuideParam.DataBean> dataBeans = guideParam.getData();
                        if (dataBeans != null) {
                            guideDatas.clear();
                            for (TrainGuideParam.DataBean dataBean : dataBeans) {
                                guideDatas.add(dataBean);
                            }

                            mHandler.sendEmptyMessage(1);
                        }
                    }
                }

            }
        });
    }

    /**
     * 军训指南-军训常识
     *
     * @param module
     */
    private void getGuide2(int module) {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setModule(module);
        RequestManager.getTrainGuide_1(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                TrainGuideParam guideParam = (TrainGuideParam) arg;
                if (guideParam != null && guideParam.isStatus()) {

                    if (guideParam.getData() != null) {

                        List<TrainGuideParam.DataBean> dataBeans = guideParam.getData();
                        if (dataBeans != null) {
                            guideDatas2.clear();
                            for (TrainGuideParam.DataBean dataBean : dataBeans) {
                                guideDatas2.add(dataBean);
                            }
                            mHandler.sendEmptyMessage(2);

                        }
                    }
                }

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_rule:
                if (guideDatas != null && guideDatas.size() > 0) {
                    Intent detailIntent = new Intent(getActivity(), HotDetailActivity.class);
                    detailIntent.putExtra("id", guideDatas.get(0).getId());
                    startActivity(detailIntent);
                }
                break;
            case R.id.lin_sense:
                if (guideDatas2 != null && guideDatas2.size() > 0) {
                    Intent senseIntent = new Intent(getActivity(), HotDetailActivity.class);
                    senseIntent.putExtra("id", guideDatas2.get(0).getId());
                    startActivity(senseIntent);
                }
                break;
        }
    }
}
