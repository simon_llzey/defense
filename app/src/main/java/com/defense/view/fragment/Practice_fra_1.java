package com.defense.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.defense.R;
import com.defense.adapter.PracticeAdapter;
import com.defense.custom.BannerImageLoader;
import com.defense.custom.MyGridView;
import com.defense.custom.MyListView;
import com.defense.http.request_new.RequestManager;
import com.defense.model.ContentListParam;
import com.defense.model.ContentListResultParam;
import com.defense.model.TradeRequestParam;
import com.defense.utils.ToastUtils;
import com.defense.view.HotDetailActivity;
import com.defense.view.PracticeListActivity;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.listener.OnBannerListener;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 军事实践
 */
@ContentView(R.layout.fragment_practice_fra_1)
public class Practice_fra_1 extends Fragment {

    @ViewInject(R.id.practice_scrollview)
    private PullToRefreshScrollView pullToRefreshScrollView;
    @ViewInject(R.id.practice_banner)
    private Banner banner;
    private List<String> paths = new ArrayList<String>();
    private List<String> titleList = new ArrayList<String>();
    private List<ContentListParam> banners = new ArrayList<ContentListParam>();

    @ViewInject(R.id.practice_grid)
    private MyGridView practice_grid;

    private int module = -1;

    //底部数据列表
    private List<ContentListParam> contents = new ArrayList<ContentListParam>();
    private PracticeAdapter practiceAdapter;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    if (banners != null && banners.size() > 0) {
                        for (ContentListParam listParam : banners) {
                            paths.add(listParam.getImage());
                            titleList.add(listParam.getIntro());
                        }
                        banner.setImages(paths);
                        banner.setBannerTitles(titleList);
                        banner.setBannerStyle(BannerConfig.NUM_INDICATOR_TITLE);
                        banner.setDelayTime(5000);
                        banner.setImageLoader(new BannerImageLoader());
                        banner.setOnBannerListener(new OnBannerListener() {
                            @Override
                            public void OnBannerClick(int position) {
                                /*Toast.makeText(getActivity(), "第几个=" + position, Toast.LENGTH_SHORT).show();*/
                                if (banners.size() <= 0) {
                                    return;
                                }
                                Intent detailIntent = new Intent(getActivity(), HotDetailActivity.class);
                                detailIntent.putExtra("id", banners.get(position).getId());
                                startActivity(detailIntent);
                            }
                        });
                        banner.start();
                    }
                    break;
                case 1:
                    practiceAdapter.notifyDataSetChanged();
                    practice_grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent listIntent = new Intent(getActivity(), PracticeListActivity.class);
                            startActivity(listIntent);
                        }
                    });
                    break;

            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return x.view().inject(this, inflater, container);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        module = getArguments().getInt("module");
        init();
    }

    private void init() {

        pullToRefreshScrollView.setMode(PullToRefreshBase.Mode.BOTH);

        pullToRefreshScrollView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ScrollView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ScrollView> refreshView) {
                paths.clear();
                titleList.clear();
                if (module > -1) {
                    getData(module, 0, false);
                    getData(module, 1, false);
                }
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ScrollView> refreshView) {
                if (module > -1) {
                    getData(module, 0, true);
                }
            }
        });

        practiceAdapter = new PracticeAdapter(getActivity(), contents);
        practice_grid.setAdapter(practiceAdapter);
        practice_grid.setFocusable(false);

        if (banners.size() > 0 || contents.size() > 0) {
            if (banners.size() > 0) {
                paths.clear();
                titleList.clear();

                mHandler.sendEmptyMessage(0);
            }
            if (contents.size() > 0) {
                mHandler.sendEmptyMessage(1);
            }
            return;
        }


        if (module > -1) {
            getData(module, 1, false);
            getData(module, 0, false);
        }
    }

    /**
     * @param module
     * @param home   0=是底部数据,1是banner
     */
    private void getData(int module, final int home, final boolean isLoad) {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setModule(module);
        requestParam.setHome(home);

        int offset = 0;
        if (isLoad) {
            if (contents != null && contents.size() > 0) {
                offset = contents.size();
            }
            requestParam.setPageOffset(offset);
        } else {
            requestParam.setPageOffset(offset);
            contents.clear();
            banners.clear();
        }


        RequestManager.getContentList(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                ContentListResultParam resultParam = (ContentListResultParam) arg;
                if (resultParam != null && resultParam.isStatus()) {

                    List<ContentListParam> listParams = resultParam.getData();

                    if (home == 1) {
                        if (listParams != null && listParams.size() > 0) {
                            for (ContentListParam listParam : listParams) {
                                banners.add(listParam);
                            }
                            mHandler.sendEmptyMessage(0);
                        }
                    } else {

                        if (listParams != null && listParams.size() > 0) {
                            for (ContentListParam listParam : listParams) {
                                contents.add(listParam);
                            }
                            mHandler.sendEmptyMessage(1);
                        } else {
                            if (isLoad)
                                ToastUtils.show("没有更多数据");
                        }

                    }
                }


                if (pullToRefreshScrollView != null) {
                    pullToRefreshScrollView.onRefreshComplete();
                }


            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();

        if (banner != null) {
            banner.stopAutoPlay();
        }
    }
}
