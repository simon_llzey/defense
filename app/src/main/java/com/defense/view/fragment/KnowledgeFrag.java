package com.defense.view.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.defense.R;
import com.defense.adapter.ActicleSpinnerAdapter;
import com.defense.adapter.KnowledgeCategoryAdapter;
import com.defense.adapter.KnowledgeListAdapter;
import com.defense.custom.BannerImageLoader;
import com.defense.custom.CustomDialog;
import com.defense.custom.LineGridView;
import com.defense.custom.OverallApplication;
import com.defense.custom.PopupUtils;
import com.defense.http.UserController;
import com.defense.http.request_new.RequestManager;
import com.defense.model.CategoryInfo;
import com.defense.model.ContentListParam;
import com.defense.model.ContentListResultParam;
import com.defense.model.DemoInfo;
import com.defense.model.TradeData;
import com.defense.model.TradeRequestParam;
import com.defense.model.TradeResultParam;
import com.defense.utils.ToastUtils;
import com.defense.view.ArticleActivity;
import com.defense.view.HotDetailActivity;
import com.defense.view.KnowledListActivity;
import com.defense.view.KnowledgeDetailActivity;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.listener.OnBannerListener;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 国防知识
 */
@ContentView(R.layout.fragment_knowledge)
public class KnowledgeFrag extends Fragment implements View.OnClickListener {

    public static KnowledgeFrag getInstance;

    @ViewInject(R.id.banner)
    private Banner banner;
    private List<String> paths = new ArrayList<String>();
    private List<ContentListParam> banners = new ArrayList<ContentListParam>();
    private List<String> titleList = new ArrayList<String>();

    @ViewInject(R.id.gv_list)
    private GridView gv_list;
    private KnowledgeCategoryAdapter categoryAdapter;
    //身份列表
    private List<TradeData> tradeDatas_logout = new ArrayList<TradeData>();
    @ViewInject(R.id.lin_logout)
    private LinearLayout lin_logout;


    //登录后的
    @ViewInject(R.id.fl_login)
    private FrameLayout fl_login;
    @ViewInject(R.id.gv_line)
    private LineGridView gv_line;
    private List<TradeData> contentLists = new ArrayList<TradeData>();
    private List<TradeData> tradeDatas = new ArrayList<TradeData>();


    @ViewInject(R.id.knowledge_spinner)
    private Spinner knowledge_spinner;
    private ActicleSpinnerAdapter acticleSpinnerAdapter;

    private KnowledgeListAdapter listAdapter;


    private Dialog dialog;

    //国防知识id
    private static final int MODULE_ID = 1;

    public static String basePath = "";


    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    //登录后的栏目列表

                    if (tradeDatas != null && tradeDatas.size() > 0) {
                        acticleSpinnerAdapter.notifyDataSetChanged();
                    }
                    break;
                case 1:
                    //未登录时栏目列表
                    if (tradeDatas_logout != null && tradeDatas_logout.size() > 0)
                        categoryAdapter.notifyDataSetChanged();
                    break;

                case 2:
                    if (contentLists != null && contentLists.size() > 0) {
                        listAdapter.notifyDataSetChanged();
                    } else {
                        ToastUtils.show("暂无数据");
                        listAdapter.notifyDataSetChanged();
                    }
                    break;

                case 3:
                    if (paths != null) {
                        paths.clear();
                    }
                    if (titleList != null) {
                        titleList.clear();
                    }
                    if (banners != null && banners.size() > 0) {
                        for (ContentListParam listParam : banners) {
                            paths.add(listParam.getImage());
                            titleList.add(listParam.getIntro());
                        }

                        banner.setImages(paths);
                        banner.setBannerTitles(titleList);
                        banner.setBannerStyle(BannerConfig.NUM_INDICATOR_TITLE);
                        banner.setDelayTime(5000);
                        banner.setImageLoader(new BannerImageLoader());
                        banner.setOnBannerListener(new OnBannerListener() {
                            @Override
                            public void OnBannerClick(int position) {
                                Intent detailIntent = new Intent(getActivity(), HotDetailActivity.class);
                                detailIntent.putExtra("id", banners.get(position).getId());
                                startActivity(detailIntent);
                            }
                        });
                        banner.start();
                    }
                    break;

            }

        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return x.view().inject(this, inflater, container);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getInstance = this;
        getBanner(1);
    }

    public void init() {
        //登录后和未登录的界面
        if (OverallApplication.userData != null) {
            init_login();

        } else {
            init_logout();
        }

    }

    /**
     * 已登录时
     */
    private void init_login() {
        lin_logout.setVisibility(View.GONE);
        fl_login.setVisibility(View.VISIBLE);


        listAdapter = new KnowledgeListAdapter(getActivity(), contentLists);
        gv_line.setAdapter(listAdapter);


        acticleSpinnerAdapter = new ActicleSpinnerAdapter(getActivity(), tradeDatas);
        knowledge_spinner.setAdapter(acticleSpinnerAdapter);


        knowledge_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                dialog = CustomDialog.createLoadingDialog(getActivity());
                dialog.show();

                if (tradeDatas != null && tradeDatas.size() > 0) {
                    getContentListData(tradeDatas.get(position).getId());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        getTradeListData();


    }

    /**
     * 未登录时
     */
    private void init_logout() {
        lin_logout.setVisibility(View.VISIBLE);
        fl_login.setVisibility(View.GONE);
        categoryAdapter = new KnowledgeCategoryAdapter(getActivity(), tradeDatas_logout);
        gv_list.setAdapter(categoryAdapter);
        gv_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent detailIntent = new Intent(getActivity(), KnowledListActivity.class);
                detailIntent.putExtra("id", tradeDatas_logout.get(position).getId());
                startActivity(detailIntent);
            }
        });

        getListData();
    }


    /**
     * 获取未登录时的栏目列表
     * 0为未登录
     */
    private void getListData() {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setPid(MODULE_ID);

        RequestManager.getTradeListData(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                TradeResultParam resultParam = (TradeResultParam) arg;
                if (resultParam != null) {
                    basePath = resultParam.getBasepath();
                    List<TradeData> datas = resultParam.getData();
                    if (datas != null) {

                        if (tradeDatas_logout != null) {
                            tradeDatas_logout.clear();
                        }

                        for (TradeData data : datas) {
                            tradeDatas_logout.add(data);
                        }
                        mHandler.sendEmptyMessage(1);

                    }
                }
            }
        });
    }

    /**
     * 获取登陆后按用户身份特定栏目子列表
     */
    private void getTradeListData() {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setPid(MODULE_ID);

        RequestManager.getTradeListData(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                TradeResultParam resultParam = (TradeResultParam) arg;
                if (resultParam != null) {
                    basePath = resultParam.getBasepath();
                    List<TradeData> datas = resultParam.getData();
                    if (datas != null) {
                        if (tradeDatas != null) {
                            tradeDatas.clear();
                        }
                        for (TradeData data : datas) {
                            tradeDatas.add(data);
                        }
                        mHandler.sendEmptyMessage(0);
                    }
                }

            }
        });

    }


    /**
     * 获取章节列表
     *
     * @param id
     */
    public void getContentListData(int id) {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setPid(id);

        RequestManager.getTradeListData(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                TradeResultParam resultParam = (TradeResultParam) arg;
                if (resultParam != null) {
                    List<TradeData> datas = resultParam.getData();
                    if (datas != null) {
                        contentLists.clear();
                        for (TradeData data : datas) {
                            contentLists.add(data);
                        }
                        mHandler.sendEmptyMessage(2);
                    }
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }
        });

    }


    /**
     * 获取banner
     *
     * @param home 1：获取banner
     */
    private void getBanner(final int home) {

        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setModule(MODULE_ID);
        requestParam.setHome(home);
        RequestManager.getContentList(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                ContentListResultParam resultParam = (ContentListResultParam) arg;

                if (resultParam != null && resultParam.isStatus()) {
                    if (banners != null) {
                        banners.clear();
                    }

                    List<ContentListParam> lists = resultParam.getData();
                    if (lists != null && lists.size() > 0) {
                        for (ContentListParam listParam : lists) {
                            banners.add(listParam);
                        }

                    }
                }
                mHandler.sendEmptyMessage(3);

            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();
        init();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (banner != null)
            banner.stopAutoPlay();
    }


    @Override
    public void onClick(View v) {

    }


}
