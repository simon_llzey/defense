package com.defense.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.defense.R;
import com.defense.adapter.HotConcernGridAdapter;
import com.defense.custom.BannerImageLoader;
import com.defense.custom.MyGridView;
import com.defense.http.request_new.RequestManager;
import com.defense.model.ContentListParam;
import com.defense.model.ContentListResultParam;
import com.defense.model.TradeRequestParam;
import com.defense.utils.ToastUtils;
import com.defense.view.HotDetailActivity;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.listener.OnBannerListener;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 热点关注
 */
@ContentView(R.layout.fragment_hot_concern)
public class HotConcernFrag extends Fragment {

    @ViewInject(R.id.hot_scrollview)
    private PullToRefreshScrollView hot_scrollview;

    @ViewInject(R.id.hot_banner)
    private Banner banner;
    private List<String> paths = new ArrayList<String>();
    private List<String> titleList = new ArrayList<String>();

    @ViewInject(R.id.hot_grid_view)
    private MyGridView hot_grid_view;

    private List<ContentListParam> banners = new ArrayList<ContentListParam>();
    private List<ContentListParam> contents = new ArrayList<ContentListParam>();
    private HotConcernGridAdapter concernGridAdapter;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    if (banners != null && banners.size() > 0) {
                        for (ContentListParam listParam : banners) {
                            paths.add(listParam.getImage());
                            titleList.add(listParam.getTitle());
                        }
                        banner.setImages(paths);
                        banner.setBannerTitles(titleList);
                        banner.setBannerStyle(BannerConfig.NUM_INDICATOR_TITLE);
                        banner.setDelayTime(5000);
                        banner.setImageLoader(new BannerImageLoader());
                        banner.setOnBannerListener(new OnBannerListener() {
                            @Override
                            public void OnBannerClick(int position) {
                                if (banners.size()<=0){
                                    return;
                                }
                                Intent detailIntent = new Intent(getActivity(), HotDetailActivity.class);
                                detailIntent.putExtra("id", banners.get(position).getId());
                                startActivity(detailIntent);
                            }
                        });
                        banner.start();
                    }
                    break;


                case 1:
                    concernGridAdapter.notifyDataSetChanged();
                    break;
            }
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return x.view().inject(this, inflater, container);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {

        hot_scrollview.setMode(PullToRefreshBase.Mode.BOTH);

        concernGridAdapter = new HotConcernGridAdapter(getActivity(), contents);

        hot_grid_view.setAdapter(concernGridAdapter);
        hot_grid_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent detailIntent = new Intent(getActivity(), HotDetailActivity.class);
                detailIntent.putExtra("id", contents.get(position).getId());
                startActivity(detailIntent);
            }
        });
        hot_grid_view.setFocusable(false);

        hot_scrollview.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ScrollView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ScrollView> refreshView) {
                paths.clear();
                titleList.clear();
                getHotBanner(4, 0, false);
                getHotBanner(4, 1, false);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ScrollView> refreshView) {
                getHotBanner(4, 0, true);
            }
        });


        getHotBanner(4, 0, false);
        getHotBanner(4, 1, false);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (banner != null) {
            banner.startAutoPlay();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (banner != null)
            banner.stopAutoPlay();
    }


    /**
     * @param module 1 热点banner, 0 内容列表
     */
    private void getHotBanner(final int module, final int home, final boolean isLoad) {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setModule(module);
        requestParam.setHome(home);


        int offset = 0;
        if (isLoad) {
            if (contents != null && contents.size() > 0) {
                offset = contents.size();
            }
            requestParam.setPageOffset(offset);
        } else {
            requestParam.setPageOffset(offset);
            contents.clear();
            banners.clear();
        }


        RequestManager.getContentList(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                ContentListResultParam resultParam = (ContentListResultParam) arg;
                if (resultParam != null && resultParam.isStatus()) {

                    List<ContentListParam> listParams = resultParam.getData();
                    if (listParams != null) {
                        if (home == 1) {
                            for (ContentListParam listParam : listParams) {
                                banners.add(listParam);
                            }
                            mHandler.sendEmptyMessage(0);
                        } else {
                            if (listParams.size() > 0) {
                                for (ContentListParam listParam : listParams) {
                                    contents.add(listParam);
                                }

                                mHandler.sendEmptyMessage(1);
                            } else {
                                if (isLoad)
                                    ToastUtils.show("没有更多数据");
                                else
                                    ToastUtils.show("暂无数据");
                            }
                        }

                    } else {
                        ToastUtils.show("加载失败");
                    }
                }


                if (hot_scrollview != null) {
                    hot_scrollview.onRefreshComplete();
                }

            }
        });

    }

}
