package com.defense.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.defense.R;
import com.defense.adapter.ClassRoomAdapter;
import com.defense.custom.DividerItem;
import com.defense.custom.PullToRefreshRecyclerView;
import com.defense.http.request_new.RequestManager;
import com.defense.listener.OnRecyclerClickListener;
import com.defense.model.ContentListParam;
import com.defense.model.ContentListResultParam;
import com.defense.model.TradeRequestParam;
import com.defense.utils.LoginUtils;
import com.defense.utils.ToastUtils;
import com.defense.view.ClassRoomDetailActivity;
import com.defense.view.HotDetailActivity;
import com.handmark.pulltorefresh.library.PullToRefreshBase;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 国防课堂
 */
@ContentView(R.layout.fragment_classroom)
public class ClassroomFrag extends Fragment {

    @ViewInject(R.id.pull_recyc_view)
    private PullToRefreshRecyclerView pull_recyc_view;
    private RecyclerView recyclerView;
    private ClassRoomAdapter roomAdapter;
    private List<ContentListParam> contentLists = new ArrayList<ContentListParam>();


    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    if (contentLists != null && contentLists.size() > 0) {
                        roomAdapter.notifyDataSetChanged();
                    }
                    break;

            }

        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return x.view().inject(this, inflater, container);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void init() {

        pull_recyc_view.setMode(PullToRefreshBase.Mode.BOTH);
        recyclerView = pull_recyc_view.getRefreshableView();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//        recyclerView.addItemDecoration(new DividerItem(getActivity(), DividerItem.VERTICAL_LIST));

        roomAdapter = new ClassRoomAdapter(getActivity(), contentLists);
        recyclerView.setAdapter(roomAdapter);

        roomAdapter.setClickListener(new OnRecyclerClickListener() {
            @Override
            public void onItemListener(int position) {
                Intent detailIntent = new Intent(getActivity(), ClassRoomDetailActivity.class);
                detailIntent.putExtra("id", contentLists.get(position).getId());
                startActivity(detailIntent);
            }

            @Override
            public void onItemMultiListener(int position, boolean isCheck) {

            }

        });


        pull_recyc_view.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<RecyclerView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<RecyclerView> refreshView) {
                getData(false);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<RecyclerView> refreshView) {
                getData(true);
            }
        });

        getData(false);
    }


    /**
     * 获取课堂列表
     */
    private void getData(final boolean isLoad) {

        final TradeRequestParam requestParam = new TradeRequestParam();
        int offset = 0;
        if (isLoad) {
            if (contentLists != null && contentLists.size() > 0) {
                offset = contentLists.size();
            }
            requestParam.setPageOffset(offset);
        } else {
            requestParam.setPageOffset(offset);
            contentLists.clear();
        }

        requestParam.setModule(2);
        RequestManager.getContentList(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                ContentListResultParam resultParam = (ContentListResultParam) arg;
                if (resultParam != null && resultParam.isStatus()) {

                    List<ContentListParam> list = resultParam.getData();

                    if (list != null && list.size() > 0) {
                        for (ContentListParam param : list) {
                            contentLists.add(param);
                        }
                        mHandler.sendEmptyMessage(0);
                    } else {
                        if (isLoad)
                            ToastUtils.show("没有更多数据");
                        else
                            ToastUtils.show("暂无数据");
                    }

                }

                if (pull_recyc_view != null) {
                    pull_recyc_view.onRefreshComplete();
                }

            }
        });

    }



}
