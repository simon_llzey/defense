package com.defense.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.defense.R;
import com.defense.adapter.TrainTitleAdapter;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

/**
 * 军事训练
 */
@ContentView(R.layout.fragment_train)
public class TrainFrag extends Fragment {

    @ViewInject(R.id.train_tab_view)
    private TabLayout tab_view;
    @ViewInject(R.id.train_vp_view)
    private ViewPager vp_view;

    private TrainTitleAdapter adapter;

    private List<Fragment> fragments = new ArrayList<Fragment>();
    private List<String> titles = new ArrayList<String>();
    private String[] names = {"军事计划", "军训指南", "军训风采", "理论考核"};
    private Fragment fra_train_plan, fra_train_guide, fra_train_style, fra_train_assessment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return x.view().inject(this, inflater, container);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void init() {
        tab_view.setTabMode(TabLayout.MODE_FIXED);

        fra_train_plan = new TrainPlanFrag();
        fra_train_guide = new TrainGuideFrag();
        fra_train_style = new TrainStyleFrag();
        fra_train_assessment = new TrainAssessmentFrag();


        fragments.add(fra_train_plan);
        fragments.add(fra_train_guide);
        fragments.add(fra_train_style);
        fragments.add(fra_train_assessment);

        for (String s : names) {
            titles.add(s);
        }

        adapter = new TrainTitleAdapter(getActivity(), getActivity().getSupportFragmentManager(), fragments, titles);
        vp_view.setAdapter(adapter);
        tab_view.setupWithViewPager(vp_view);
    }

}
