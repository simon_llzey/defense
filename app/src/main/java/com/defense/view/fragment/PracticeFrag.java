package com.defense.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.defense.R;
import com.defense.adapter.PracticeTitleAdapter;
import com.defense.adapter.TrainTitleAdapter;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

/**
 * 社会实践
 */
@ContentView(R.layout.fragment_practice)
public class PracticeFrag extends Fragment {

    @ViewInject(R.id.practice_tab)
    private TabLayout practice_tab;
    @ViewInject(R.id.practice_vp)
    private ViewPager practice_vp;
    private PracticeTitleAdapter adapter;

    private List<Fragment> fragments = new ArrayList<Fragment>();
    private List<String> titles = new ArrayList<String>();
    private String[] names = {"军事实践", "素质拓展", "校园安全"};


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return x.view().inject(this, inflater, container);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void init() {
        practice_tab.setTabMode(TabLayout.MODE_FIXED);

        for (String s : names) {
            Practice_fra_1 fra_1 = new Practice_fra_1();
            titles.add(s);
            fragments.add(fra_1);
        }

        adapter = new PracticeTitleAdapter(getActivity(), getActivity().getSupportFragmentManager(), fragments, titles);
        practice_vp.setAdapter(adapter);
        practice_tab.setupWithViewPager(practice_vp);
    }


}
