package com.defense.view.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.defense.R;
import com.defense.adapter.PracticeAdapter;
import com.defense.custom.BannerImageLoader;
import com.defense.custom.MyGridView;
import com.defense.custom.PullToRefreshRecyclerView;
import com.defense.http.request_new.RequestManager;
import com.defense.model.ContentListParam;
import com.defense.model.ContentListResultParam;
import com.defense.model.TradeRequestParam;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.listener.OnBannerListener;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 素质拓展
 */
@ContentView(R.layout.fragment_practice_fra_2)
public class Practice_fra_2 extends Fragment {

    @ViewInject(R.id.practice_scrollview_2)
    private PullToRefreshScrollView pullToRefreshScrollView;
    @ViewInject(R.id.practice_banner_2)
    private Banner banner;
    private List<String> paths = new ArrayList<String>();
    private List<String> titleList = new ArrayList<String>();
    private List<ContentListParam> banners = new ArrayList<ContentListParam>();

    @ViewInject(R.id.practice_grid_2)
    private MyGridView practice_grid;

    //底部数据列表
    private List<ContentListParam> contents = new ArrayList<ContentListParam>();
    private PracticeAdapter practiceAdapter;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    if (banners != null && banners.size() > 0) {
                        for (ContentListParam listParam : banners) {
                            paths.add(listParam.getImage());
                            titleList.add(listParam.getIntro());
                        }
                        banner.setImages(paths);
                        banner.setBannerTitles(titleList);
                        banner.setBannerStyle(BannerConfig.NUM_INDICATOR_TITLE);
                        banner.setDelayTime(5000);
                        banner.setImageLoader(new BannerImageLoader());
                        banner.setOnBannerListener(new OnBannerListener() {
                            @Override
                            public void OnBannerClick(int position) {
                                Toast.makeText(getActivity(), "第几个=" + position, Toast.LENGTH_SHORT).show();
                            }
                        });
                        banner.start();
                    }
                    break;
                case 1:
                    practiceAdapter.notifyDataSetChanged();
                    break;

            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return x.view().inject(this, inflater, container);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();
    }

    private void init() {
        practiceAdapter = new PracticeAdapter(getActivity(), contents);
        practice_grid.setAdapter(practiceAdapter);

        getData(50, 1);
        getData(50, 0);
    }

    /**
     * @param module
     * @param home   0=是底部数据,1是banner
     */
    private void getData(int module, final int home) {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setModule(module);
        requestParam.setHome(home);
        RequestManager.getContentList(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                ContentListResultParam resultParam = (ContentListResultParam) arg;
                if (resultParam != null && resultParam.isStatus()) {

                    List<ContentListParam> listParams = resultParam.getData();
                    if (listParams != null) {
                        if (home == 1) {
                            for (ContentListParam listParam : listParams) {
                                banners.add(listParam);
                            }
                            mHandler.sendEmptyMessage(0);
                        } else {
                            for (ContentListParam listParam : listParams) {
                                contents.add(listParam);
                            }
                            mHandler.sendEmptyMessage(1);
                        }

                    }
                }


            }
        });
    }

}

