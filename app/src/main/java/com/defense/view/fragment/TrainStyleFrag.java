package com.defense.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.defense.R;
import com.defense.adapter.SchoolListAdapter;
import com.defense.custom.BannerImageLoader;
import com.defense.custom.MyGridView;
import com.defense.http.request_new.RequestManager;
import com.defense.model.ContentListParam;
import com.defense.model.ContentListResultParam;
import com.defense.model.SchoolResultParam;
import com.defense.model.TradeRequestParam;
import com.defense.utils.ToastUtils;
import com.defense.view.HotDetailActivity;
import com.defense.view.TrainStyleListActivity;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.listener.OnBannerListener;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 军事风采
 */
@ContentView(R.layout.fragment_train_style)
public class TrainStyleFrag extends Fragment {


    @ViewInject(R.id.style_scrollview)
    private PullToRefreshScrollView style_scrollview;

    @ViewInject(R.id.style_banner)
    private Banner banner;
    private List<String> paths = new ArrayList<String>();
    private List<String> titleList = new ArrayList<String>();

    @ViewInject(R.id.style_grid_view)
    private MyGridView hot_grid_view;

    private List<ContentListParam> banners = new ArrayList<ContentListParam>();
    private SchoolListAdapter schoolListAdapter;

    private List<SchoolResultParam.SchoolBean.ImglistBean> contents = new ArrayList<SchoolResultParam.SchoolBean.ImglistBean>();

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    if (banners != null && banners.size() > 0) {
                        for (ContentListParam listParam : banners) {
                            paths.add(listParam.getImage());
                            titleList.add(listParam.getTitle());
                        }
                        banner.setImages(paths);
                        banner.setBannerTitles(titleList);
                        banner.setBannerStyle(BannerConfig.NUM_INDICATOR_TITLE);
                        banner.setDelayTime(5000);
                        banner.setImageLoader(new BannerImageLoader());
                        banner.setOnBannerListener(new OnBannerListener() {
                            @Override
                            public void OnBannerClick(int position) {
                                Intent detailIntent = new Intent(getActivity(), HotDetailActivity.class);
                                detailIntent.putExtra("id", banners.get(position).getId());
                                startActivity(detailIntent);
                            }
                        });
                        banner.start();
                    }
                    break;

                case 1:
                    schoolListAdapter.notifyDataSetChanged();
                    break;
            }
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return x.view().inject(this, inflater, container);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        init();
    }

    private void init() {
        style_scrollview.setMode(PullToRefreshBase.Mode.PULL_FROM_START);

        schoolListAdapter = new SchoolListAdapter(getActivity(), contents);

        hot_grid_view.setAdapter(schoolListAdapter);
        hot_grid_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent detailIntent = new Intent(getActivity(), TrainStyleListActivity.class);
                detailIntent.putExtra("id", contents.get(position).getId());
                detailIntent.putExtra("name", contents.get(position).getName());
                startActivity(detailIntent);
            }
        });
        hot_grid_view.setFocusable(false);

        style_scrollview.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ScrollView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ScrollView> refreshView) {
                paths.clear();
                titleList.clear();
                getHotBanner(22, 1, false);
                getSchoolList();
            }
        });


        if (contents.size() > 0 || banners.size() > 0) {
            if (banners.size() > 0) {
                paths.clear();
                titleList.clear();
                mHandler.sendEmptyMessage(0);
            }
            if (contents.size() > 0) {
                mHandler.sendEmptyMessage(1);
            }
            return;
        }

        getHotBanner(22, 1, false);
        getSchoolList();

    }

    @Override
    public void onResume() {
        super.onResume();
        if (banner != null) {
            banner.startAutoPlay();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (banner != null)
            banner.stopAutoPlay();
    }


    /**
     * @param module 1 热点banner, 0 内容列表
     */
    private void getHotBanner(final int module, final int home, final boolean isLoad) {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setModule(module);
        requestParam.setHome(home);


        int offset = 0;
        if (isLoad) {
            if (contents != null && contents.size() > 0) {
                offset = contents.size();
            }
            requestParam.setPageOffset(offset);
        } else {
            requestParam.setPageOffset(offset);
            contents.clear();
            banners.clear();
        }


        RequestManager.getContentList(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                ContentListResultParam resultParam = (ContentListResultParam) arg;
                if (resultParam != null && resultParam.isStatus()) {

                    List<ContentListParam> listParams = resultParam.getData();
                    if (listParams != null) {
                        if (home == 1) {
                            for (ContentListParam listParam : listParams) {
                                banners.add(listParam);
                            }
                            mHandler.sendEmptyMessage(0);
                        }
                    } else {
                        ToastUtils.show("暂无数据");
                    }
                }


                if (style_scrollview != null) {
                    style_scrollview.onRefreshComplete();
                }

            }
        });

    }


    /**
     * 获取学校列表
     */
    private void getSchoolList() {
        RequestManager.getSchool(new Observer() {
            @Override
            public void update(Observable observable, Object o) {
                SchoolResultParam resultParam = (SchoolResultParam) o;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {
                        SchoolResultParam.SchoolBean schoolBean = resultParam.getData();
                        if (schoolBean != null) {
                            if (schoolBean.getImglist() != null) {
                                List<SchoolResultParam.SchoolBean.ImglistBean> imglist = schoolBean.getImglist();
                                if (contents != null) {
                                    contents.clear();
                                }
                                for (SchoolResultParam.SchoolBean.ImglistBean imglistBean : imglist) {
                                    SchoolResultParam.SchoolBean.ImglistBean bean = new SchoolResultParam.SchoolBean.ImglistBean();
                                    bean.setId(imglistBean.getId());
                                    bean.setLogo(schoolBean.getBasepath() + imglistBean.getLogo());
                                    bean.setName(imglistBean.getName());
                                    bean.setTrade_id(imglistBean.getTrade_id());
                                    bean.setTrade_name(imglistBean.getTrade_name());
                                    contents.add(bean);
                                }
                            }
                        }
                    }
                }


                mHandler.sendEmptyMessage(1);
            }
        });
    }

}
