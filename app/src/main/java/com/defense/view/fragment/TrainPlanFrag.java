package com.defense.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.custom.OverallApplication;
import com.defense.http.request_new.RequestManager;
import com.defense.http.util.GsonUtils;
import com.defense.model.Course;
import com.defense.model.ScheduleResultParam;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;


/**
 * 军事计划
 */
@ContentView(R.layout.fragment_train_plan)
public class TrainPlanFrag extends Fragment {
    LinearLayout weekPanels[] = new LinearLayout[7];
    List courseData[] = new ArrayList[7];
    int itemHeight;
    int marTop, marLeft;

    @ViewInject(R.id.tv_room)
    private TextView tv_room;
    @ViewInject(R.id.pull_refresh_scrollview)
    private PullToRefreshScrollView pull_refresh_scrollview;

    private View mView;

    private List<List<ScheduleResultParam.DataBean.ListBean>> dataList = new ArrayList<List<ScheduleResultParam.DataBean.ListBean>>();

//    private List<List<ScheduleResultParam.DataBean.ListBean>> dataAlls = new ArrayList<List<ScheduleResultParam.DataBean.ListBean>>();

    //记录8节课的id
    private int[] ids = new int[8];

    private int classrooms[][] = new int[7][8];


    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    if (dataList != null && dataList.size() > 0) {
                        getDataList(dataList);
                    }
                    break;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return x.view().inject(this, inflater, container);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mView = view;
        pull_refresh_scrollview.setMode(PullToRefreshBase.Mode.PULL_FROM_START);
        pull_refresh_scrollview.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ScrollView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ScrollView> refreshView) {
                if (OverallApplication.userData != null) {
                    dataList.clear();
                    getData();
                }
            }
        });

        if (OverallApplication.userData != null) {
            init();
        }
    }


    private void init() {


        //
        int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        tv_room.measure(w, h);
        int height = tv_room.getMeasuredHeight();
        itemHeight = getResources().getDimensionPixelSize(R.dimen.weekItemHeight);
//        itemHeight = dip2px(MainActivity.this, height);


        marTop = 0;
        marLeft = getResources().getDimensionPixelSize(R.dimen.weekItemMarLeft);

        if (dataList != null && dataList.size() > 0) {
            for (int i = 0; i < dataList.size(); i++) {
                if (courseData[i] != null)
                    courseData[i].clear();
            }

            mHandler.sendEmptyMessage(0);
            return;
        }
        //数据
        getData();


    }


    public void getDataList(List<List<ScheduleResultParam.DataBean.ListBean>> lists) {
        for (int i = 0; i < lists.size(); i++) {
            List<ScheduleResultParam.DataBean.ListBean> titles = lists.get(i);
            List<Course> list1 = new ArrayList<Course>();
            if (courseData[i] != null) {
                courseData[i].clear();
            }
            for (int z = 0; z < titles.size(); z++) {

                ScheduleResultParam.DataBean.ListBean title = titles.get(z);
                if (title != null) {
                    String name = title.getName();
                    int id = title.getId();
                    if (!TextUtils.isEmpty(name)) {
                        Course c1 = new Course(id, name, title.getIcon(), (z + 1), title.getNum());
                        list1.add(c1);
                        courseData[i] = list1;
                    }
                }

            }
        }

        for (int i = 0; i < weekPanels.length; i++) {
            weekPanels[i] = (LinearLayout) mView.findViewById(R.id.weekPanel_1 + i);
            initWeekPanel(weekPanels[i], courseData[i]);
        }
    }

    public void initWeekPanel(LinearLayout ll, List<Course> data) {
        ll.removeAllViews();
        if (ll == null || data == null || data.size() < 1) return;
        Course pre = data.get(0);
//        for (int z = 0; z < length; z++) {
        for (int i = 0; i < data.size(); i++) {
            Course c = data.get(i);
            TextView tv = new TextView(getActivity());
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.FILL_PARENT,
                    itemHeight * c.getStep() + marTop * (c.getStep() - 1));
            if (i > 0) {
                lp.setMargins(marLeft, (c.getStart() - (pre.getStart() + pre.getStep())) * (itemHeight + marTop) + marTop, 0, 0);
            } else {
                lp.setMargins(marLeft, (c.getStart() - 1) * (itemHeight + marTop) + marTop, 0, 0);
            }
            tv.setLayoutParams(lp);
            tv.setGravity(Gravity.CENTER);
            tv.setTextSize(12);
            tv.setTextColor(getResources().getColor(R.color.courseTextColor));
            tv.setText(c.getName());

            String z = c.getId() + "";

            if (z.contains("1")) {
                tv.setBackgroundResource(R.drawable.color1);
            } else if (z.contains("2")) {
                tv.setBackgroundResource(R.drawable.color2);
            } else if (z.contains("3")) {
                tv.setBackgroundResource(R.drawable.color3);
            } else if (z.contains("4")) {
                tv.setBackgroundResource(R.drawable.color4);
            } else if (z.contains("5")) {
                tv.setBackgroundResource(R.drawable.color5);
            } else if (z.contains("6")) {
                tv.setBackgroundResource(R.drawable.color6);
            } else if (z.contains("7")) {
                tv.setBackgroundResource(R.drawable.color7);
            } else if (z.contains("8")) {
                tv.setBackgroundResource(R.drawable.color8);
            } else if (z.contains("9")) {
                tv.setBackgroundResource(R.drawable.color9);
            } else if (z.contains("10")) {
                tv.setBackgroundResource(R.drawable.color10);
            }
            ll.addView(tv);
            pre = c;
        }
//        }
    }


    /**
     * 获取军事计划列表
     */
    private void getData() {

        RequestManager.getSchedule(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                ScheduleResultParam resultParam = (ScheduleResultParam) arg;
                if (resultParam != null && resultParam.isStatus()) {
                    if (resultParam.getData() != null) {
                        List<List<ScheduleResultParam.DataBean.ListBean>> datas = resultParam.getData().getList();
                        getList(datas);
                        if (datas != null && datas.size() > 0) {
                            dataList.clear();
                            for (List<ScheduleResultParam.DataBean.ListBean> list : datas) {
                                dataList.add(list);
                            }
                            mHandler.sendEmptyMessage(0);
                        }

                    }

                }

                pull_refresh_scrollview.onRefreshComplete();

            }
        });
    }


    /**
     * 记录每天的课
     *
     * @param datas
     */
    private void getList(List<List<ScheduleResultParam.DataBean.ListBean>> datas) {

        if (datas != null) {

            for (int i = 0; i < datas.size(); i++) {
                List<ScheduleResultParam.DataBean.ListBean> listBeens = datas.get(i);
                List<ScheduleResultParam.DataBean.ListBean> temps = new ArrayList<ScheduleResultParam.DataBean.ListBean>();
                ScheduleResultParam.DataBean.ListBean temp = null;
                for (int z = 0; z < listBeens.size(); z++) {
                    ScheduleResultParam.DataBean.ListBean listBean = listBeens.get(z);
                    int len = 1;
                    for (int x = (z + 1); x < listBeens.size(); x++) {
                        if (listBean.getId() > 0) {
                            if (listBean.getId() == listBeens.get(x).getId()) {
                                len++;
                                z = x;
                            } else {
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                    listBean.setNum(len);
                    temps.add(listBean);
                }
                dataList.add(temps);
            }
            mHandler.sendEmptyMessage(0);
        }
    }

}
