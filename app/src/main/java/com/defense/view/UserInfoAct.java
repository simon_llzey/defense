package com.defense.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.custom.OverallApplication;
import com.defense.http.UserController;
import com.defense.http.model.ResultParam;
import com.defense.http.request_new.RequestManager;
import com.defense.model.LoginResultParam;
import com.defense.model.RequestParam;
import com.defense.model.UploadHeadParam;
import com.defense.utils.ImageUtils;
import com.defense.utils.ToastUtils;
import com.joooonho.SelectableRoundedImageView;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.io.ByteArrayOutputStream;
import java.util.Observable;
import java.util.Observer;

/**
 * 用户信息
 */
@ContentView(R.layout.activity_user_info)
public class UserInfoAct extends Activity implements View.OnClickListener {

    @ViewInject(R.id.lin_back)
    private LinearLayout lin_back;
    @ViewInject(R.id.img_head)
    private SelectableRoundedImageView img_head;
    @ViewInject(R.id.ed_name)
    private EditText ed_name;
    @ViewInject(R.id.tv_sure)
    private TextView tv_sure;
    @ViewInject(R.id.fl_head)
    private FrameLayout fl_head;
    @ViewInject(R.id.tv_forget)
    private TextView tv_forget;
    @ViewInject(R.id.ed_info)
    private EditText ed_info;

    private static final int PHOTO_CAMAER = 100;


    private LoginResultParam.UserData.InfoBean userInfo;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:

                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        x.view().inject(this);


        init();
    }

    private void init() {
        if (OverallApplication.userData != null) {

            if (OverallApplication.userData.getInfo() != null) {
                userInfo = OverallApplication.userData.getInfo();
                ed_name.setHint(userInfo.getNick_name());

                Glide.with(UserInfoAct.this).
                        load(userInfo.getPic()).
                        asBitmap().
                        placeholder(R.mipmap.def_1).
                        into(img_head);
            }

            if (OverallApplication.userData.getSchoolInfo() != null) {
                LoginResultParam.UserData.SchoolInfoBean schoolInfoBean = OverallApplication.userData.getSchoolInfo();
                ed_info.setText(schoolInfoBean.getSchoolName() + schoolInfoBean.getSecSchoolName() + schoolInfoBean.getSpecialtyName());
            }

        }


        fl_head.setOnClickListener(UserInfoAct.this);
        lin_back.setOnClickListener(UserInfoAct.this);
        tv_sure.setOnClickListener(UserInfoAct.this);
        tv_forget.setOnClickListener(UserInfoAct.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fl_head:
                Intent photoIntent = new Intent(UserInfoAct.this, PictureActivity.class);
                startActivityForResult(photoIntent, PHOTO_CAMAER);
                break;
            case R.id.lin_back:
                UserInfoAct.this.finish();
                break;

            case R.id.tv_sure:
                String name = ed_name.getText().toString().trim();
                if (!TextUtils.isEmpty(name)) {
                    setName(name);
                } else {
                    ToastUtils.show("请填写内容");
                }

                break;

            case R.id.tv_forget:
                startActivity(new Intent(UserInfoAct.this, RegisterAct.class).putExtra("form", "forget"));
                break;
        }
    }


    private void setName(final String name) {
        RequestParam requestParam = new RequestParam();
        requestParam.setNickname(name);
        RequestManager.upload_nickname(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                ResultParam resultParam = (ResultParam) arg;
                if (resultParam != null && resultParam.isStatus()) {
                    ToastUtils.show("设置成功");
                    OverallApplication.userData.getInfo().setNick_name(name);
                    UserInfoAct.this.finish();
                } else {
                    ToastUtils.show("设置失败");
                }

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case PHOTO_CAMAER:
                if (resultCode == Activity.RESULT_OK) {
                    if (data == null)
                        break;
                    String path = data.getStringExtra("save_path");
                    uploadImages(path);
                }
                break;
        }
    }


    /**
     * 单张图片上传
     *
     * @param chosePath
     */
    public void uploadImages(final String chosePath) {

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                RequestParam requestParam = new RequestParam();
                try {
                    //上传成功设置头像
                    Bitmap bitmap = BitmapFactory.decodeFile(chosePath);
                    String head = ImageUtils.bitmaptoString(bitmap);
                    requestParam.setHead(head);
                    RequestManager.upload_head(requestParam, new Observer() {
                        @Override
                        public void update(Observable observable, Object o) {
                            UploadHeadParam headParam = (UploadHeadParam) o;
                            if (headParam != null) {
                                UploadHeadParam.UploadData uploadData = headParam.getData();
                                if (uploadData.isSuccess()) {
                                    OverallApplication.userData.getInfo().setPic(uploadData.getHeader());
                                    Toast.makeText(UserInfoAct.this, "上传成功", Toast.LENGTH_SHORT).show();
                                    Glide.with(UserInfoAct.this).
                                            load(uploadData.getHeader()).
                                            asBitmap().
                                            placeholder(R.drawable.def_1).
                                            into(img_head);
                                } else {
                                    Toast.makeText(UserInfoAct.this, "上传失败", Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                Toast.makeText(UserInfoAct.this, "上传失败", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                    Toast.makeText(UserInfoAct.this, "图片太大，请重新选择", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


}
