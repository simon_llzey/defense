package com.defense.view;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.defense.R;
import com.defense.adapter.CommentMoreAdapter;
import com.defense.custom.CustomDialog;
import com.defense.custom.PullToRefreshRecyclerView;
import com.defense.http.request_new.RequestManager;
import com.defense.model.CommentParam;
import com.defense.model.SaveCommentParam;
import com.defense.model.TradeRequestParam;
import com.defense.utils.LoginUtils;
import com.defense.utils.ToastUtils;
import com.handmark.pulltorefresh.library.PullToRefreshBase;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 更多评论
 */
public class CommentActivity extends Activity implements View.OnClickListener {

    @ViewInject(R.id.lin_back)
    private LinearLayout lin_back;

    @ViewInject(R.id.rv_comment)
    private PullToRefreshRecyclerView rv_comment;
    @ViewInject(R.id.ed_comment)
    private EditText ed_comment;

    private RecyclerView recyclerView;
    private CommentMoreAdapter moreAdapter;

    private List<CommentParam.DataBean> comments = new ArrayList<CommentParam.DataBean>();

    //文章id
    private int id = -1;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    moreAdapter.notifyDataSetChanged();
                    break;

            }

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_comment);
        x.view().inject(this);

        id = getIntent().getIntExtra("id", -1);

        rv_comment.setMode(PullToRefreshBase.Mode.BOTH);
        recyclerView = rv_comment.getRefreshableView();
        recyclerView.setLayoutManager(new LinearLayoutManager(CommentActivity.this));


        moreAdapter = new CommentMoreAdapter(this, comments);
        recyclerView.setAdapter(moreAdapter);

        if (id > -1) {
            getData(id, false);
        }

        rv_comment.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<RecyclerView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<RecyclerView> refreshView) {
                getData(id, false);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<RecyclerView> refreshView) {
                getData(id, true);
            }
        });

        ed_comment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {//EditorInfo.IME_ACTION_SEARCH、EditorInfo.IME_ACTION_SEND等分别对应EditText的imeOptions属性
                    //TODO回车键按下时要执行的操作
                    String result = ed_comment.getText().toString();
                    if (!TextUtils.isEmpty(result)) {
                        if (LoginUtils.isLogin(CommentActivity.this)){
                            saveComment(result, id);
                        }

                    } else {
                        ToastUtils.show("请填写评论内容");
                    }

                }
                return false;
            }
        });

        lin_back.setOnClickListener(this);
    }


    /**
     * 获取评论列表
     *
     * @param cid
     */
    private void getData(int cid, final boolean isLoad) {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setcId(cid);
        int offset = 0;
        if (isLoad) {
            if (comments != null && comments.size() > 0) {
                offset = comments.size();
            }
            requestParam.setPageOffset(offset);
        } else {
            requestParam.setPageOffset(0);
            comments.clear();
        }

        RequestManager.getCommentList(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                CommentParam commentParam = (CommentParam) arg;
                if (commentParam != null && commentParam.isStatus()) {

                    List<CommentParam.DataBean> dataBeanList = commentParam.getData();
                    if (dataBeanList != null && dataBeanList.size() > 0) {
                        for (CommentParam.DataBean dataBean : dataBeanList) {
                            comments.add(dataBean);
                        }
                        handler.sendEmptyMessage(0);
                    } else {
                        if (isLoad)
                            ToastUtils.show("没有更多数据");
                        else
                            ToastUtils.show("暂无数据");
                    }
                } else {
                    ToastUtils.show("暂无数据");
                }

                if (rv_comment != null) {
                    rv_comment.onRefreshComplete();
                }

            }
        });

    }


    /**
     * 保存评论
     *
     * @param detail
     * @param id
     */
    private void saveComment(String detail, final int id) {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setDetails(detail);
        requestParam.setcId(id);
        RequestManager.saveComment(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                SaveCommentParam commentParam = (SaveCommentParam) arg;
                if (commentParam != null && commentParam.isStatus()) {
                    ToastUtils.show("评论成功");
                    ed_comment.setText("");
                    getData(id, false);
                } else {
                    ToastUtils.show("评论失败");
                }


            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_back:
                CommentActivity.this.finish();
                break;

        }
    }
}
