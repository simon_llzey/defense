package com.defense.view;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.custom.pinchimageviewpager.Global;
import com.defense.custom.pinchimageviewpager.PinchImageView;
import com.defense.custom.pinchimageviewpager.PinchImageViewPager;
import com.defense.custom.pinchimageviewpager.images.ImageSource;
import com.defense.utils.StatusBarCompat;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.LinkedList;

public class PagerActivity extends Activity {
    @ViewInject(R.id.linlay_bottom)
    private LinearLayout linlay_bottom;
    @ViewInject(R.id.linlay_top)
    private RelativeLayout linlay_top;
    @ViewInject(R.id.linlay_back)
    private LinearLayout linlay_back;
    @ViewInject(R.id.rellay)
    private RelativeLayout rellay;
    @ViewInject(R.id.tv_number)
    private TextView tv_number;
    @ViewInject(R.id.tv_title)
    private TextView tv_title;
    @ViewInject(R.id.tv_intro)
    private TextView tv_intro;
    @ViewInject(R.id.pager)
    private PinchImageViewPager pager;

    private Global global;
    private Boolean isShow = true;
    private LinkedList<PinchImageView> viewCache;

    private final static int SHOW_TIME = 200;


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    tv_number.setText(msg.obj + "/" + global.getTestImagesCount());
                    break;
                case 2:
                    if (isShow) {
                        dismiss();
                    }
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pager);
        x.view().inject(this);
        StatusBarCompat.translucentStatusBar(this);
        global = (Global) getIntent().getSerializableExtra("global");
        initView();
        initData();
        Timer();
    }

    private void initView() {
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) linlay_top.getLayoutParams();
            params.setMargins(params.leftMargin, params.topMargin + + StatusBarCompat.getStatusBarHeight(this), params.rightMargin, params.bottomMargin);// 通过自定义坐标来放置你的控件
            linlay_top.setLayoutParams(params);
        }*/

        tv_title.setText(getIntent().getStringExtra("title"));
        tv_intro.setText(getIntent().getStringExtra("intro"));
        tv_number.setText("1/" + global.getTestImagesCount());
        pager.setOnPageChangeListener(new PinchImageViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                Message msg = new Message();
                msg.what = 1;
                msg.obj = position + 1;
                handler.sendMessage(msg);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        linlay_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private ImageSource image;
    private PinchImageView piv;

    private void initData() {
        viewCache = new LinkedList<PinchImageView>();

        PagerAdapter pagerAdapter = new PagerAdapter() {
            @Override
            public int getCount() {
                return global.getTestImagesCount();
            }

            @Override
            public boolean isViewFromObject(View view, Object o) {
                return view == o;
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
//                PinchImageView piv;
                if (viewCache.size() > 0) {
                    piv = viewCache.remove();
                    piv.reset();
                } else {
                    piv = new PinchImageView(PagerActivity.this);
                }
                image = global.getTestImage(position);
                Glide.with(PagerActivity.this).load(image.getUrl(100, 100)).into(piv);
                piv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isShow) {
                            dismiss();
                        } else {
                            show();
                        }
                    }
                });
                container.addView(piv);
                return piv;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                PinchImageView piv = (PinchImageView) object;
                container.removeView(piv);
                viewCache.add(piv);
            }

            @Override
            public void setPrimaryItem(ViewGroup container, int position, Object object) {
                PinchImageView piv = (PinchImageView) object;
                ImageSource image = global.getTestImage(position);
                Glide.with(PagerActivity.this).load(image.getUrl(100, 100)).into(piv);
                pager.setMainPinchImageView(piv);
            }
        };
        pager.setAdapter(pagerAdapter);


    }

    private Animation top_showAnim;
    private Animation top_endAnim;
    private Animation bottom_showAnim;
    private Animation bottom_endAnim;

    public void show() {
        isShow = true;
        Timer();
        if (top_showAnim == null && top_endAnim == null && bottom_showAnim == null && bottom_endAnim == null) {
            getDelta();
        }
        linlay_top.startAnimation(top_showAnim);
        linlay_bottom.startAnimation(bottom_showAnim);

    }

    public void dismiss() {
        isShow = false;
        if (top_showAnim == null && top_endAnim == null && bottom_showAnim == null && bottom_endAnim == null) {
            getDelta();
        }
        linlay_top.startAnimation(top_endAnim);
        linlay_bottom.startAnimation(bottom_endAnim);
    }

    public void getDelta() {
        Rect rectangle = new Rect();
        getWindow().getDecorView().getWindowVisibleDisplayFrame(rectangle);
        //高度为rectangle.top-0仍为rectangle.top
        Log.e("WangJ", "状态栏-方法3:" + rectangle.top);

        top_showAnim = new TranslateAnimation(0, 0, -(linlay_top.getHeight() + rectangle.top), 0);
        top_showAnim.setFillAfter(true);/* True:图片停在动画结束位置 */
        top_showAnim.setDuration(SHOW_TIME);
        top_endAnim = new TranslateAnimation(0,
                0, 0, -linlay_top.getHeight() - rectangle.top);
        top_endAnim.setFillAfter(true);/* True:图片停在动画结束位置 */
        top_endAnim.setDuration(SHOW_TIME);

        bottom_showAnim = new TranslateAnimation(0, 0, linlay_bottom.getHeight(), 0);
        bottom_showAnim.setFillAfter(true);/* True:图片停在动画结束位置 */
        bottom_showAnim.setDuration(SHOW_TIME);
        bottom_endAnim = new TranslateAnimation(0,
                0, 0, linlay_bottom.getHeight());
        bottom_endAnim.setFillAfter(true);/* True:图片停在动画结束位置 */
        bottom_endAnim.setDuration(SHOW_TIME);
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
// TODO Auto-generated method stub
            handler.sendEmptyMessage(2);
        }
    };

    private void Timer() {
        handler.removeCallbacks(runnable);// 关闭定时器处理
        handler.postDelayed(runnable, 5 * 1000);// 打开定时器，执行操作
    }
}
