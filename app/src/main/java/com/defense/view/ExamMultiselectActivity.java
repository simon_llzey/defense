package com.defense.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.defense.R;
import com.defense.adapter.AssessmentAdapter;
import com.defense.adapter.AssessmentMultiAdapter;
import com.defense.custom.CustomDialog;
import com.defense.custom.OverallApplication;
import com.defense.http.request_new.RequestManager;
import com.defense.listener.OnRecyclerClickListener;
import com.defense.model.AnswerResultParam;
import com.defense.model.LoginResultParam;
import com.defense.model.RequestParam;
import com.defense.utils.ToastUtils;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 答题多选
 */
public class ExamMultiselectActivity extends Activity implements View.OnClickListener {
    @ViewInject(R.id.train_recyc_view)
    private RecyclerView recyclerView;
    private AssessmentMultiAdapter adapter;
    @ViewInject(R.id.assessment_title)
    private TextView assessment_title;
    private List<AnswerResultParam.Data.DataBean> dataBeanList = new ArrayList<AnswerResultParam.Data.DataBean>();

    @ViewInject(R.id.tv_next)
    private TextView tv_next;
    @ViewInject(R.id.tv_index)
    private TextView tv_index;
    @ViewInject(R.id.tv_category)
    private TextView tv_category;
    @ViewInject(R.id.lin_back)
    private LinearLayout lin_back;
    @ViewInject(R.id.tv_second)
    private TextView tv_second;

    private List<AnswerResultParam.Data.DataBean.OptionListBean> answers = null;


    private LoginResultParam.UserData.InfoBean userInfo;

    private Dialog dialog;

    //记录当前答题的位置和一共多少题
    private int current = 0;
    private int totalRecord = 0;

    //记录答对的题数
    private int yes = 0;
    private boolean isYes = false;

    //记录是否选中答案
    private boolean isSelect = false;

    private AlertDialog alertDialog;
    private AlertDialog.Builder builder;

    private String title = "";

    private AnswerResultParam.Data.TradeBean tradeBean = null;
    private TimeCount time = null;//倒计时

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    if ((current + 1) == totalRecord) {
                        tv_next.setText("提交");
                    }

                    if (dataBeanList != null && dataBeanList.size() > 0) {
                        AnswerResultParam.Data.DataBean dataBean = dataBeanList.get(current);
                        if (dataBean != null) {
                            tv_index.setText("您已经做了" + (current + 1) + "道题了，" + (current + 1) + "/" + totalRecord + "次");
                            assessment_title.setText(dataBean.getQuestion());
                            answers = dataBean.getOption_list();
                            Collections.shuffle(answers);
                            if (answers != null) {
                                adapter = new AssessmentMultiAdapter(ExamMultiselectActivity.this, answers);
                                recyclerView.setAdapter(adapter);
                                /*adapter.setOnRecyclerClickListener(new OnRecyclerClickListener() {
                                    @Override
                                    public void onItemListener(int position) {

                                    }

                                    @Override
                                    public void onItemMultiListener(int position, boolean isCheck) {
                                        int sign = answers.get(position).getSign();
                                        if (sign == 1 && isCheck) {
                                            isYes = true;
                                            Log.e("exam", "答案对了=");
                                        } else {
                                            isYes = false;
                                        }

                                    }
                                });*/
                            }
                        }
                    }


                    break;

                case 1:
                    if (tradeBean != null) {

                        int second = tradeBean.getSecond();
                        time = new TimeCount(second * 1000 * 60, 1000);
                        time.start();

                    }
                    break;

            }

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_exam_multiselect);
        x.view().inject(this);

        title = getIntent().getStringExtra("title");
        if (!TextUtils.isEmpty(title)) {
            tv_category.setText(title);
        }
        init();
    }

    private void init() {
        if (OverallApplication.userData != null) {
            userInfo = OverallApplication.userData.getInfo();
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(ExamMultiselectActivity.this));


        tv_next.setOnClickListener(ExamMultiselectActivity.this);
        lin_back.setOnClickListener(ExamMultiselectActivity.this);


        if (userInfo != null) {
            getData();
        }


    }


    /**
     * 获取答题列表
     */
    private void getData() {
        RequestParam requestParam = new RequestParam();

        dialog = CustomDialog.createLoadingDialog(ExamMultiselectActivity.this);
        dialog.show();

        RequestManager.getTheoryList(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                AnswerResultParam resultParam = (AnswerResultParam) arg;

                if (resultParam != null && resultParam.isStatus()) {

                    if (resultParam.getData() != null) {
                        tradeBean = resultParam.getData().getTrade();
                        mHandler.sendEmptyMessage(1);
                        List<AnswerResultParam.Data.DataBean> dataBeens = resultParam.getData().getData();
                        if (dataBeens != null) {
                            totalRecord = dataBeens.size();
                            for (AnswerResultParam.Data.DataBean dataBean : dataBeens) {
                                dataBeanList.add(dataBean);
                            }
                            mHandler.sendEmptyMessage(0);
                        }

                    }
                }

                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_next:

                if (adapter != null) {

                    if (adapter.getSelect_num() > 0) {

                        if (adapter.getSelect_num() == adapter.getNum() && adapter.getYes_num() == adapter.getNum()) {
                            Log.e("exam", "答对了");
                            yes++;
                        } else {
                            Log.e("exam", "答错了");
                        }

                        current++;
                        if (current < totalRecord) {
                            mHandler.sendEmptyMessage(0);
                        } else {
                            jump();
                        }

                    } else {
                        ToastUtils.show("请选择答案");
                        return;
                    }

                }else {
                    ToastUtils.show("没有数据");
                }
/*
                if (!isSelect) {
                    ToastUtils.show("请选择答案");
                    return;
                }

                isSelect = false;

                if (isYes) {
                    yes++;
                }

                current++;
                if (current < totalRecord) {
                    mHandler.sendEmptyMessage(0);
                } else {
                    jump();
                }*/
                break;

            case R.id.lin_back:
                alert();
                break;
        }
    }


    private void jump() {
        Intent intent = new Intent(ExamMultiselectActivity.this, PostAnswerActivity.class);
        intent.putExtra("yes", yes);
        intent.putExtra("totalRecord", totalRecord);
        intent.putExtra("title", title);
        startActivity(intent);
        ExamMultiselectActivity.this.finish();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        yes = 0;
    }


    /**
     * 获取验证码的按钮变化
     */
    class TimeCount extends CountDownTimer {

        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            int minute = 0;
            int second = (int) (millisUntilFinished / 1000);
            if (second > 60) {
                minute = second / 60;         //取整
                second = second % 60;         //取余
            }
            tv_second.setClickable(false);
            tv_second.setText(minute + "分" + second + "秒后答题结束");
        }

        @Override
        public void onFinish() {
            ToastUtils.show("答题结束");

        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            alert();
            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }


    private void alert() {
        builder = new AlertDialog.Builder(ExamMultiselectActivity.this);
        builder.setMessage("退出后考试需重新作答，确定退出吗?");
        builder.setTitle("提示");

        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });

        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
                ExamMultiselectActivity.this.finish();
            }
        });

        alertDialog = builder.create();
        alertDialog.show();

    }

}


