package com.defense.view;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import com.defense.R;
import com.defense.adapter.PracticeListAdapter;
import com.defense.custom.PullToRefreshRecyclerView;
import com.defense.http.request_new.RequestManager;
import com.defense.model.ContentListParam;
import com.defense.model.ContentListResultParam;
import com.defense.model.TradeRequestParam;
import com.handmark.pulltorefresh.library.PullToRefreshBase;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 社会实践列表
 */
@ContentView(R.layout.activity_practice_list)
public class PracticeListActivity extends Activity implements View.OnClickListener {

    @ViewInject(R.id.practice_recycler_view)
    private PullToRefreshRecyclerView pullToRefreshRecyclerView;
    @ViewInject(R.id.lin_back)
    private LinearLayout lin_back;

    private RecyclerView recyclerView;
    private PracticeListAdapter listAdapter;
    private List<ContentListParam> contents = new ArrayList<ContentListParam>();

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    listAdapter.notifyDataSetChanged();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        x.view().inject(this);

        init();

    }

    private void init() {
        pullToRefreshRecyclerView.setMode(PullToRefreshBase.Mode.BOTH);
        recyclerView = pullToRefreshRecyclerView.getRefreshableView();
        recyclerView.setLayoutManager(new LinearLayoutManager(PracticeListActivity.this));

        listAdapter = new PracticeListAdapter(PracticeListActivity.this, contents);
        recyclerView.setAdapter(listAdapter);


        getData(45);
        lin_back.setOnClickListener(this);
    }


    /**
     * @param module
     *  0=是底部数据,1是banner
     */
    private void getData(int module) {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setModule(module);
        RequestManager.getContentList(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                ContentListResultParam resultParam = (ContentListResultParam) arg;
                if (resultParam != null && resultParam.isStatus()) {

                    List<ContentListParam> listParams = resultParam.getData();
                    if (listParams != null) {

                        for (ContentListParam listParam : listParams) {
                            contents.add(listParam);
                        }
                        mHandler.sendEmptyMessage(0);

                    }
                }


            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_back:
                PracticeListActivity.this.finish();
                break;
        }
    }
}
