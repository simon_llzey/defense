package com.defense.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.defense.R;
import com.defense.custom.CustomDialog;
import com.defense.custom.OverallApplication;
import com.defense.http.UserController;
import com.defense.http.request_new.RequestManager;
import com.defense.http.util.GsonUtils;
import com.defense.model.LoginParam;
import com.defense.model.LoginResultParam;
import com.defense.utils.PermisUtils;
import com.defense.utils.ShareUtils;
import com.defense.utils.ToastUtils;
import com.defense.view.fragment.KnowledgeFrag;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.Observable;
import java.util.Observer;

/**
 * 登录界面
 */
public class LoginActivity extends Activity implements View.OnClickListener {

    @ViewInject(R.id.tv_login)
    private TextView tv_login;
    @ViewInject(R.id.ed_passwd)
    private EditText ed_passwd;
    @ViewInject(R.id.ed_phone)
    private EditText ed_phone;
    @ViewInject(R.id.tv_register)
    private TextView tv_register;
    @ViewInject(R.id.tv_forget)
    private TextView tv_forget;

    private LoginParam loginParam = new LoginParam();

    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);
        x.view().inject(this);
        PermisUtils.verifyStoragePermissions(this);
        init();
    }

    private void init() {


        String phone = ShareUtils.getString(this, "phone1", "phone2", "");
        String passwd = ShareUtils.getString(this, "passwd1", "passwd2", "");
        ed_passwd.setText(passwd);
        ed_phone.setText(phone);
        if (!TextUtils.isEmpty(phone) && !TextUtils.isEmpty(passwd)) {
            login();
        }

        tv_login.setOnClickListener(LoginActivity.this);
        tv_register.setOnClickListener(LoginActivity.this);
        tv_forget.setOnClickListener(LoginActivity.this);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        boolean writeAccepted = false;
        switch (requestCode) {
            case 1:
                writeAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (writeAccepted) {

                }
                break;
        }
    }

    /**
     * 登录
     */
    private void login() {

        String phone = ed_phone.getText().toString().trim();
        String passwd = ed_passwd.getText().toString().trim();

        if (TextUtils.isEmpty(phone)) {
            ToastUtils.show("账户不能为空");
            return;
        }
        if (TextUtils.isEmpty(passwd)) {
            ToastUtils.show("密码不能为空");
            return;
        }


        dialog = CustomDialog.createLoadingDialog(LoginActivity.this);
        dialog.show();
        loginParam.setUsername(phone);
        loginParam.setPassword(passwd);

        RequestManager.login(loginParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                LoginResultParam resultParam = (LoginResultParam) arg;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {

                        ShareUtils.saveString(LoginActivity.this, "phone1", "phone2", loginParam.getUsername());
                        ShareUtils.saveString(LoginActivity.this, "passwd1", "passwd2", loginParam.getPassword());
                        OverallApplication.userData = resultParam.getData();
                        ToastUtils.show("登录成功");
                        LoginActivity.this.finish();

                    } else {
                        ToastUtils.show(resultParam.getMessage());
                    }
                } else {
                    ToastUtils.show("登录失败");
                }

                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        /*UserController.login(loginParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                LoginResultParam resultParam = (LoginResultParam) arg;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {
                        OverallApplication.userData = resultParam.getData();

                        Intent mainIntent = new Intent(LoginActivity.this, MainFragment.class);
                        startActivity(mainIntent);
                        ToastUtils.show("登录成功");
                        LoginActivity.this.finish();
                    } else {
                        ToastUtils.show(resultParam.getMessage());
                    }
                }

                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
*/

    }


    @Override
    public void onClick(View v) {
        Intent registerIntent = new Intent(LoginActivity.this, RegisterAct.class);
        switch (v.getId()) {
            case R.id.tv_login:
                login();
                break;
            case R.id.tv_register:
                registerIntent.putExtra("form", "register");
                startActivity(registerIntent);
                break;
            case R.id.tv_forget:
                registerIntent.putExtra("form", "forget");
                startActivity(registerIntent);
                break;
        }
    }
}
