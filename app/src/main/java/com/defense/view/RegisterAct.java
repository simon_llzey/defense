package com.defense.view;

import android.app.Activity;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.defense.R;
import com.defense.adapter.RegisterAdapter;
import com.defense.http.model.ResultParam;
import com.defense.http.request_new.RequestManager;
import com.defense.model.RequestParam;
import com.defense.model.SchoolList;
import com.defense.model.SchoolResultParam;
import com.defense.model.SchoolSpecialtyResultParam;
import com.defense.model.StudyInfo;
import com.defense.utils.ToastUtils;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 注册界面
 */
public class RegisterAct extends Activity implements View.OnClickListener {

    @ViewInject(R.id.tv_validate)
    private TextView tv_validate;
    @ViewInject(R.id.tv_register)
    private TextView tv_register;


    @ViewInject(R.id.ed_phone)
    private EditText ed_phone;
    @ViewInject(R.id.ed_validate)
    private EditText ed_validate;
    @ViewInject(R.id.ed_passwd)
    private EditText ed_passwd;
    @ViewInject(R.id.ed_passwd2)
    private EditText ed_passwd2;

    @ViewInject(R.id.tv_back)
    private TextView tv_back;

    @ViewInject(R.id.spinner_1)
    private Spinner spinner_1;
    @ViewInject(R.id.spinner_2)
    private Spinner spinner_2;
    @ViewInject(R.id.spinner_3)
    private Spinner spinner_3;
    @ViewInject(R.id.spinner_4)
    private Spinner spinner_4;

    @ViewInject(R.id.edit_user_name)
    private EditText edit_user_name;
    @ViewInject(R.id.edit_detail)
    private EditText edit_detail;

    @ViewInject(R.id.lin_register)
    private LinearLayout lin_register;

    private RegisterAdapter adapter1;
    private RegisterAdapter adapter2;
    private RegisterAdapter adapter3;
    private RegisterAdapter adapter4;

    private RequestParam requestParam;
    private String form = "";

    private TimeCount time = new TimeCount(60000, 1000);//倒计时

    private String[] names = {"大学", "高中", "初中", "小学", "企事业单位"};
    private int[] types = {0, 1, 2, 3, 4};
    private List<StudyInfo> studyInfos = new ArrayList<>();
    private List<StudyInfo> studyInfos2 = new ArrayList<>();
    private List<StudyInfo> studyInfos3 = new ArrayList<>();
    private List<StudyInfo> studyInfos4 = new ArrayList<>();
    private List<List<SchoolList.DataBean.SchoolBean>> studyInfoList = new ArrayList<>();

    private List<SchoolList.DataBean> schoolList = new ArrayList<>();


    private int index = 0;


    private int school_id = -1;
    private int second_school_id = -1;
    private int specialty_id = -1;


    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    adapter2 = new RegisterAdapter(RegisterAct.this, studyInfos2);
                    spinner_2.setAdapter(adapter2);

                    if (studyInfos2.size() <= 0) {
                        spinner_2.setVisibility(View.GONE);
                        spinner_3.setVisibility(View.GONE);
                        spinner_4.setVisibility(View.GONE);
                        studyInfos4.clear();
                        adapter4 = new RegisterAdapter(RegisterAct.this, studyInfos4);
                        spinner_4.setAdapter(adapter4);

                    } else {
                        spinner_2.setVisibility(View.VISIBLE);
                        spinner_3.setVisibility(View.VISIBLE);
                        spinner_4.setVisibility(View.VISIBLE);
                    }


                    /*if (schoolList != null && schoolList.size() > 0) {
                        for (SchoolList.DataBean dataBean : schoolList) {
                            List<SchoolList.DataBean.SchoolBean> schoolBeans = dataBean.getSecondSchool();
                            if (schoolBeans != null && schoolBeans.size() > 0) {
                                for (int z = 0; z < schoolBeans.size(); z++) {
                                    SchoolList.DataBean.SchoolBean schoolBean = schoolBeans.get(z);
                                    StudyInfo studyInfo = new StudyInfo();
                                    studyInfo.setName(schoolList.get(z).getSchool().getName());
                                    studyInfo.setType(schoolList.get(z).getSchool().getId());
                                    studyInfo.setSpec_id(schoolBean.getSchool_id());
                                }

                            }
                        }
                    }*/

                    break;
                case 1:
                    studyInfos3.clear();
                    if (studyInfoList.size() > 0) {
                        for (int i = 0; i < studyInfoList.size(); i++) {
                            List<SchoolList.DataBean.SchoolBean> list = studyInfoList.get(i);

                            if (index == i) {
                                if (list != null && list.size() > 0) {
                                    for (int z = 0; z < list.size(); z++) {
                                        SchoolList.DataBean.SchoolBean schoolBean = list.get(z);
                                        if (schoolBean != null && !TextUtils.isEmpty(schoolBean.getName())) {
                                            StudyInfo studyInfo = new StudyInfo();
                                            studyInfo.setName(schoolBean.getName());
                                            studyInfo.setType(schoolBean.getSchool_id());
                                            studyInfo.setSpec_id(schoolBean.getId());

                                            studyInfos3.add(studyInfo);
                                        }
                                    }

                                    // adapter3 = new RegisterAdapter(RegisterAct.this, studyInfos3);
                                    //spinner_3.setAdapter(adapter3);

                                } else {
                                    // adapter3 = new RegisterAdapter(RegisterAct.this, studyInfos3);
                                    //spinner_3.setAdapter(adapter3);
                                }
                            }

                            spinner_3.setVisibility(View.VISIBLE);
                            adapter3 = new RegisterAdapter(RegisterAct.this, studyInfos3);
                            spinner_3.setAdapter(adapter3);


                        }
                    }

                    if (studyInfos3.size() <= 0) {
                        spinner_3.setVisibility(View.GONE);
                        spinner_4.setVisibility(View.GONE);
                        if (studyInfos2.size() > 0) {
                            getSchoolSpec(studyInfos2.get(index).getType(), 0);
                        }
                    } else {
                        spinner_3.setVisibility(View.VISIBLE);
                        spinner_4.setVisibility(View.VISIBLE);
                    }

                    break;

                case 2:
                    if (studyInfos4.size() > 0) {
                        spinner_4.setVisibility(View.VISIBLE);
                    } else {
                        spinner_4.setVisibility(View.GONE);
                    }
                    adapter4 = new RegisterAdapter(RegisterAct.this, studyInfos4);
                    spinner_4.setAdapter(adapter4);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        x.view().inject(this);

        form = getIntent().getStringExtra("form");
        if (!TextUtils.isEmpty(form)) {

            if (form.equals("register")) {
                lin_register.setVisibility(View.VISIBLE);
                ed_passwd.setHint("请输入密码");
                ed_passwd2.setHint("请确定输入密码");
                tv_register.setText("注册");
            } else {
                lin_register.setVisibility(View.GONE);
                ed_passwd.setHint("请输入新密码");
                ed_passwd2.setHint("请确定输入新密码");
                tv_register.setText("确定");
            }
        } else {
            ed_passwd.setHint("请输入密码");
            ed_passwd2.setHint("请确定输入密码");
        }

        init();
    }

    private void init() {

        for (int i = 0; i < names.length; i++) {
            StudyInfo info = new StudyInfo();
            info.setName(names[i]);
            info.setType(types[i]);
            studyInfos.add(info);
        }

        adapter1 = new RegisterAdapter(this, studyInfos);
        spinner_1.setAdapter(adapter1);
        spinner_1.setSelection(0, true);
        if (studyInfos.size() > 0) {
            getSchoolList(studyInfos.get(0).getType() + "");
        }
        spinner_1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("register", "选择了第---》" + position);
                getSchoolList(studyInfos.get(position).getType() + "");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                index = position;
                mHandler.sendEmptyMessage(1);

                school_id = studyInfos2.get(position).getType();
                Log.e("register", "学校id----》" + school_id);

//                getSchoolSpec(studyInfos2.get(position).getType());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (studyInfos3.size() > 0) {
                    second_school_id = studyInfos3.get(position).getType();
                    Log.e("register", "二级学校id----》" + second_school_id);
                    getSchoolSpec(studyInfos3.get(position).getType(), studyInfos3.get(position).getSpec_id());
                } else {
                    getSchoolSpec(studyInfos2.get(position).getType(), 0);
//                    mHandler.sendEmptyMessage(2);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinner_4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                specialty_id = studyInfos4.get(position).getType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        tv_validate.setOnClickListener(RegisterAct.this);
        tv_register.setOnClickListener(RegisterAct.this);
        tv_back.setOnClickListener(RegisterAct.this);
    }


    /**
     * 获取验证码
     *
     * @param param
     */
    private void getValidate(RequestParam param) {

        RequestManager.getValidate(param, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                ResultParam resultParam = (ResultParam) arg;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {
                        ToastUtils.show("发送成功");
                    } else {
                        ToastUtils.show(resultParam.getMessage());
                    }
                } else {
                    ToastUtils.show("获取失败");
                }

            }
        });

    }


    /**
     * 注册
     *
     * @param param
     */
    private void register(RequestParam param) {

        RequestManager.getRegister(param, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                ResultParam resultParam = (ResultParam) arg;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {
                        ToastUtils.show("注册成功");
                        RegisterAct.this.finish();
                    } else {
                        ToastUtils.show(resultParam.getMessage());
                    }
                } else {
                    ToastUtils.show("注册失败");
                }
            }
        });

    }


    /**
     * 修改密码
     *
     * @param param
     */
    private void changePwd(RequestParam param) {

        RequestManager.getChangePwd(param, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                ResultParam resultParam = (ResultParam) arg;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {
                        ToastUtils.show("修改成功");
                        RegisterAct.this.finish();
                    } else {
                        ToastUtils.show(resultParam.getMessage());
                    }
                } else {
                    ToastUtils.show("修改失败");
                }
            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_validate:

                String phone = ed_phone.getText().toString().trim();
                if (TextUtils.isEmpty(phone)) {
                    ToastUtils.show("请输入手机号");
                    return;
                }
                time.start();
                requestParam = new RequestParam();
                requestParam.setPhone(phone);
                requestParam.setType(0 + "");
                getValidate(requestParam);
                break;

            case R.id.tv_back:
                RegisterAct.this.finish();
                break;


            case R.id.tv_register:
                String phone1 = ed_phone.getText().toString().trim();
                if (TextUtils.isEmpty(phone1)) {
                    ToastUtils.show("请输入手机号");
                    return;
                }
                String validate = ed_validate.getText().toString().trim();
                if (TextUtils.isEmpty(validate)) {
                    ToastUtils.show("请填写验证码");
                    return;
                }

                String username = edit_user_name.getText().toString().trim();

                if (TextUtils.isEmpty(username)) {
                    ToastUtils.show("请填写姓名");
                    return;
                }

                String passwd = ed_passwd.getText().toString().trim();
                String passwd2 = ed_passwd2.getText().toString().trim();
                if (TextUtils.isEmpty(passwd) || TextUtils.isEmpty(passwd2)) {
                    ToastUtils.show("请输入密码");
                    return;
                }
                if (!passwd.equals(passwd2)) {
                    ToastUtils.show("密码请填写一致");
                    return;
                }
                requestParam = new RequestParam();
                requestParam.setPhone(phone1);
                requestParam.setValidateCode(validate);
                requestParam.setPassword(passwd);


                if (!TextUtils.isEmpty(form)) {

                    if (form.equals("register")) {
                        requestParam.setName(username);

                        requestParam.setSchool_id(school_id);
                        requestParam.setSecond_school_id(second_school_id);
                        requestParam.setSpecialty_id(specialty_id);

                        String detail = edit_detail.getText().toString().trim();
                        if (!TextUtils.isEmpty(detail)) {
                            requestParam.setGradeStr(detail);
                        }
                        register(requestParam);
                    } else {
                        changePwd(requestParam);
                    }
                } else {
                    requestParam.setName(username);

                    requestParam.setSchool_id(school_id);
                    requestParam.setSecond_school_id(second_school_id);
                    requestParam.setSpecialty_id(specialty_id);

                    String detail = edit_detail.getText().toString().trim();
                    if (!TextUtils.isEmpty(detail)) {
                        requestParam.setGradeStr(detail);
                    }
                    register(requestParam);
                }

                break;

        }
    }


    /**
     * 获取验证码的按钮变化
     */
    class TimeCount extends CountDownTimer {

        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            tv_validate.setClickable(false);
            tv_validate.setText(millisUntilFinished / 1000 + "s后重新获取");
        }

        @Override
        public void onFinish() {
            tv_validate.setText("获取验证码");
            tv_validate.setClickable(true);

        }
    }


    private void getSchoolList(String type) {
        RequestParam requestParam = new RequestParam();
        requestParam.setType(type);
        RequestManager.register_school_list(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                studyInfos2.clear();
                SchoolList resultParam = (SchoolList) arg;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {

                        schoolList = resultParam.getData();
                        if (schoolList != null) {
                            for (int z = 0; z < schoolList.size(); z++) {
                                StudyInfo studyInfo = new StudyInfo();
                                studyInfo.setName(schoolList.get(z).getSchool().getName());
                                studyInfo.setType(schoolList.get(z).getSchool().getId());

                                List<SchoolList.DataBean.SchoolBean> schoolBeans = schoolList.get(z).getSecondSchool();
                                if (schoolBeans != null && schoolBeans.size() > 0) {
                                    studyInfoList.add(schoolBeans);
                                } else {
                                    studyInfoList.add(new ArrayList<SchoolList.DataBean.SchoolBean>());
                                }

                                studyInfos2.add(studyInfo);
                            }
                        }
                    }
                }

                mHandler.sendEmptyMessage(0);
                mHandler.sendEmptyMessage(1);
            }
        });
    }

    private void getSchoolSpec(int id, int spec_id) {
        RequestParam requestParam = new RequestParam();
        requestParam.setSchool_id(id);
        requestParam.setSecond_school_id(spec_id);

        RequestManager.register_school_specialty(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                studyInfos4.clear();
                SchoolSpecialtyResultParam resultParam = (SchoolSpecialtyResultParam) arg;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {

                        List<SchoolSpecialtyResultParam.DataBean> dataBeans = resultParam.getData();
                        if (dataBeans != null) {
                            for (int z = 0; z < dataBeans.size(); z++) {
                                StudyInfo studyInfo = new StudyInfo();
                                studyInfo.setName(dataBeans.get(z).getName());
                                studyInfo.setType(dataBeans.get(z).getId());
                                studyInfos4.add(studyInfo);
                            }
                        }
                    }
                }

                mHandler.sendEmptyMessage(2);
            }
        });
    }
}
