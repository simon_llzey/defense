package com.defense.view;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.custom.CustomDialog;
import com.defense.custom.OverallApplication;
import com.defense.http.model.ResultParam;
import com.defense.http.request_new.RequestManager;
import com.defense.model.LoginParam;
import com.defense.model.LoginResultParam;
import com.defense.utils.IntentUtils;
import com.defense.utils.LoginUtils;
import com.defense.utils.ShareUtils;
import com.defense.utils.ToastUtils;
import com.defense.view.fragment.ClassroomFrag;
import com.defense.view.fragment.HotConcernFrag;
import com.defense.view.fragment.KnowledgeFrag;
import com.defense.view.fragment.PracticeFrag;
import com.defense.view.fragment.TrainFrag;
import com.joooonho.SelectableRoundedImageView;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class MainFragment extends FragmentActivity implements View.OnClickListener {

    public static MainFragment getInstance = null;

    @ViewInject(R.id.lin_root)
    private LinearLayout lin_root;
    @ViewInject(R.id.lin_user)
    private LinearLayout lin_user;

    private Fragment knowledgeFrag, classRoomFrag, trainFrag, hotConcernFrag, practiceFrag;
    private List<ImageView> imgs = new ArrayList<ImageView>();
    private List<Fragment> fragments;
    @ViewInject(R.id.lin_knowledge)
    private LinearLayout lin_knowledge;
    @ViewInject(R.id.lin_classRoom)
    private LinearLayout lin_classRoom;
    @ViewInject(R.id.lin_train)
    private LinearLayout lin_train;
    @ViewInject(R.id.lin_hotConcern)
    private LinearLayout lin_hotConcern;
    @ViewInject(R.id.lin_practice)
    private LinearLayout lin_practice;

    @ViewInject(R.id.img_knowledge)
    private ImageView img_knowledge;
    @ViewInject(R.id.img_classRoom)
    private ImageView img_classRoom;
    @ViewInject(R.id.img_train)
    private ImageView img_train;
    @ViewInject(R.id.img_hotConcern)
    private ImageView img_hotConcern;
    @ViewInject(R.id.img_practice)
    private ImageView img_practice;

    private int currentTab;//当前Tab页面索引
    private int[][] images = {{R.drawable.btn_knowledge, R.drawable.btn_knowledge_sel}, {R.drawable.btn_classroom, R.drawable.btn_classroom_sel},
            {R.drawable.btn_train, R.drawable.btn_train_sel}, {R.drawable.btn_hot_concern, R.drawable.btn_hot_concern_sel},
            {R.drawable.btn_practice, R.drawable.btn_practice_sel}};

    //登录后
    @ViewInject(R.id.img_head)
    private SelectableRoundedImageView img_head;
    @ViewInject(R.id.rl_login)
    private RelativeLayout rl_login;
    @ViewInject(R.id.tv_name)
    private TextView tv_name;
    @ViewInject(R.id.tv_title2)
    private TextView tv_title2;


    //未登录
    @ViewInject(R.id.rl_logout)
    private RelativeLayout rl_logout;
    @ViewInject(R.id.lin_login)
    private LinearLayout lin_login;
    @ViewInject(R.id.tv_title)
    private TextView tv_title;
    @ViewInject(R.id.lin_search)
    private LinearLayout lin_search;
    @ViewInject(R.id.tv_logout)
    private TextView tv_logout;


    private Dialog dialog;


    //用户信息
    private LoginResultParam.UserData.InfoBean userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_fragment);
        x.view().inject(this);
        getInstance = this;
        init();
    }

    private void init() {
        fragments = new ArrayList<Fragment>();
        knowledgeFrag = new KnowledgeFrag();
        classRoomFrag = new ClassroomFrag();
        trainFrag = new TrainFrag();
        hotConcernFrag = new HotConcernFrag();
        practiceFrag = new PracticeFrag();

        fragments.add(knowledgeFrag);
        fragments.add(classRoomFrag);
        fragments.add(trainFrag);
        fragments.add(hotConcernFrag);
        fragments.add(practiceFrag);

        imgs.add(img_knowledge);
        imgs.add(img_classRoom);
        imgs.add(img_train);
        imgs.add(img_hotConcern);
        imgs.add(img_practice);

        switchFragment(0);
        lin_knowledge.setOnClickListener(MainFragment.this);
        lin_classRoom.setOnClickListener(MainFragment.this);
        lin_train.setOnClickListener(MainFragment.this);
        lin_hotConcern.setOnClickListener(MainFragment.this);
        lin_practice.setOnClickListener(MainFragment.this);
        lin_user.setOnClickListener(MainFragment.this);
        lin_login.setOnClickListener(MainFragment.this);
        lin_search.setOnClickListener(MainFragment.this);
        tv_logout.setOnClickListener(MainFragment.this);


    }

    @Override
    protected void onResume() {
        super.onResume();
        updateData();
    }

    public void updateData() {
        if (OverallApplication.userData != null) {
            rl_login.setVisibility(View.VISIBLE);
            rl_logout.setVisibility(View.GONE);
            KnowledgeFrag.getInstance.init();
            if (OverallApplication.userData.getInfo() != null) {
                userInfo = OverallApplication.userData.getInfo();
                String score = userInfo.getScore();
                if (!TextUtils.isEmpty(score)) {
                    String[] lens = score.split(",");
                    userInfo.setNumber(lens.length);
                }

                Glide.with(MainFragment.this).
                        load(userInfo.getPic()).
                        asBitmap().
                        placeholder(R.drawable.def_1).
                        into(img_head);
                tv_name.setText(userInfo.getNick_name());

            }

        } else {
            rl_login.setVisibility(View.GONE);
            rl_logout.setVisibility(View.VISIBLE);
        }
    }


    private void switchFragment(int index) {
        Fragment fragment = fragments.get(index);
        FragmentTransaction transaction = obtainFragmentTransaction(index);
        getCurrentFragment().onPause();

        if (fragment.isAdded()) {
            fragment.onResume();
        } else {
            transaction.add(R.id.lin_root, fragment);
        }
        showTab(index);
        transaction.commit();

    }

    private Fragment getCurrentFragment() {
        return fragments.get(currentTab);
    }

    private FragmentTransaction obtainFragmentTransaction(int index) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        /*if (index > currentTab) {
            transaction.setCustomAnimations(R.anim.slide_left_in, R.anim.slide_left_out);
        } else {
            transaction.setCustomAnimations(R.anim.slide_right_in, R.anim.slide_right_out);
        }*/

        return transaction;
    }

    private void showTab(int index) {
        for (int i = 0; i < fragments.size(); i++) {
            Fragment fragment = fragments.get(i);
            FragmentTransaction transaction = obtainFragmentTransaction(index);
            if (index == i) {
                transaction.show(fragment);
            } else {
                transaction.hide(fragment);
            }
            transaction.commit();
        }
        imgs.get(currentTab).setImageResource(images[currentTab][0]);
        currentTab = index;
        imgs.get(currentTab).setImageResource(images[currentTab][1]);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lin_knowledge:
                switchFragment(0);
                lin_user.setVisibility(View.VISIBLE);
                lin_login.setVisibility(View.VISIBLE);
                tv_logout.setVisibility(View.VISIBLE);
                if (OverallApplication.userData != null) {
                    tv_title2.setText("国防知识");
                } else {
                    tv_title.setText("国防知识");
                }
                break;
            case R.id.lin_classRoom:
                switchFragment(1);
                lin_user.setVisibility(View.GONE);
                lin_login.setVisibility(View.GONE);
                tv_logout.setVisibility(View.GONE);
                if (OverallApplication.userData != null) {
                    tv_title2.setText("国防课堂");
                } else {
                    tv_title.setText("国防课堂");
                }
                break;
            case R.id.lin_train:

                if (LoginUtils.isLogin(MainFragment.this)) {
                    lin_user.setVisibility(View.GONE);
                    lin_login.setVisibility(View.GONE);
                    tv_logout.setVisibility(View.GONE);
                    tv_title2.setText("军事训练");
                    switchFragment(2);
                }
               /* if (OverallApplication.userData != null) {
                    tv_title2.setText("军事训练");
                } else {
                    tv_title.setText("军事训练");
                }*/
                break;
            case R.id.lin_hotConcern:
                switchFragment(3);
                lin_user.setVisibility(View.GONE);
                lin_login.setVisibility(View.GONE);
                tv_logout.setVisibility(View.GONE);
                if (OverallApplication.userData != null) {
                    tv_title2.setText("热点关注");
                } else {
                    tv_title.setText("热点关注");
                }
                break;
            case R.id.lin_practice:
                switchFragment(4);
                lin_user.setVisibility(View.GONE);
                lin_login.setVisibility(View.GONE);
                tv_logout.setVisibility(View.GONE);
                if (OverallApplication.userData != null) {
                    tv_title2.setText("社会实践");
                } else {
                    tv_title.setText("社会实践");
                }
                break;

            case R.id.lin_user:
                startActivity(new Intent(MainFragment.this, PersonalActivity.class));
//                startActivity(new Intent(MainFragment.this, UserInfoAct.class));
                break;

            case R.id.lin_login:
                IntentUtils.jumpToLogin(MainFragment.this);
                break;
            case R.id.lin_search:
                IntentUtils.jumpToOther(MainFragment.this, SearchActivity.class);
                break;

            case R.id.tv_logout:
                dialog = CustomDialog.createLoadingDialog(MainFragment.this);
                dialog.show();
                logout();
                break;
        }
    }


    private void logout() {
        RequestManager.logout(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                ResultParam resultParam = (ResultParam) arg;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {
                        ShareUtils.clear(MainFragment.this, "passwd1");
                        ToastUtils.show("已退出账号");
                        OverallApplication.userData = null;
                        onResume();
                        switchFragment(0);
                    } else {
                        ToastUtils.show(resultParam.getMessage());
                    }
                } else {
                    ToastUtils.show("未知错误");
                }


                if (dialog != null) {
                    dialog.dismiss();
                }

            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }


}
