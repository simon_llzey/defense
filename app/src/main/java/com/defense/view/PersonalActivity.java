package com.defense.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.adapter.RegisterAdapter;
import com.defense.custom.CustomDialog;
import com.defense.custom.OverallApplication;
import com.defense.http.model.ResultParam;
import com.defense.http.request_new.RequestManager;
import com.defense.model.LoginResultParam;
import com.defense.model.RequestParam;
import com.defense.model.SchoolList;
import com.defense.model.SchoolSpecialtyResultParam;
import com.defense.model.StudyInfo;
import com.defense.model.UploadHeadParam;
import com.defense.utils.ImageUtils;
import com.defense.utils.ShareUtils;
import com.defense.utils.ToastUtils;
import com.joooonho.SelectableRoundedImageView;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class PersonalActivity extends Activity implements View.OnClickListener {

    @ViewInject(R.id.img_head)
    private SelectableRoundedImageView img_head;
    @ViewInject(R.id.lin_passwd)
    private LinearLayout lin_passwd;
    @ViewInject(R.id.edit_name)
    private EditText edit_name;
    @ViewInject(R.id.lin_school)
    private LinearLayout lin_school;
    @ViewInject(R.id.lin_back)
    private LinearLayout lin_back;
    @ViewInject(R.id.tv_modify)
    private TextView tv_modify;
    @ViewInject(R.id.edit_info)
    private EditText edit_info;
    @ViewInject(R.id.tv_exit)
    private TextView tv_exit;


    private LoginResultParam.UserData.InfoBean userInfo;
    private LoginResultParam.UserData.SchoolInfoBean schoolInfoBean;

    private static final int PHOTO_CAMAER = 100;
    private Dialog dialog;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_personal);
        x.view().inject(this);

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        if (OverallApplication.userData != null) {

            if (OverallApplication.userData.getInfo() != null) {
                userInfo = OverallApplication.userData.getInfo();
                edit_name.setHint(userInfo.getName());
                edit_info.setHint(userInfo.getGrade());
                Glide.with(PersonalActivity.this).
                        load(userInfo.getPic()).
                        asBitmap().
                        placeholder(R.drawable.def_1).
                        into(img_head);

            }
            if (OverallApplication.userData.getSchoolInfo() != null) {
                schoolInfoBean = OverallApplication.userData.getSchoolInfo();
                if (schoolInfoBean != null) {
                    edit_info.setText(schoolInfoBean.getSchoolName() + schoolInfoBean.getSecSchoolName() + schoolInfoBean.getSpecialtyName());
                } else {
                    edit_info.setText(schoolInfoBean.getSchoolName() + schoolInfoBean.getSecSchoolName() + schoolInfoBean.getSpecialtyName());
                }

            }

        }

        lin_passwd.setOnClickListener(this);
        img_head.setOnClickListener(this);
        lin_school.setOnClickListener(this);
        lin_back.setOnClickListener(this);
        tv_modify.setOnClickListener(this);
        tv_exit.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_passwd:
                startActivity(new Intent(PersonalActivity.this, RegisterAct.class).putExtra("form", "forget"));
                break;
            case R.id.img_head:
                Intent photoIntent = new Intent(PersonalActivity.this, PictureActivity.class);
                startActivityForResult(photoIntent, PHOTO_CAMAER);
                break;
            case R.id.lin_school:
                startActivity(new Intent(PersonalActivity.this, SchoolActivity.class));
                break;
            case R.id.lin_back:
                finish();
                break;
            case R.id.tv_modify:
                String name = edit_name.getText().toString().trim();
                String info = edit_info.getText().toString().trim();

                if (TextUtils.isEmpty(name) && TextUtils.isEmpty(info)) {
                    ToastUtils.show("请填写名称或信息");
                    return;
                }
                RequestParam param = new RequestParam();
                param.setName(name);
                param.setGradeStr(info);
                setData(param);
                break;

            case R.id.tv_exit:
                logout();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case PHOTO_CAMAER:
                if (resultCode == Activity.RESULT_OK) {
                    if (data == null)
                        break;
                    String path = data.getStringExtra("save_path");
                    uploadImages(path);
                }
                break;
        }
    }

    private void setData(RequestParam param) {

        dialog = CustomDialog.createLoadingDialog(PersonalActivity.this);
        dialog.show();

        RequestManager.setModify(param, new Observer() {
            @Override
            public void update(Observable o, Object arg) {

                dialog.dismiss();

                ResultParam resultParam = (ResultParam) arg;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {
                        Toast.makeText(PersonalActivity.this, "修改成功", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(PersonalActivity.this, resultParam.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(PersonalActivity.this, "未知错误", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * 单张图片上传
     *
     * @param chosePath
     */
    public void uploadImages(final String chosePath) {

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                RequestParam requestParam = new RequestParam();
                try {
                    //上传成功设置头像
                    Bitmap bitmap = BitmapFactory.decodeFile(chosePath);
                    String head = ImageUtils.bitmaptoString(bitmap);
                    requestParam.setHead(head);
                    RequestManager.upload_head(requestParam, new Observer() {
                        @Override
                        public void update(Observable observable, Object o) {
                            UploadHeadParam headParam = (UploadHeadParam) o;
                            if (headParam != null) {
                                UploadHeadParam.UploadData uploadData = headParam.getData();
                                if (uploadData.isSuccess()) {
                                    OverallApplication.userData.getInfo().setPic(uploadData.getHeader());
                                    Toast.makeText(PersonalActivity.this, "上传成功", Toast.LENGTH_SHORT).show();
                                    Glide.with(PersonalActivity.this).
                                            load(uploadData.getHeader()).
                                            asBitmap().
                                            placeholder(R.drawable.def_1).
                                            into(img_head);
                                } else {
                                    Toast.makeText(PersonalActivity.this, headParam.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                Toast.makeText(PersonalActivity.this, "上传失败", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                    Toast.makeText(PersonalActivity.this, "图片太大，请重新选择", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void logout() {
        dialog = CustomDialog.createLoadingDialog(PersonalActivity.this);
        dialog.show();
        RequestManager.logout(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                ResultParam resultParam = (ResultParam) arg;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {
                        ShareUtils.clear(PersonalActivity.this, "passwd1");
                        ToastUtils.show("已退出账号");
                        OverallApplication.userData = null;
                        MainFragment.getInstance.updateData();
                        finish();
                    } else {
                        ToastUtils.show(resultParam.getMessage());
                    }
                } else {
                    ToastUtils.show("未知错误");
                }


                if (dialog != null) {
                    dialog.dismiss();
                }

            }
        });
    }
}
