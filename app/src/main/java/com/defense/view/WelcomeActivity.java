package com.defense.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.custom.CustomDialog;
import com.defense.custom.OverallApplication;
import com.defense.http.request_new.RequestManager;
import com.defense.model.LoginParam;
import com.defense.model.LoginResultParam;
import com.defense.utils.ShareUtils;
import com.defense.utils.StatusBarCompat;
import com.defense.utils.ToastUtils;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.Observable;
import java.util.Observer;

@ContentView(R.layout.activity_welcome)
public class WelcomeActivity extends Activity {

    @ViewInject(R.id.img_welcome)
    private ImageView img_welcome;
    private Animation animation;
    @ViewInject(R.id.activity_welcome)
    private LinearLayout activity_welcome;

    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        x.view().inject(this);

        StatusBarCompat.translucentStatusBar(this);



        Glide.with(this).
                load(R.drawable.welcome).
                asBitmap().
                into(img_welcome);

        animation = AnimationUtils.loadAnimation(this, R.anim.alpha);

        activity_welcome.startAnimation(animation);

        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String phone = ShareUtils.getString(WelcomeActivity.this, "phone1", "phone2", "");
                        String passwd = ShareUtils.getString(WelcomeActivity.this, "passwd1", "passwd2", "");

                        if (TextUtils.isEmpty(phone) || TextUtils.isEmpty(passwd)) {
                            startActivity(new Intent(WelcomeActivity.this, MainFragment.class));
                            WelcomeActivity.this.finish();
                            return;
                        }


                        login(phone, passwd);

                    }
                }, 3000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }


    /**
     * 登录
     */
    private void login(String phone, String passwd) {

        LoginParam loginParam = new LoginParam();


        dialog = CustomDialog.createLoadingDialog(WelcomeActivity.this);
        dialog.show();
        loginParam.setUsername(phone);
        loginParam.setPassword(passwd);

        RequestManager.login(loginParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                LoginResultParam resultParam = (LoginResultParam) arg;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {
                        OverallApplication.userData = resultParam.getData();
                        startActivity(new Intent(WelcomeActivity.this, MainFragment.class));
                        WelcomeActivity.this.finish();
                    } else {
                        ToastUtils.show(resultParam.getMessage());
                    }
                } else {
                    startActivity(new Intent(WelcomeActivity.this, MainFragment.class));
                    WelcomeActivity.this.finish();
                }

                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

    }
}
