package com.defense.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.defense.R;
import com.defense.adapter.TrainStyleAdapter;
import com.defense.custom.OverallApplication;
import com.defense.custom.PullToRefreshRecyclerView;
import com.defense.http.request_new.RequestManager;
import com.defense.listener.OnRecyclerClickListener;
import com.defense.model.RequestParam;
import com.defense.model.SchoolListResultParam;
import com.defense.model.TradeRequestParam;
import com.defense.model.TrainStyleParam;
import com.defense.utils.IntentUtils;
import com.defense.utils.ToastUtils;
import com.handmark.pulltorefresh.library.PullToRefreshBase;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 军事风采列表
 */
public class TrainStyleListActivity extends Activity implements View.OnClickListener {

    public static TrainStyleListActivity getInstance = null;

    private RecyclerView rv_style;

    @ViewInject(R.id.rv_style)
    private PullToRefreshRecyclerView refreshRecyclerView;
    private TrainStyleAdapter styleAdapter;

    @ViewInject(R.id.lin_back)
    private LinearLayout lin_back;
    @ViewInject(R.id.tv_school)
    private TextView tv_school;
    @ViewInject(R.id.lin_publish)
    private LinearLayout lin_publish;

    private List<SchoolListResultParam.DataBean.ImglistBean> imgList = new ArrayList<SchoolListResultParam.DataBean.ImglistBean>();

    //学校ID
    private int sid = 0;
    private String name = "";

    private int school_id = -1;
    public String path = "";

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    styleAdapter.notifyDataSetChanged();
                    break;
            }

        }
    };

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_train_style_list);
        x.view().inject(this);

        getInstance = this;

        if (OverallApplication.userData != null) {
            if (OverallApplication.userData.getInfo() != null) {
                school_id = OverallApplication.userData.getInfo().getSchool_id();
            }
        }

        sid = getIntent().getIntExtra("id", 0);
        name = getIntent().getStringExtra("name");
        if (!TextUtils.isEmpty(name)) {
            tv_school.setText(name);
        }

        if (school_id == sid){
            lin_publish.setVisibility(View.VISIBLE);
        }else {
            lin_publish.setVisibility(View.INVISIBLE);
        }
        init();

    }

    private void init() {
        refreshRecyclerView.setMode(PullToRefreshBase.Mode.BOTH);
        rv_style = refreshRecyclerView.getRefreshableView();

        rv_style.setLayoutManager(new LinearLayoutManager(TrainStyleListActivity.this));
        styleAdapter = new TrainStyleAdapter(TrainStyleListActivity.this, imgList);
        rv_style.setAdapter(styleAdapter);

        styleAdapter.setOnRecyclerClickListener(new OnRecyclerClickListener() {
            @Override
            public void onItemListener(int position) {
                Intent intent = new Intent(TrainStyleListActivity.this, ImagePageActivity.class);
                intent.putExtra("style_id", imgList.get(position).getId());
                startActivity(intent);
            }

            @Override
            public void onItemMultiListener(int position, boolean isCheck) {

            }
        });

        refreshRecyclerView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<RecyclerView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<RecyclerView> refreshView) {
                getSchoolList(false, sid);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<RecyclerView> refreshView) {
                getSchoolList(true, sid);
            }
        });


        refresh();
        lin_back.setOnClickListener(TrainStyleListActivity.this);
        lin_publish.setOnClickListener(TrainStyleListActivity.this);

    }


    public void refresh() {
        getSchoolList(false, sid);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_back:
                TrainStyleListActivity.this.finish();
                break;
            case R.id.lin_publish:
                if (school_id == sid)
                    IntentUtils.jumpToOther(TrainStyleListActivity.this, TrainStyleEditActivity.class);
                else
                    ToastUtils.show("只能在自己所属的学校发布");
                break;
        }
    }


    /**
     * 获取学校列表
     *
     * @param id
     */
    private void getSchoolList(boolean isLoad, final int id) {
        RequestParam requestParam = new RequestParam();
        requestParam.setOrgId(id);
        int offset = 0;
        if (isLoad) {
            if (imgList != null && imgList.size() > 0) {
                offset = imgList.size();
            }

            requestParam.setPageOffset(offset);
        } else {
            requestParam.setPageOffset(offset);
            imgList.clear();
        }

        RequestManager.getSchoolList(requestParam, new Observer() {
            @Override
            public void update(Observable observable, Object o) {
                SchoolListResultParam resultParam = (SchoolListResultParam) o;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {
                        setPath(resultParam.getData().getBasepath());
                        List<SchoolListResultParam.DataBean.ImglistBean> imglistBeens = resultParam.getData().getImglist();
                        if (imglistBeens != null && imglistBeens.size() > 0) {
                            for (SchoolListResultParam.DataBean.ImglistBean imglistBean : imglistBeens) {
                                imgList.add(imglistBean);
                            }
                            mHandler.sendEmptyMessage(0);
                        } else {
                            ToastUtils.show("暂无数据");
                        }
                    }
                }

                if (refreshRecyclerView != null) {
                    refreshRecyclerView.onRefreshComplete();
                }

            }
        });
    }

}
