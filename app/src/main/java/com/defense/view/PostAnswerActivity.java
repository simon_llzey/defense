package com.defense.view;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.custom.OverallApplication;
import com.defense.http.model.ResultParam;
import com.defense.http.request_new.RequestManager;
import com.defense.model.LoginResultParam;
import com.defense.model.RequestParam;
import com.defense.utils.NumberUtils;
import com.defense.utils.ToastUtils;
import com.defense.view.fragment.TrainAssessmentFrag;
import com.joooonho.SelectableRoundedImageView;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.Observable;
import java.util.Observer;

/**
 * 提交答题
 */
@ContentView(R.layout.activity_post_answer)
public class PostAnswerActivity extends Activity implements View.OnClickListener {

    @ViewInject(R.id.tv_score)
    private TextView tv_score;
    @ViewInject(R.id.tv_content)
    private TextView tv_content;
    @ViewInject(R.id.tv_name)
    private TextView tv_name;
    @ViewInject(R.id.lin_back)
    private LinearLayout lin_back;
    @ViewInject(R.id.img_head)
    private SelectableRoundedImageView img_head;

    @ViewInject(R.id.tv_next)
    private TextView tv_next;

    //答对的题数
    private int yes = 0;
    //总题数
    private int totalRecord = 0;

    //得分
    private int score = 0;
    //总分
    private double scores = 100.00;


    private String nick_name = "";
    private String user_head = "";

    //是否正式
    private String title = "";

    private LoginResultParam.UserData.InfoBean infoBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        x.view().inject(this);

        if (OverallApplication.userData != null) {
            infoBean = OverallApplication.userData.getInfo();
            if (infoBean != null) {
                nick_name = infoBean.getNick_name();
                user_head = infoBean.getPic();
            }

            tv_name.setText(nick_name);
            Glide.with(PostAnswerActivity.this).
                    load(user_head).
                    asBitmap().
                    into(img_head);
        }

        yes = getIntent().getIntExtra("yes", 0);
        totalRecord = getIntent().getIntExtra("totalRecord", 0);
        title = getIntent().getStringExtra("title");
        init();
    }


    private void init() {
        score = (int) (((scores / totalRecord)) * yes);

        if (title.equals(TrainAssessmentFrag.title_formal)) {
            getScore(score);
        }


        tv_score.setText(score + "分");
        tv_content.setText("答对:" + yes + "题" + ",  答错:" + (totalRecord - yes) + "题");



        lin_back.setOnClickListener(PostAnswerActivity.this);
        tv_next.setOnClickListener(PostAnswerActivity.this);

    }


    /**
     * 提交答题
     *
     * @param num
     */
    private void getScore(int num) {
        RequestParam requestParam = new RequestParam();
        requestParam.setScore(num);
        RequestManager.post_score(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                ResultParam resultParam = (ResultParam) arg;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {
                        ToastUtils.show("提交成功");
                        int num = infoBean.getNumber();
                        infoBean.setNumber(num + 1);
                    }
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_back:
                PostAnswerActivity.this.finish();
                break;
            case R.id.tv_next:
                PostAnswerActivity.this.finish();
                break;
        }
    }
}
