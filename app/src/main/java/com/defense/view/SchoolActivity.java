package com.defense.view;

import android.app.Activity;
import android.app.Dialog;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.defense.R;
import com.defense.adapter.RegisterAdapter;
import com.defense.custom.CustomDialog;
import com.defense.custom.OverallApplication;
import com.defense.http.model.ResultParam;
import com.defense.http.request_new.RequestManager;
import com.defense.model.LoginResultParam;
import com.defense.model.RequestParam;
import com.defense.model.SchoolList;
import com.defense.model.SchoolSpecialtyResultParam;
import com.defense.model.StudyInfo;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class SchoolActivity extends Activity implements View.OnClickListener {

    @ViewInject(R.id.spinner_1)
    private Spinner spinner_1;
    @ViewInject(R.id.spinner_2)
    private Spinner spinner_2;
    @ViewInject(R.id.spinner_3)
    private Spinner spinner_3;
    @ViewInject(R.id.spinner_4)
    private Spinner spinner_4;
    @ViewInject(R.id.tv_replace)
    private TextView tv_replace;
    @ViewInject(R.id.lin_back)
    private LinearLayout lin_back;

    private RegisterAdapter adapter1;
    private RegisterAdapter adapter2;
    private RegisterAdapter adapter3;
    private RegisterAdapter adapter4;
    private List<StudyInfo> studyInfos = new ArrayList<>();
    private List<StudyInfo> studyInfos2 = new ArrayList<>();
    private List<StudyInfo> studyInfos3 = new ArrayList<>();
    private List<StudyInfo> studyInfos4 = new ArrayList<>();
    private List<List<SchoolList.DataBean.SchoolBean>> studyInfoList = new ArrayList<>();
    private List<SchoolList.DataBean> schoolList = new ArrayList<>();
    private int index = 0;


    private int school_id = -1;
    private int second_school_id = -1;
    private int specialty_id = -1;
    private String[] names = {"大学", "高中", "初中", "小学", "企事业单位"};
    private int[] types = {0, 1, 2, 3, 4};

    private Dialog dialog;

    private String school_1 = "";
    private String school_2 = "";
    private String school_3 = "";
    private String school_4 = "";

    RequestParam requestParam = new RequestParam();

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    adapter2 = new RegisterAdapter(SchoolActivity.this, studyInfos2);
                    spinner_2.setAdapter(adapter2);

                    if (studyInfos2.size() <= 0) {
                        spinner_2.setVisibility(View.GONE);
                        spinner_3.setVisibility(View.GONE);
                        spinner_4.setVisibility(View.GONE);
                        studyInfos4.clear();
                        adapter4 = new RegisterAdapter(SchoolActivity.this, studyInfos4);
                        spinner_4.setAdapter(adapter4);

                    } else {
                        spinner_2.setVisibility(View.VISIBLE);
                        spinner_3.setVisibility(View.VISIBLE);
                        spinner_4.setVisibility(View.VISIBLE);
                    }


                    /*if (schoolList != null && schoolList.size() > 0) {
                        for (SchoolList.DataBean dataBean : schoolList) {
                            List<SchoolList.DataBean.SchoolBean> schoolBeans = dataBean.getSecondSchool();
                            if (schoolBeans != null && schoolBeans.size() > 0) {
                                for (int z = 0; z < schoolBeans.size(); z++) {
                                    SchoolList.DataBean.SchoolBean schoolBean = schoolBeans.get(z);
                                    StudyInfo studyInfo = new StudyInfo();
                                    studyInfo.setName(schoolList.get(z).getSchool().getName());
                                    studyInfo.setType(schoolList.get(z).getSchool().getId());
                                    studyInfo.setSpec_id(schoolBean.getSchool_id());
                                }

                            }
                        }
                    }*/

                    break;
                case 1:
                    studyInfos3.clear();
                    if (studyInfoList.size() > 0) {
                        for (int i = 0; i < studyInfoList.size(); i++) {
                            List<SchoolList.DataBean.SchoolBean> list = studyInfoList.get(i);

                            if (index == i) {
                                if (list != null && list.size() > 0) {
                                    for (int z = 0; z < list.size(); z++) {
                                        SchoolList.DataBean.SchoolBean schoolBean = list.get(z);
                                        if (schoolBean != null && !TextUtils.isEmpty(schoolBean.getName())) {
                                            StudyInfo studyInfo = new StudyInfo();
                                            studyInfo.setName(schoolBean.getName());
                                            studyInfo.setType(schoolBean.getSchool_id());
                                            studyInfo.setSpec_id(schoolBean.getId());

                                            studyInfos3.add(studyInfo);
                                        }
                                    }

                                    // adapter3 = new RegisterAdapter(RegisterAct.this, studyInfos3);
                                    //spinner_3.setAdapter(adapter3);

                                } else {
                                    // adapter3 = new RegisterAdapter(RegisterAct.this, studyInfos3);
                                    //spinner_3.setAdapter(adapter3);
                                }
                            }

                            spinner_3.setVisibility(View.VISIBLE);
                            adapter3 = new RegisterAdapter(SchoolActivity.this, studyInfos3);
                            spinner_3.setAdapter(adapter3);


                        }
                    }

                    if (studyInfos3.size() <= 0) {
                        spinner_3.setVisibility(View.GONE);
                        spinner_4.setVisibility(View.GONE);
                        if (studyInfos2.size() > 0) {
                            getSchoolSpec(studyInfos2.get(index).getType(), 0);
                        }
                    } else {
                        spinner_3.setVisibility(View.VISIBLE);
                        spinner_4.setVisibility(View.VISIBLE);
                    }

                    break;

                case 2:
                    if (studyInfos4.size() > 0) {
                        spinner_4.setVisibility(View.VISIBLE);
                    } else {
                        spinner_4.setVisibility(View.GONE);
                    }
                    adapter4 = new RegisterAdapter(SchoolActivity.this, studyInfos4);
                    spinner_4.setAdapter(adapter4);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_school);
        x.view().inject(this);

        initData();
    }

    private void initData() {

        if (OverallApplication.userData != null && OverallApplication.userData.getInfo() != null) {
            requestParam.setName(OverallApplication.userData.getInfo().getNick_name());
        } else {

        }

        for (int i = 0; i < names.length; i++) {
            StudyInfo info = new StudyInfo();
            info.setName(names[i]);
            info.setType(types[i]);
            studyInfos.add(info);
        }

        adapter1 = new RegisterAdapter(this, studyInfos);
        spinner_1.setAdapter(adapter1);
        spinner_1.setSelection(0, true);
        if (studyInfos.size() > 0) {
            getSchoolList(studyInfos.get(0).getType() + "");
        }
        spinner_1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("register", "选择了第---》" + position);
                school_1 = studyInfos.get(position).getName();

                getSchoolList(studyInfos.get(position).getType() + "");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                index = position;
                mHandler.sendEmptyMessage(1);
                school_id = studyInfos2.get(position).getType();
                school_2 = studyInfos2.get(position).getName();
//                getSchoolSpec(studyInfos2.get(position).getType());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (studyInfos3.size() > 0) {
                    school_3 = studyInfos3.get(position).getName();
                    second_school_id = studyInfos3.get(position).getType();
                    getSchoolSpec(studyInfos3.get(position).getType(), studyInfos3.get(position).getSpec_id());
                } else {
                    school_3 = "";
                    getSchoolSpec(studyInfos2.get(position).getType(), 0);
//                    mHandler.sendEmptyMessage(2);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinner_4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                specialty_id = studyInfos4.get(position).getType();
                school_4 = studyInfos4.get(position).getName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        tv_replace.setOnClickListener(this);
        lin_back.setOnClickListener(this);
    }


    private void getSchoolList(String type) {
        RequestParam requestParam = new RequestParam();
        requestParam.setType(type);
        RequestManager.register_school_list(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                studyInfos2.clear();
                SchoolList resultParam = (SchoolList) arg;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {

                        schoolList = resultParam.getData();
                        if (schoolList != null) {
                            for (int z = 0; z < schoolList.size(); z++) {
                                StudyInfo studyInfo = new StudyInfo();
                                studyInfo.setName(schoolList.get(z).getSchool().getName());
                                studyInfo.setType(schoolList.get(z).getSchool().getId());

                                List<SchoolList.DataBean.SchoolBean> schoolBeans = schoolList.get(z).getSecondSchool();
                                if (schoolBeans != null && schoolBeans.size() > 0) {
                                    studyInfoList.add(schoolBeans);
                                } else {
                                    studyInfoList.add(new ArrayList<SchoolList.DataBean.SchoolBean>());
                                }

                                studyInfos2.add(studyInfo);
                            }
                        }
                    }
                }

                mHandler.sendEmptyMessage(0);
                mHandler.sendEmptyMessage(1);
            }
        });
    }

    private void getSchoolSpec(int id, int spec_id) {
        RequestParam requestParam = new RequestParam();
        requestParam.setSchool_id(id);
        requestParam.setSecond_school_id(spec_id);

        RequestManager.register_school_specialty(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                studyInfos4.clear();
                SchoolSpecialtyResultParam resultParam = (SchoolSpecialtyResultParam) arg;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {

                        List<SchoolSpecialtyResultParam.DataBean> dataBeans = resultParam.getData();
                        if (dataBeans != null) {
                            for (int z = 0; z < dataBeans.size(); z++) {
                                StudyInfo studyInfo = new StudyInfo();
                                studyInfo.setName(dataBeans.get(z).getName());
                                studyInfo.setType(dataBeans.get(z).getId());
                                studyInfos4.add(studyInfo);
                            }
                        }
                    }
                }

                mHandler.sendEmptyMessage(2);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_replace:

                requestParam.setSchool_id(school_id);
                requestParam.setSecond_school_id(second_school_id);
                requestParam.setSpecialty_id(specialty_id);

                setData(requestParam);
                break;
            case R.id.lin_back:
                finish();
                break;
        }
    }


    private void setData(RequestParam param) {

        dialog = CustomDialog.createLoadingDialog(SchoolActivity.this);
        dialog.show();

        RequestManager.setModify(param, new Observer() {
            @Override
            public void update(Observable o, Object arg) {

                dialog.dismiss();

                ResultParam resultParam = (ResultParam) arg;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {

                        if (OverallApplication.userData != null && OverallApplication.userData.getSchoolInfo() != null) {
                            LoginResultParam.UserData.SchoolInfoBean schoolInfoBean = OverallApplication.userData.getSchoolInfo();
                            schoolInfoBean.setSchoolName(school_1);
                            schoolInfoBean.setSecSchoolName(school_2);
                            schoolInfoBean.setSpecialtyName(school_3 + school_4);
                        }

                        Toast.makeText(SchoolActivity.this, "修改成功", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(SchoolActivity.this, resultParam.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}
