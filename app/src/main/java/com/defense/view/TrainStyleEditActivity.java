package com.defense.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.defense.R;
import com.defense.adapter.StyleEditAdapter;
import com.defense.custom.CustomDialog;
import com.defense.custom.MyGridView;
import com.defense.custom.OverallApplication;
import com.defense.http.request_new.RequestManager;
import com.defense.model.RequestParam;
import com.defense.model.UploadResult;
import com.defense.utils.ImageUtils;
import com.defense.utils.ToastUtils;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 军事风采-发布编辑界面
 */
@ContentView(R.layout.activity_train_style_edit)
public class TrainStyleEditActivity extends Activity implements View.OnClickListener {

    @ViewInject(R.id.ed_content)
    private EditText ed_content;
    @ViewInject(R.id.grid_list)
    private MyGridView grid_list;
    @ViewInject(R.id.img_add)
    private ImageView img_add;
    @ViewInject(R.id.img_reduce)
    private ImageView img_reduce;
    @ViewInject(R.id.lin_back)
    private LinearLayout lin_back;
    @ViewInject(R.id.lin_send)
    private LinearLayout lin_send;

    //读取照片返回
    private static final int PHOTO_CAMAER = 0;

    private List<String> images = new ArrayList<String>();
    private StyleEditAdapter editAdapter;


    private List<String> postImages = new ArrayList<String>();

    private Dialog dialog;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    editAdapter.notifyDataSetChanged();
                    break;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        x.view().inject(this);
        init();
    }

    private void init() {

        editAdapter = new StyleEditAdapter(TrainStyleEditActivity.this, images);
        grid_list.setAdapter(editAdapter);

        img_add.setOnClickListener(TrainStyleEditActivity.this);
        img_reduce.setOnClickListener(TrainStyleEditActivity.this);
        lin_back.setOnClickListener(TrainStyleEditActivity.this);
        lin_send.setOnClickListener(TrainStyleEditActivity.this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_add:
                Intent photoIntent = new Intent(TrainStyleEditActivity.this, PictureActivity.class);
                startActivityForResult(photoIntent, PHOTO_CAMAER);
                break;
            case R.id.img_reduce:
                if (images.size() <= 0) {
                    return;
                }
                postImages.remove(postImages.size() - 1);
                images.remove(images.size() - 1);
                mHandler.sendEmptyMessage(0);
                break;

            case R.id.lin_back:
                TrainStyleEditActivity.this.finish();
                break;

            case R.id.lin_send:
                String content = ed_content.getText().toString().trim();
                if (!TextUtils.isEmpty(content)) {
                    postMsg(postImages, content);
                } else {
                    ToastUtils.show("内容不能为空");
                }
                break;
        }
    }


    /**
     * 上传学校风采
     *
     * @param list
     * @param content
     */
    private void postMsg(List<String> list, String content) {

        dialog = CustomDialog.createLoadingDialog(this);
        dialog.show();

        RequestParam requestParam = new RequestParam();
        requestParam.setImages(list);
        requestParam.setDetails(content);
        RequestManager.upload_style(requestParam, new Observer() {
            @Override
            public void update(Observable observable, Object o) {
                UploadResult result = (UploadResult) o;
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (result != null) {
                    if (result.isStatus()) {
                        ToastUtils.show("上传成功");
                        TrainStyleListActivity.getInstance.refresh();
                        TrainStyleEditActivity.this.finish();
                    } else {
                        ToastUtils.show(result.getMessage());
                    }

                } else {
                    ToastUtils.show("上传失败");
                }

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PHOTO_CAMAER:
                if (resultCode == Activity.RESULT_OK) {
                    if (data == null)
                        break;
                    final String path = data.getStringExtra("save_path");
                    if (images.size() > 8) {
                        ToastUtils.show("最多上传9张图");
                        return;
                    }
                    images.add(path);
                    mHandler.sendEmptyMessage(0);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Bitmap bitmap = BitmapFactory.decodeFile(path);
                            String head = ImageUtils.bitmaptoString(bitmap);
                            Log.e("train", "图==" + head);
                            postImages.add(head);
                        }
                    }).start();

                }
                break;
        }
    }
}
