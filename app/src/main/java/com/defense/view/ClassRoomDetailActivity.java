package com.defense.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.defense.R;
import com.defense.custom.CustomDialog;
import com.defense.custom.OverallApplication;
import com.defense.http.request_new.RequestManager;
import com.defense.model.ContentDetailParam;
import com.defense.model.SaveCommentParam;
import com.defense.model.TradeRequestParam;
import com.defense.utils.DownloadMan;
import com.defense.utils.FileUtils;
import com.defense.utils.IntentUtils;
import com.defense.utils.LoginUtils;
import com.defense.utils.ToastUtils;
import com.defense.utils.WebViewUtils;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.just.agentwebX5.AgentWeb;
import com.just.agentwebX5.IWebLayout;
import com.shockwave.pdfium.PdfDocument;
import com.tencent.smtt.sdk.QbSdk;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import static com.defense.view.HotDetailActivity.fullScreenByJs;

/**
 * 国防课堂详情
 */
public class ClassRoomDetailActivity extends Activity implements View.OnClickListener, OnPageChangeListener, OnLoadCompleteListener {

    private ClassRoomDetailActivity a;

    @ViewInject(R.id.lin_back)
    private LinearLayout lin_back;
    @ViewInject(R.id.tv_title)
    private TextView tv_title;
    //    @ViewInject(R.id.pdfView)
//    private PDFView pdfView;
    @ViewInject(R.id.web_content)
    private WebView web_content;

    @ViewInject(R.id.tv_browse)
    private TextView tv_browse;
    @ViewInject(R.id.tv_praise)
    private TextView tv_praise;

    @ViewInject(R.id.lin_like)
    private LinearLayout lin_like;

    @ViewInject(R.id.lin_comment)
    private LinearLayout lin_comment;

    @ViewInject(R.id.video_view)
    private FrameLayout videoview;
    @ViewInject(R.id.lin_bottom)
    private LinearLayout lin_bottom;

    private View xCustomView;
    private WebChromeClient.CustomViewCallback xCustomViewCallback;

    ContentDetailParam.DataBean dataBean;

    //记录是否点赞
    private boolean isLike = false;
    //记录是否恢复界面
    private int browse = 1;
    private boolean isLoading = false;

    private int pageNumber = 0;
    //文章列表id
    private int id = -1;

    private Dialog dialog;

    @ViewInject(R.id.lin_root)
    private LinearLayout lin_root;
    private AgentWeb mAgentWebX5;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    dataBean = (ContentDetailParam.DataBean) msg.obj;
                    if (dataBean != null) {
                        isLike = dataBean.isPraiseSign();

                        tv_browse.setText("阅读量 " + dataBean.getBrowse());
                        tv_praise.setText("" + dataBean.getPraise());

                        if (dataBean.getContent() != null) {
                            tv_title.setText(dataBean.getContent().getTitle());

                            mAgentWebX5 = WebViewUtils.optionHtmlWebView(ClassRoomDetailActivity.this, dataBean.getContent().getDetails(), lin_root);
                           /* web_content.addJavascriptInterface(new JsObject(), "onClick");
                            WebViewUtils.optionHtmlWebView(dataBean.getContent().getDetails(), web_content);
//                            web_content.loadUrl("http://v.qq.com/iframe/player.html?vid=o054644cx58&tiny=0&auto=0");
                            web_content.setWebChromeClient(new MyWebChromeClient());
                            web_content.setWebViewClient(new MyWebViewClient());*/

                            /*String path = dataBean.getBasepath() + dataBean.getContent().getDetails();
                            loadPDF(path);*/
                        }
                    }

                    break;

                case 1:
                    tv_praise.setText("" + (dataBean.getPraise() + 1));
                    break;

                case 2:
                    String filePath = (String) msg.obj;
                    if (!TextUtils.isEmpty(filePath)) {
                       /* pdfView.fromFile(new File(filePath))
                                .defaultPage(pageNumber)
                                .onPageChange(ClassRoomDetailActivity.this)
                                .enableAnnotationRendering(true)
                                .onLoad(ClassRoomDetailActivity.this)
                                .scrollHandle(new DefaultScrollHandle(ClassRoomDetailActivity.this))
                                .load();*/


                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }

                    break;

            }
        }
    };


    private void loadPDF(final String path) {
        dialog = CustomDialog.createLoadingDialog(ClassRoomDetailActivity.this);
        dialog.show();
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    DownloadMan.getInstance(ClassRoomDetailActivity.this).downloadFile(path, new Observer() {
                        @Override
                        public void update(Observable o, Object arg) {
                            String filePath = (String) arg;
                            Message message = Message.obtain();
                            message.what = 2;
                            message.obj = filePath;
                            mHandler.sendMessage(message);
                            Log.e("download", "保存路径=" + filePath);

                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_class_room_detail);
        x.view().inject(this);
        a = this;
        id = getIntent().getIntExtra("id", -1);

        if (id > -1) {
            getData(id);
        } else {
            ToastUtils.show("文章加载失败");
        }

        lin_comment.setOnClickListener(ClassRoomDetailActivity.this);
        lin_back.setOnClickListener(ClassRoomDetailActivity.this);
        lin_like.setOnClickListener(ClassRoomDetailActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAgentWebX5 != null)
            mAgentWebX5.getWebLifeCycle().onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAgentWebX5 != null)
            mAgentWebX5.getWebLifeCycle().onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mAgentWebX5 != null)
            mAgentWebX5.uploadFileResult(requestCode, resultCode, data);
    }


    @Override
    protected void onDestroy() {
        //mAgentWebX5.destroy();
        super.onDestroy();
        if (mAgentWebX5 != null)
            mAgentWebX5.getWebLifeCycle().onDestroy();
    }

    /**
     * 获取课堂详情
     *
     * @param id
     */
    private void getData(int id) {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setId(id);
        requestParam.setBrowse(browse);
        requestParam.setPraise(0);
        RequestManager.getContentDetail(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {

                ContentDetailParam contentDetailParam = (ContentDetailParam) arg;
                if (contentDetailParam != null && contentDetailParam.isStatus()) {
                    ContentDetailParam.DataBean data = contentDetailParam.getData();
                    if (data != null) {
                        Message message = Message.obtain();
                        message.what = 0;
                        message.obj = data;
                        mHandler.sendMessage(message);
                    }
                }


            }
        });

    }

    /**
     * 点赞
     *
     * @param id
     */
    private void saveLike(int id) {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setcId(id);
        RequestManager.savePraise(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                SaveCommentParam commentParam = (SaveCommentParam) arg;
                if (commentParam != null && commentParam.isStatus()) {
                    ToastUtils.show("点赞成功");
                    isLike = true;
                    mHandler.sendEmptyMessage(1);
                } else {
                    isLike = false;
                    ToastUtils.show("点赞失败");
                }

            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_comment:
                if (LoginUtils.isLogin(ClassRoomDetailActivity.this)) {
                    if (dataBean != null && dataBean.getContent() != null) {
                        Intent commentIntent = new Intent(ClassRoomDetailActivity.this, CommentActivity.class);
                        commentIntent.putExtra("id", dataBean.getContent().getId());
                        startActivity(commentIntent);
                    } else {
                        ToastUtils.show("文章不存在");
                    }
                }

                break;
            case R.id.lin_back:
                ClassRoomDetailActivity.this.finish();
                break;

            case R.id.lin_like:
                if (LoginUtils.isLogin(ClassRoomDetailActivity.this)) {
                    if (dataBean != null) {
                        if (!dataBean.isPraiseSign()) {
                            if (isLike) {
                                ToastUtils.show("您已经赞过");
                                return;
                            }

                            if (dataBean.getContent() != null) {
                                saveLike(dataBean.getContent().getId());
                            }
                        } else {
                            ToastUtils.show("您已经赞过");
                        }
                    }
                }


                break;
        }
    }

    @Override
    public void loadComplete(int nbPages) {
        /*PdfDocument.Meta meta = pdfView.getDocumentMeta();

        printBookmarksTree(pdfView.getTableOfContents(), "-");*/
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        setTitle(String.format("%s %s / %s", dataBean.getContent().getTitle(), page + 1, pageCount));
    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
            DownloadMan.getInstance(ClassRoomDetailActivity.this).closeDownload();
        }
        return super.onKeyDown(keyCode, event);
    }
}
