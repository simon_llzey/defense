package com.defense.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.defense.adapter.ImagePageListAdapter;
import com.defense.custom.BannerImageLoader;
import com.defense.custom.MyGridView;
import com.defense.http.request_new.RequestManager;
import com.defense.http.util.GsonUtils;
import com.defense.model.ContentListParam;
import com.defense.model.TradeRequestParam;
import com.defense.model.TrainStyleDetailParam;
import com.defense.model.TrainStyleParam;
import com.defense.utils.WebViewUtils;
import com.joooonho.SelectableRoundedImageView;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.listener.OnBannerListener;

import org.json.JSONArray;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 军训风采-详情界面
 */
public class ImagePageActivity extends Activity implements View.OnClickListener {

    @ViewInject(R.id.lin_back)
    private LinearLayout lin_back;

    @ViewInject(R.id.img_head)
    private SelectableRoundedImageView img_head;
    @ViewInject(R.id.tv_name)
    private TextView tv_name;
    @ViewInject(R.id.tv_browse)
    private TextView tv_browse;
    @ViewInject(R.id.tv_comment)
    private TextView tv_comment;

    @ViewInject(R.id.tv_content)
    private TextView tv_content;
    @ViewInject(R.id.list_img)
    private LinearLayout list_img;

    private ImagePageListAdapter listAdapter;
    private List<String> images = new ArrayList<String>();

    private int id = -1;

    TrainStyleDetailParam.DataBean dataBean;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    dataBean = (TrainStyleDetailParam.DataBean) msg.obj;
                    if (dataBean != null && dataBean.getContent() != null) {
                        TrainStyleDetailParam.DataBean.ContentBean contentListParam = dataBean.getContent();
                        if (contentListParam != null) {

                            for (int i = 0; i < images.size(); i++) {
                                ImageView imageView = new ImageView(ImagePageActivity.this);
//                                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                params.setMargins(10,10,10,10);
                                imageView.setLayoutParams(params);

                                Glide.with(ImagePageActivity.this).
                                        load(TrainStyleListActivity.getInstance.getPath() + images.get(i)).
                                        asBitmap().
                                        into(imageView);

                                list_img.addView(imageView);
                            }


//                            listAdapter.notifyDataSetChanged();
                            Glide.with(ImagePageActivity.this).
                                    load(TrainStyleListActivity.getInstance.getPath() + contentListParam.getPhoto()).
                                    asBitmap().
                                    into(img_head);
                            tv_name.setText(dataBean.getContent().getNickname());
                            tv_content.setText(dataBean.getContent().getIntro());
                            tv_browse.setText("浏览 " + dataBean.getBrowse());
                            tv_comment.setText("评论 " + dataBean.getComment());
                        }


                    }

                    break;
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_page);
        x.view().inject(this);

        id = getIntent().getIntExtra("style_id", -1);
        if (id > -1) {
            init();
        }

    }

    private void init() {

//        listAdapter = new ImagePageListAdapter(ImagePageActivity.this, images);
//        list_img.setAdapter(listAdapter);
        getData(id, 1, 0);

        lin_back.setOnClickListener(ImagePageActivity.this);
        tv_comment.setOnClickListener(ImagePageActivity.this);
    }

    /**
     * 获取风采详情
     *
     * @param id
     * @param browse
     * @param comment
     */
    private void getData(int id, int browse, int comment) {
        TradeRequestParam requestParam = new TradeRequestParam();
        requestParam.setId(id);
        requestParam.setBrowse(browse);
        requestParam.setComment(comment);
        RequestManager.getTrainStyleDetail(requestParam, new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                TrainStyleDetailParam detailParam = (TrainStyleDetailParam) arg;
                if (detailParam != null && detailParam.isStatus()) {
                    TrainStyleDetailParam.DataBean styleData = detailParam.getData();
                    if (styleData != null) {
                        String list = styleData.getContent().getDetails();
                        try {
                            JSONArray jsonArray = new JSONArray(list);
                            for (int z = 0; z < jsonArray.length(); z++) {
                                String data = jsonArray.getString(z);
                                images.add(data);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        Message message = Message.obtain();
                        message.what = 0;
                        message.obj = styleData;
                        mHandler.sendMessage(message);
                    }

                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_back:
                ImagePageActivity.this.finish();
                break;

            case R.id.tv_comment:
                if (dataBean != null && dataBean.getContent() != null) {
                    Intent commentIntent = new Intent(ImagePageActivity.this, CommentActivity.class);
                    commentIntent.putExtra("id", dataBean.getContent().getId());
                    startActivity(commentIntent);
                }
                break;
        }
    }
}
