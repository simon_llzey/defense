package com.defense.utils;

import android.widget.Toast;

import com.defense.custom.OverallApplication;

/**
 * Created by simon on 17/3/22.
 */

public class ToastUtils {
    public static void show(String msg){
        Toast.makeText(OverallApplication.context, msg, Toast.LENGTH_SHORT).show();
    }
}
