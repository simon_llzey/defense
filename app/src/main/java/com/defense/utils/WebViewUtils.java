package com.defense.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.defense.R;
import com.just.agentwebX5.AgentWeb;

/**
 * Created by simon on 17/3/23.
 */

public class WebViewUtils {
    /**
     * 加载html适配屏幕
     *
     * @param content
     */
    public static void optionHtmlWebView(String content, WebView webView) {
        if (null == content || content.equals("")) {
            content = "<p>暂无详情</p>";
        }

        WebSettings webSettings = webView.getSettings();
        //设置WebView属性，能够执行Javascript脚本
        webSettings.setJavaScriptEnabled(true);

        webSettings.setPluginState(WebSettings.PluginState.ON);
        webSettings.setUseWideViewPort(true);  //将图片调整到适合webview的大小
        webSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true); //支持通过JS打开新窗口
        webSettings.setLoadsImagesAutomatically(true);  //支持自动加载图片
        webSettings.setDomStorageEnabled(true);
        webSettings.setDefaultTextEncodingName("UTF-8");//设置编码格式
        //设置可以访问文件
        webSettings.setAllowFileAccess(true);
        //设置支持缩放
        webSettings.setBuiltInZoomControls(true);
// 设置可以支持缩放
        webSettings.setSupportZoom(false);
//自适应屏幕
//        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);

        //设置WebView属性，能够执行Javascript脚本
        /*webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);  //将图片调整到适合webview的大小
        webSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);

        webSettings.setJavaScriptCanOpenWindowsAutomatically(true); //支持通过JS打开新窗口
        webSettings.setLoadsImagesAutomatically(true);  //支持自动加载图片
        webSettings.setDomStorageEnabled(true);
        webSettings.setDefaultTextEncodingName("utf-8");//设置编码格式
        //设置可以访问文件
//        webSettings.setAllowFileAccess(true);
        //设置支持缩放
        webSettings.setBuiltInZoomControls(false);
        webSettings.setSupportZoom(false);

        webView.setInitialScale(25);*/

        content = content.replace("https", "http");
        StringBuffer sb = new StringBuffer();
        sb.append("<html>");
        sb.append("<head>");
        sb.append("<meta name=\"viewport\" content=\"initial-scale=1.0\"/>");
        sb.append("<style>");
        sb.append("img{width:");
        sb.append("100%");
        sb.append("!important;height:auto !important;}");
        sb.append("</style>");
        sb.append("</head>");
        sb.append("<div style='margin-top:10px;font-size:12px'>");
        sb.append(content);
        sb.append("</div>");
        sb.append("</html>");


//        webView.loadData(sb.toString(), "text/html", "UTF-8");
//        webView.loadUrl("https://v.qq.com/iframe/player.html?vid=o054644cx58&tiny=0&auto=0");


        webView.loadDataWithBaseURL("", sb.toString(), "text/html", "UTF-8", null);

//        webView.setWebChromeClient(new NewWebChromeClient());
//        webView.setWebViewClient(new MyWebViewClient());

    }

    public static AgentWeb optionHtmlWebView(Activity activity, String content, LinearLayout root) {
        if (null == content || content.equals("")) {
            content = "<p>暂无详情</p>";
        }
//        content = content.replace("https", "http");
        StringBuffer sb = new StringBuffer();
        sb.append("<html>");
        sb.append("<head>");
        sb.append("<meta name=\"viewport\" content=\"initial-scale=1.0\"/>");
        sb.append("<style>");
        sb.append("img{width:");
        sb.append("100%");
        sb.append("!important;height:auto !important;}");
        sb.append("</style>");
        sb.append("</head>");
        sb.append("<div style='margin-top:10px;font-size:12px'>");
        sb.append(content);
        sb.append("</div>");
        sb.append("</html>");


        AgentWeb webView = AgentWeb.with(activity)//传入Activity or Fragment
                .setAgentWebParent(root, new LinearLayout.LayoutParams(-1, -1))//传入AgentWeb 的父控件 ，如果父控件为 RelativeLayout ， 那么第二参数需要传入 RelativeLayout.LayoutParams ,第一个参数和第二个参数应该对应。
                .useDefaultIndicator()// 使用默认进度条
                .defaultProgressBarColor() // 使用默认进度条颜色
//                                    .setReceivedTitleCallback(mCallback) //设置 Web 页面的 title 回调
                .createAgentWeb()//
                .ready()
                .go("");

        webView.getJsEntraceAccess().quickCallJs("callByAndroid");
        webView.getLoader().loadDataWithBaseURL("", sb.toString(), "text/html", "UTF-8", "");

        webView.getWebLifeCycle().onResume();
        return webView;
    }

    /**
     * j加载url
     *
     * @param content
     */
    private static void optionUrlWebView(String content, WebView webView, String Url) {
        WebSettings webSettings = webView.getSettings();
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setDefaultTextEncodingName("UTF-8");
        StringBuffer sb = new StringBuffer();
        sb.append("<html>");
        sb.append("<head>");
        sb.append("<style>");
        sb.append("img{width:");
        sb.append("100%");
        sb.append("!important;height:auto !important;}");
        sb.append("</style>");
        sb.append("</head>");
        sb.append("<div style='margin-top:20px;font-size:15px'>");
        sb.append(content);
        sb.append("</div>");
        sb.append("</html>");
//        String headUrl = AppConfigUtil.getValue(context, "HTTP_URL");
        webView.loadDataWithBaseURL(Url, sb.toString(), "text/html", "UTF-8", null);
    }


    //Web视图
    private static class MyWebViewClient extends WebViewClient {
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.e("tag", "加载视频的----》" + url);
            view.loadUrl(url);
            return super.shouldOverrideUrlLoading(view, url);
        }
    }

}
