package com.defense.utils;

import android.content.Context;

import com.defense.custom.OverallApplication;
import com.defense.view.ClassRoomDetailActivity;

/**
 * Created by simon on 17/4/18.
 */

public class LoginUtils {

    /**
     * 是否登录 没有登录跳转登录
     * @return
     */
    public static boolean isLogin(Context context) {
        if (OverallApplication.userData != null) {
            return true;
        }
        ToastUtils.show("请先登录");
        IntentUtils.jumpToLogin(context);
        return false;
    }


    /**
     * 是否登录
     * @param context
     * @return
     */
    public static boolean isLogin2(Context context) {
        if (OverallApplication.userData != null) {
            return true;
        }
        return false;
    }

}
