package com.defense.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Administrator on 2016/9/27.
 */

public class ShareUtils {
    /**
     * 保存boolean值
     *
     * @param context
     * @param val1
     * @param val2    值对应的key
     * @param val3    保存的值
     */
    public static void saveBoolean(Context context, String val1, String val2, boolean val3) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(val1, Context.MODE_PRIVATE);
        sharedPreferences.edit().putBoolean(val2, val3).commit();
    }

    /**
     * 获取boolean值
     *
     * @param context
     * @param val1
     * @param val2    值对应的key
     * @param val3    默认值
     * @return
     */
    public static Boolean getBoolean(Context context, String val1, String val2, boolean val3) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(val1, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(val2, val3);
    }


    /**
     * 保存字符串
     *
     * @param context
     * @param val1
     * @param val2
     * @param val3
     */
    public static void saveString(Context context, String val1, String val2, String val3) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(val1, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(val2, val3).commit();
    }

    public static String getString(Context context, String val1, String val2, String val3) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(val1, Context.MODE_PRIVATE);
        return sharedPreferences.getString(val2, val3);
    }

    /**
     * @param context
     * @param val1
     * @param val2
     * @param val3
     */
    public static void saveHashset(Context context, String val1, String val2, HashSet val3) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(val1, Context.MODE_PRIVATE);
        sharedPreferences.edit().putStringSet(val2, val3).apply();
    }

    public static Set<String> getHashset(Context context, String val1, String val2, HashSet val3) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(val1, Context.MODE_PRIVATE);
        return sharedPreferences.getStringSet(val2, val3);
    }


    public static void clear(Context context, String val1) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(val1, Context.MODE_PRIVATE);
        sharedPreferences.edit().clear().commit();
    }
}
