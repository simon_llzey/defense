package com.defense.utils;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;

import com.defense.custom.CustomDialog;

import org.xutils.common.Callback;
import org.xutils.common.util.LogUtil;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.File;
import java.io.IOException;
import java.util.Observer;

/**
 * Created by simon on 17/4/28.
 */

public class DownloadMan {

    public static Callback.Cancelable cancelable;
    public static DownloadMan downloadMan;
    public Context mContext;

    public DownloadMan(Context context) {
        this.mContext = context;
    }

    public static DownloadMan getInstance(Context context) {
        if (downloadMan == null) {
            downloadMan = new DownloadMan(context);
        }
        return downloadMan;
    }


    public void downloadFile(String url, final Observer obs) throws IOException {

        String name = url.substring(url.lastIndexOf("/"), url.length());
        Log.e("download", "文件名称=" + name);
        Log.e("download", "下载路径=" + url);
        //文件存在直接返回
        if (FileUtils.isExist(FileUtils.getPath() + name)) {
            obs.update(null, FileUtils.getPath() + name);
            return;
        }


        RequestParams requestParams = new RequestParams(url);
        requestParams.setSaveFilePath(FileUtils.getPath() + name);
//        requestParams.setAutoRename(true);
        requestParams.setAutoResume(true);
        requestParams.setConnectTimeout(15 * 1000);
        /*x.http().get(requestParams, new Callback.CommonCallback<File>() {
            @Override
            public void onSuccess(File f) {
                obs.update(null, f.getAbsolutePath());
            }

            @Override
            public void onError(Throwable throwable, boolean b) {

                obs.update(null, null);
            }

            @Override
            public void onCancelled(CancelledException e) {

            }

            @Override
            public void onFinished() {
            }
            
        });*/

        cancelable = x.http().get(requestParams, new Callback.ProgressCallback<File>() {
            @Override
            public void onSuccess(File file) {
                obs.update(null, file.getAbsolutePath());
            }

            @Override
            public void onError(Throwable throwable, boolean b) {
                obs.update(null, null);
            }

            @Override
            public void onCancelled(CancelledException e) {

            }

            @Override
            public void onFinished() {

            }

            @Override
            public void onWaiting() {

            }

            @Override
            public void onStarted() {

            }

            @Override
            public void onLoading(long l, long l1, boolean b) {
                int pro = (int) ((l1 * 100) / l);
                Log.e("download", "下载文件进度=" + pro + ",总长度" + l + ",-" + l1);
            }
        });
    }


    public void closeDownload() {
        if (cancelable != null) {
            cancelable.cancel();
        }
    }

}
