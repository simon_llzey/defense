package com.defense.utils;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2016/5/24.
 * 时间转换
 */
public class DateUtils {


    /**
     * 年月日时分转时间戳
     *
     * @param time
     * @return
     */
    public static Long date(String time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Long d = 0L;
        try {
            Date date = format.parse(time);
            d = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    /**
     * 时间戳转年月日时分秒
     *
     * @param time
     * @return
     */
    public static String date2(long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String dateStr = "";
        dateStr = format.format(time);
        return dateStr;
    }

    /**
     * 时间戳转年月日时分
     *
     * @param time
     * @return
     */
    public static String date3(long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String dateStr = "";
        dateStr = format.format(time);

        return dateStr;
    }

    /**
     * 时间戳转时分
     *
     * @param time
     * @return
     */
    public static String date4(long time) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        String dateStr = "";
        dateStr = format.format(time);

        return dateStr;
    }

    /**
     * 时间戳转年月日
     *
     * @param time
     * @return
     */
    public static String timesTurnDate(Long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        Long t = new Long(time);
        String d = format.format(t);
        return d;
    }

    /**
     * 时间戳转年月日格式不同
     *
     * @param time
     * @return
     */
    public static String yearMonthDay(Long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Long t = new Long(time);
        String d = format.format(t);
        return d;
    }

    /**
     * 时间戳获取日
     *
     * @param time
     * @return
     */
    public static String timesTurnDay(Long time) {
        SimpleDateFormat format = new SimpleDateFormat("dd");
        Long t = new Long(time);
        String d = format.format(t);
        return d;
    }

    /**
     * 时间戳获取年月
     *
     * @param time
     * @return
     */
    public static String timesTurnMonth(Long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM");
        Long t = new Long(time);
        String d = format.format(t);
        return d;
    }

    /**
     * 年月日转时间戳
     *
     * @param time
     * @return
     */
    public static Long dateTurnTimes(String time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        Long d = 0L;
        try {
            Date date = format.parse(time);
            d = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }


    /**
     * 时间转时间戳
     *
     * @param time
     * @return
     */
    public static Long timeTurnTimes(String time) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        Long d = 0L;
        try {
            Date date = format.parse(time);
            d = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    /**
     * 时间戳转时间
     *
     * @param time
     * @return
     */
    public static String timesTurnTime(Long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Long t = new Long(time);
        String d = format.format(t);
        return d;
    }

    /**
     * 时间戳转化为Date
     *
     * @return
     */
    public static Date timesTurnDate1(Long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String d = format.format(time);
        Date date = null;
        try {
            date = format.parse(d);
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("DateUtils", "时间戳转化为Date错误");
        }
        return date;
    }


    /**
     * 获取两个时间戳相隔的时间
     * @param startTime
     * @param endTime
     */
    public static int getTimeResult(String startTime, String endTime){
        DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = df.parse(startTime);
            d2 = df.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.e("Dis", "开始时间"+d1.getTime()+",结束时间"+d2.getTime());
        long diff = d2.getTime() - d1.getTime();// 这样得到的差值是微秒级别
        long days = diff / 1000;
        int betweenDays = (int)days;
//        int betweenDays = (int)((d2.getTime() - d1.getTime()) / 1000);
        return betweenDays;
    }
}
