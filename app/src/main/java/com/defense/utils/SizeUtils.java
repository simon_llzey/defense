package com.defense.utils;

import android.content.Context;
import android.view.WindowManager;

/**
 * Created by Administrator on 2017/3/13.
 */

public class SizeUtils {
    public static int getWidth(Context context) {
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);

        int width = wm.getDefaultDisplay().getWidth();
        return width;
    }
}
