package com.defense.utils;

import java.text.DecimalFormat;

/**
 * Created by Administrator on 2016/11/17.
 */

public class NumberUtils {

    /**
     * 保留两位
     * @param number
     * @return
     */
    public static String forTwo(double number){
        DecimalFormat df = new DecimalFormat("###0.00");
        return df.format(number);
    }

}
