package com.defense.utils;

import android.os.Environment;

import java.io.File;

/**
 * Created by Administrator on 2017/2/27.
 */

public class FileUtils {

    public static final String Division = File.separator;
    public static final String ROOT_NAME = "defense";

    public static String getPath() {

//        String path = "";
//        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
        String root_path = Environment.getExternalStorageDirectory().getAbsolutePath() + Division + ROOT_NAME;
        File file = new File(root_path);
        if (file.exists()) {
            return file.getPath();
        } else {
            file.mkdirs();
            return file.getPath();
        }
       /* }else {
            String root_path = Environment.getExternalStorageDirectory().getAbsolutePath()+Division+ROOT_NAME;
            File file = new File(root_path);
            if (file.exists()){
                return file.getPath();
            }else {
                file.mkdirs();
                return file.getPath();
            }
        }*/
    }


    /**
     * 判断文件是否存在
     * @param path
     * @return
     */
    public static boolean isExist(String path) {
        File file = new File(path);
        if (file.exists()) {
            return true;
        }
        return false;
    }

}
