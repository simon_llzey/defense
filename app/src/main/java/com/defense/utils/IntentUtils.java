package com.defense.utils;

import android.content.Context;
import android.content.Intent;

import com.defense.view.LoginActivity;

/**
 * Created by simon on 17/4/14.
 */

public class IntentUtils {

    /**
     * 未登录时跳转登录界面
     * @param context
     */
    public static void jumpToLogin(Context context){
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }


    /**
     * 跳转到界面
     * @param context
     * @param c
     */
    public static void jumpToOther(Context context, Class c){
        Intent intent = new Intent(context, c);
        context.startActivity(intent);
    }




}
