package com.defense.model;

import com.defense.http.model.ResultParam;
import com.google.gson.annotations.SerializedName;

/**
 * Created by simon on 17/3/28.
 * 教官详情
 */

public class InstructorDetailParam extends ResultParam {

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {

        private boolean praiseSign = false;
        private ContentBean content;

        public boolean isPraiseSign() {
            return praiseSign;
        }

        public void setPraiseSign(boolean praiseSign) {
            this.praiseSign = praiseSign;
        }

        public ContentBean getContent() {
            return content;
        }

        public void setContent(ContentBean content) {
            this.content = content;
        }

        public static class ContentBean {

            private int id;
            private String title;
            private String intro;
            private String image;
            private String details;
            private int d_type;
            private int module_id;
            private int home;
            private int status;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getIntro() {
                return intro;
            }

            public void setIntro(String intro) {
                this.intro = intro;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getDetails() {
                return details;
            }

            public void setDetails(String details) {
                this.details = details;
            }

            public int getD_type() {
                return d_type;
            }

            public void setD_type(int d_type) {
                this.d_type = d_type;
            }

            public int getModule_id() {
                return module_id;
            }

            public void setModule_id(int module_id) {
                this.module_id = module_id;
            }

            public int getHome() {
                return home;
            }

            public void setHome(int home) {
                this.home = home;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }
        }
    }
}
