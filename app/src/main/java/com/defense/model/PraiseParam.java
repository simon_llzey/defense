package com.defense.model;

import com.defense.http.model.ResultParam;

import java.util.List;

/**
 * Created by simon on 17/3/28.
 * 1.	获取文章点赞列表
 */

public class PraiseParam extends ResultParam {


    private List<PraiseData> data;

    public List<PraiseData> getData() {
        return data;
    }

    public void setData(List<PraiseData> data) {
        this.data = data;
    }

    public static class PraiseData {

        private int id;
        private String nick_name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getNick_name() {
            return nick_name;
        }

        public void setNick_name(String nick_name) {
            this.nick_name = nick_name;
        }
    }
}
