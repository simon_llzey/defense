package com.defense.model;

import com.defense.http.model.ResultParam;

import java.util.List;

/**
 * Created by simon on 17/3/22.
 * 获取登陆后按用户身份特定栏目子列表
 */

public class TradeResultParam extends ResultParam{

    private String basepath;
    private List<TradeData> data;

    public String getBasepath() {
        return basepath;
    }

    public void setBasepath(String basepath) {
        this.basepath = basepath;
    }

    public List<TradeData> getData() {
        return data;
    }

    public void setData(List<TradeData> data) {
        this.data = data;
    }

}
