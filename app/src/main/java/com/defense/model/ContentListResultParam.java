package com.defense.model;

import com.defense.http.model.ResultParam;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by simon on 17/3/23.
 * 1.	栏目内容列表接口
 */

public class ContentListResultParam extends ResultParam{

    private int totalRecord;
    private int pageSize;
    private int pageOffset;
    private List<ContentListParam> data;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageOffset() {
        return pageOffset;
    }

    public void setPageOffset(int pageOffset) {
        this.pageOffset = pageOffset;
    }

    public List<ContentListParam> getData() {
        return data;
    }

    public void setData(List<ContentListParam> data) {
        this.data = data;
    }

}
