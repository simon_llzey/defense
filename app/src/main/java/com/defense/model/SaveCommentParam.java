package com.defense.model;

import com.defense.http.model.ResultParam;

/**
 * Created by simon on 17/4/11.
 */

public class SaveCommentParam extends ResultParam {

    public DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean{

    }
}
