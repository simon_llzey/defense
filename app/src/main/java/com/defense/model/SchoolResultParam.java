package com.defense.model;

import com.defense.http.model.ResultParam;

import java.util.List;

/**
 * 学校列表
 * Created by simon on 2017/4/25.
 */

public class SchoolResultParam extends ResultParam {

    private SchoolBean data;

    public SchoolBean getData() {
        return data;
    }

    public void setData(SchoolBean data) {
        this.data = data;
    }

    public static class SchoolBean {
        private String basepath;
        private List<ImglistBean> imglist;

        public String getBasepath() {
            return basepath;
        }

        public void setBasepath(String basepath) {
            this.basepath = basepath;
        }

        public List<ImglistBean> getImglist() {
            return imglist;
        }

        public void setImglist(List<ImglistBean> imglist) {
            this.imglist = imglist;
        }

        public static class ImglistBean {
            private int id;
            private String name;
            private int trade_id;
            private String trade_name;
            private String logo;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getTrade_id() {
                return trade_id;
            }

            public void setTrade_id(int trade_id) {
                this.trade_id = trade_id;
            }

            public String getTrade_name() {
                return trade_name;
            }

            public void setTrade_name(String trade_name) {
                this.trade_name = trade_name;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }
        }
    }
}
