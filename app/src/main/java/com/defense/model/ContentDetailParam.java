package com.defense.model;

import com.defense.http.model.ResultParam;

/**
 * Created by simon on 17/3/23.
 * 1.	内容详情接口
 */

public class ContentDetailParam extends ResultParam{

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {

        private boolean praiseSign = false;
        private ContentDetail content;
        private int praise;
        private int browse;
        private String basepath;

        public boolean isPraiseSign() {
            return praiseSign;
        }

        public void setPraiseSign(boolean praiseSign) {
            this.praiseSign = praiseSign;
        }

        public ContentDetail getContent() {
            return content;
        }

        public void setContent(ContentDetail content) {
            this.content = content;
        }

        public int getPraise() {
            return praise;
        }

        public void setPraise(int praise) {
            this.praise = praise;
        }

        public int getBrowse() {
            return browse;
        }

        public void setBrowse(int browse) {
            this.browse = browse;
        }

        public String getBasepath() {
            return basepath;
        }

        public void setBasepath(String basepath) {
            this.basepath = basepath;
        }
    }
}
