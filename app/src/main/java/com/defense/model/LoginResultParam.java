package com.defense.model;

import com.defense.http.model.ResultParam;

/**
 * Created by simon on 17/3/22.
 * 登录
 */

public class LoginResultParam extends ResultParam {

    private UserData data;

    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }

    public static class UserData {
        private AccountBean account;
        private InfoBean info;
        private SchoolInfoBean schoolInfo;

        public AccountBean getAccount() {
            return account;
        }

        public void setAccount(AccountBean account) {
            this.account = account;
        }

        public InfoBean getInfo() {
            return info;
        }

        public void setInfo(InfoBean info) {
            this.info = info;
        }

        public SchoolInfoBean getSchoolInfo() {
            return schoolInfo;
        }

        public void setSchoolInfo(SchoolInfoBean schoolInfo) {
            this.schoolInfo = schoolInfo;
        }

        public static class AccountBean {

            private int id;
            private String username;
            private String email;
            private long create_time;
            private int status;
            private String phone;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public long getCreate_time() {
                return create_time;
            }

            public void setCreate_time(long create_time) {
                this.create_time = create_time;
            }

            public int getStatusX() {
                return status;
            }

            public void setStatusX(int statusX) {
                this.status = statusX;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }
        }

        public static class InfoBean {

            private String name;
            private String nick_name;
            private int sex;
            private int trade_id;
            private String introduce;
            private String pic;
            private int organization_id;
            private int school_id;
            private String score;
            private int number = 0;//记录考试次数

            private int id;
            private int member_id;
            private int second_school_id;
            private int specialty_id;
            private String grade;
            private String class_str;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getNick_name() {
                return nick_name;
            }

            public void setNick_name(String nick_name) {
                this.nick_name = nick_name;
            }

            public int getSex() {
                return sex;
            }

            public void setSex(int sex) {
                this.sex = sex;
            }

            public int getTrade_id() {
                return trade_id;
            }

            public void setTrade_id(int trade_id) {
                this.trade_id = trade_id;
            }

            public String getIntroduce() {
                return introduce;
            }

            public void setIntroduce(String introduce) {
                this.introduce = introduce;
            }

            public String getPic() {
                return pic;
            }

            public void setPic(String pic) {
                this.pic = pic;
            }

            public int getOrganization_id() {
                return organization_id;
            }

            public void setOrganization_id(int organization_id) {
                this.organization_id = organization_id;
            }

            public int getSchool_id() {
                return school_id;
            }

            public void setSchool_id(int school_id) {
                this.school_id = school_id;
            }

            public String getScore() {
                return score;
            }

            public void setScore(String score) {
                this.score = score;
            }

            public int getNumber() {
                return number;
            }

            public void setNumber(int number) {
                this.number = number;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getMember_id() {
                return member_id;
            }

            public void setMember_id(int member_id) {
                this.member_id = member_id;
            }

            public int getSecond_school_id() {
                return second_school_id;
            }

            public void setSecond_school_id(int second_school_id) {
                this.second_school_id = second_school_id;
            }

            public int getSpecialty_id() {
                return specialty_id;
            }

            public void setSpecialty_id(int specialty_id) {
                this.specialty_id = specialty_id;
            }

            public String getGrade() {
                return grade;
            }

            public void setGrade(String grade) {
                this.grade = grade;
            }

            public String getClass_str() {
                return class_str;
            }

            public void setClass_str(String class_str) {
                this.class_str = class_str;
            }
        }

        public static class SchoolInfoBean {
            private String specialtyName = "";
            private String secSchoolName = "";
            private String schoolName = "";

            public String getSpecialtyName() {
                return specialtyName;
            }

            public void setSpecialtyName(String specialtyName) {
                this.specialtyName = specialtyName;
            }

            public String getSecSchoolName() {
                return secSchoolName;
            }

            public void setSecSchoolName(String secSchoolName) {
                this.secSchoolName = secSchoolName;
            }

            public String getSchoolName() {
                return schoolName;
            }

            public void setSchoolName(String schoolName) {
                this.schoolName = schoolName;
            }
        }
    }
}
