package com.defense.model;

import com.defense.http.model.ResultParam;

import java.util.List;

/**
 * Created by simon on 17/9/20.
 */

public class SchoolSpecialtyResultParam extends ResultParam {

    private String basepath;
    private List<DataBean> data;

    public String getBasepath() {
        return basepath;
    }

    public void setBasepath(String basepath) {
        this.basepath = basepath;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {

        private int id;
        private String name;
        private String school_id;
        private String second_school_id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSchool_id() {
            return school_id;
        }

        public void setSchool_id(String school_id) {
            this.school_id = school_id;
        }

        public String getSecond_school_id() {
            return second_school_id;
        }

        public void setSecond_school_id(String second_school_id) {
            this.second_school_id = second_school_id;
        }
    }
}
