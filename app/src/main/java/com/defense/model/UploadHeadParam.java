package com.defense.model;


import com.defense.http.model.ResultParam;

/**
 * Created by Administrator on 2017/1/9.
 * 上传头像返回
 */

public class UploadHeadParam extends ResultParam {

    private UploadData data;

    @Override
    public UploadData getData() {
        return data;
    }

    public void setData(UploadData data) {
        this.data = data;
    }

    public static class UploadData{
        boolean success = false;
        String header = "";

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getHeader() {
            return header;
        }

        public void setHeader(String header) {
            this.header = header;
        }
    }

}
