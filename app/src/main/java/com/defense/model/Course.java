package com.defense.model;

/**
 * Created by simon on 17/4/10.
 */

public class Course {

    private String name, room, teach, icon;//课程名称、上课教室，教师，课程编号
    int start, step; //开始上课节次， 一共几节课

    private int id;
    public Course(String name, String room, int start, int step,
                  String teach) {
        super();
        this.name = name;
        this.room = room;
        this.start = start;
        this.step = step;
        this.teach = teach;

    }

    public Course(int id, String name, String icon, int start, int step) {
        super();
        this.id = id;
        this.name = name;
        this.start = start;
        this.step = step;
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public String getTeach() {
        return teach;
    }

    public void setTeach(String teach) {
        this.teach = teach;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
