package com.defense.model;

import com.defense.http.model.ResultParam;
import com.google.gson.annotations.SerializedName;

/**
 * 风采详情内容
 * Created by simon on 17/3/27.
 */

public class TrainStyleDetailParam extends ResultParam {
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {

        private int comment;
        private ContentBean content;
        private int browse;

        public int getComment() {
            return comment;
        }

        public void setComment(int comment) {
            this.comment = comment;
        }

        public ContentBean getContent() {
            return content;
        }

        public void setContent(ContentBean content) {
            this.content = content;
        }

        public int getBrowse() {
            return browse;
        }

        public void setBrowse(int browse) {
            this.browse = browse;
        }

        public static class ContentBean {

            private int id;
            private String intro;
            private String details;
            private int d_type;
            private long create_time;
            private int author;
            private int module_id;
            private int home;
            private int status;
            private String nickname;
            private int tradeId;
            private String tradename;
            private int schoolId;
            private String schoolname;
            private String photo;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getIntro() {
                return intro;
            }

            public void setIntro(String intro) {
                this.intro = intro;
            }

            public String getDetails() {
                return details;
            }

            public void setDetails(String details) {
                this.details = details;
            }

            public int getD_type() {
                return d_type;
            }

            public void setD_type(int d_type) {
                this.d_type = d_type;
            }

            public long getCreate_time() {
                return create_time;
            }

            public void setCreate_time(long create_time) {
                this.create_time = create_time;
            }

            public int getAuthor() {
                return author;
            }

            public void setAuthor(int author) {
                this.author = author;
            }

            public int getModule_id() {
                return module_id;
            }

            public void setModule_id(int module_id) {
                this.module_id = module_id;
            }

            public int getHome() {
                return home;
            }

            public void setHome(int home) {
                this.home = home;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public int getTradeId() {
                return tradeId;
            }

            public void setTradeId(int tradeId) {
                this.tradeId = tradeId;
            }

            public String getTradename() {
                return tradename;
            }

            public void setTradename(String tradename) {
                this.tradename = tradename;
            }

            public int getSchoolId() {
                return schoolId;
            }

            public void setSchoolId(int schoolId) {
                this.schoolId = schoolId;
            }

            public String getSchoolname() {
                return schoolname;
            }

            public void setSchoolname(String schoolname) {
                this.schoolname = schoolname;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }
        }
    }
}
