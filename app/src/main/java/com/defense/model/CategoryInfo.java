package com.defense.model;

/**
 * Created by Administrator on 2017/3/7.
 */

public class CategoryInfo {
    private int img;
    private String title;

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
