package com.defense.model;

import com.defense.http.model.ResultParam;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by simon on 17/4/13.
 * 军事计划
 */

public class ScheduleResultParam extends ResultParam{

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {

        private ScheduleBean schedule;
        private List<List<ListBean>> list;

        public ScheduleBean getSchedule() {
            return schedule;
        }

        public void setSchedule(ScheduleBean schedule) {
            this.schedule = schedule;
        }

        public List<List<ListBean>> getList() {
            return list;
        }

        public void setList(List<List<ListBean>> list) {
            this.list = list;
        }

        public static class ScheduleBean {
            private int id;
            private int organization_id;
            private int status;
            private long create_time;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getOrganization_id() {
                return organization_id;
            }

            public void setOrganization_id(int organization_id) {
                this.organization_id = organization_id;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public long getCreate_time() {
                return create_time;
            }

            public void setCreate_time(long create_time) {
                this.create_time = create_time;
            }
        }

        public static class ListBean {
            private int num;
            private int id;
            private String name;
            private String icon;

            public int getNum() {
                return num;
            }

            public void setNum(int num) {
                this.num = num;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getIcon() {
                return icon;
            }

            public void setIcon(String icon) {
                this.icon = icon;
            }

            @Override
            public String toString() {
                return "ListBean{" +
                        "num=" + num +
                        ", id=" + id +
                        ", name='" + name + '\'' +
                        ", icon='" + icon + '\'' +
                        '}';
            }
        }
    }
}
