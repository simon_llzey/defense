package com.defense.model;

import com.defense.http.model.ResultParam;

import java.util.List;

/**
 * Created by simon on 17/3/23.
 * 1.	获取文章（国防知识）测试题
 */

public class AnswerResultParam extends ResultParam {
    private int totalRecord = -1;
    private int pageSize = -1;
    private int pageOffset = -1;
    private Data data;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageOffset() {
        return pageOffset;
    }

    public void setPageOffset(int pageOffset) {
        this.pageOffset = pageOffset;
    }

    @Override
    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data{

        private TradeBean trade;
        private List<DataBean> data;

        public TradeBean getTrade() {
            return trade;
        }

        public void setTrade(TradeBean trade) {
            this.trade = trade;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class TradeBean {
            private int id;
            private String name;
            private int count;
            private int second;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getCount() {
                return count;
            }

            public void setCount(int count) {
                this.count = count;
            }

            public int getSecond() {
                return second;
            }

            public void setSecond(int second) {
                this.second = second;
            }
        }

        public static class DataBean {

            private int id;
            private String question;
            private String title;
            private List<OptionListBean> option_list;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getQuestion() {
                return question;
            }

            public void setQuestion(String question) {
                this.question = question;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public List<OptionListBean> getOption_list() {
                return option_list;
            }

            public void setOption_list(List<OptionListBean> option_list) {
                this.option_list = option_list;
            }

            public static class OptionListBean {
                private boolean isSelect;
                private int sign;
                private String option;

                public boolean isSelect() {
                    return isSelect;
                }

                public void setSelect(boolean select) {
                    isSelect = select;
                }

                public int getSign() {
                    return sign;
                }

                public void setSign(int sign) {
                    this.sign = sign;
                }

                public String getOption() {
                    return option;
                }

                public void setOption(String option) {
                    this.option = option;
                }
            }
        }
    }



}
