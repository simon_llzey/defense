package com.defense.model;

import com.defense.http.model.ResultParam;

/**
 * 上传图片返回
 * Created by simon on 2017/4/25.
 */

public class UploadResult extends ResultParam {

    private DataBean data;

    private String basepath;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private String basePath;
        private String imglist;

        public String getBasePath() {
            return basePath;
        }

        public void setBasePath(String basePath) {
            this.basePath = basePath;
        }

        public String getImglist() {
            return imglist;
        }

        public void setImglist(String imglist) {
            this.imglist = imglist;
        }
    }

    public String getBasepath() {
        return basepath;
    }

    public void setBasepath(String basepath) {
        this.basepath = basepath;
    }
}
