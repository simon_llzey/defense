package com.defense.model;

import com.defense.http.model.ResultParam;

import java.io.Serializable;
import java.util.List;

/**
 * Created by simon on 17/3/27.
 * 军训风采
 */

public class TrainStyleParam extends ResultParam {

    private int totalRecord;
    private int pageSize;
    private int pageOffset;
    private List<TrainStyle> data;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageOffset() {
        return pageOffset;
    }

    public void setPageOffset(int pageOffset) {
        this.pageOffset = pageOffset;
    }

    public List<TrainStyle> getData() {
        return data;
    }

    public void setData(List<TrainStyle> data) {
        this.data = data;
    }

    public static class TrainStyle implements Serializable{

        private int id;
        private String title;
        private String image;
        private int d_type;
        private int module_id;
        private int status;
        private String extra1;
        private String extra2;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public int getD_type() {
            return d_type;
        }

        public void setD_type(int d_type) {
            this.d_type = d_type;
        }

        public int getModule_id() {
            return module_id;
        }

        public void setModule_id(int module_id) {
            this.module_id = module_id;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getExtra1() {
            return extra1;
        }

        public void setExtra1(String extra1) {
            this.extra1 = extra1;
        }

        public String getExtra2() {
            return extra2;
        }

        public void setExtra2(String extra2) {
            this.extra2 = extra2;
        }
    }
}
