package com.defense.model;

import com.defense.http.model.ResultParam;

/**
 * 学校列表详情
 * Created by simon on 2017/4/25.
 */

public class SchoolDetailParam extends ResultParam {

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private boolean praiseSign;
        private int comment;

        private ContentListParam content;
        private int praise;
        private int browse;

        public boolean isPraiseSign() {
            return praiseSign;
        }

        public void setPraiseSign(boolean praiseSign) {
            this.praiseSign = praiseSign;
        }

        public int getComment() {
            return comment;
        }

        public void setComment(int comment) {
            this.comment = comment;
        }

        public ContentListParam getContent() {
            return content;
        }

        public void setContent(ContentListParam content) {
            this.content = content;
        }

        public int getPraise() {
            return praise;
        }

        public void setPraise(int praise) {
            this.praise = praise;
        }

        public int getBrowse() {
            return browse;
        }

        public void setBrowse(int browse) {
            this.browse = browse;
        }
    }
}
