package com.defense.model;

/**
 * Created by simon on 17/3/22.
 * 特定栏目
 */

public class TradeRequestParam {
    private int pageSize = -1;//每页条数
    private int pageOffset = -1;//获取页的起始位置

    private int mid = -1;//栏目ID，获取指定栏目时填写
    private int pid = -1;//父栏目ID，获取指定栏目子列表时填写
    private int tid = -1;//职业ID，若为空，则默认使用登陆用户的职业ID

    private int module = -1;//栏目类型
    private int home = -1;//是否为顶部数据，为空表示全部，1-仅顶部数据，0-仅非顶部数据

    private int id = -1;//栏目内容ID，/ws/content/list.jhtml接口获取到的列表中的ID
    private int cId = -1;//文章ID

    private int browse = -1;//浏览量，1-新增一次浏览量，并返回总浏览量；0-不新增，但会返回总浏览量
    private int praise = -1;//赞数，0返回赞数，>0返回指定的数量赞列表，最大100；<0返回最大数量赞列表（100）
    private int comment = -1;//评论数，0返回评论数，>0返回指定的评论列表，最大100；<0返回最大数量评论列表（100）

    private String details;//评论详情


    public int getPageOffset() {
        return pageOffset;
    }

    public void setPageOffset(int pageOffset) {
        this.pageOffset = pageOffset;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }

    public int getModule() {
        return module;
    }

    public void setModule(int module) {
        this.module = module;
    }

    public int getHome() {
        return home;
    }

    public void setHome(int home) {
        this.home = home;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getcId() {
        return cId;
    }

    public void setcId(int cId) {
        this.cId = cId;
    }

    public int getBrowse() {
        return browse;
    }

    public void setBrowse(int browse) {
        this.browse = browse;
    }

    public int getPraise() {
        return praise;
    }

    public void setPraise(int praise) {
        this.praise = praise;
    }

    public int getComment() {
        return comment;
    }

    public void setComment(int comment) {
        this.comment = comment;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
