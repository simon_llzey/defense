package com.defense.model;

import com.defense.http.model.ResultParam;

import java.util.List;

/**
 * Created by simon on 17/3/24.
 * 1.	获取教官列表
 */

public class InstructorResultParam extends ResultParam{


    private List<InstructorData> data;

    public List<InstructorData> getData() {
        return data;
    }

    public void setData(List<InstructorData> data) {
        this.data = data;
    }

    public static class InstructorData {

        private int id;
        private int member_id;
        private String name;
        private String pic;
        private int content_id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getMember_id() {
            return member_id;
        }

        public void setMember_id(int member_id) {
            this.member_id = member_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public int getContent_id() {
            return content_id;
        }

        public void setContent_id(int content_id) {
            this.content_id = content_id;
        }
    }
}
