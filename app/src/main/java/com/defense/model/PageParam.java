package com.defense.model;

/**
 * Created by simon on 17/3/23.
 */

public class PageParam {

    private int pageSize = -1;//每页条数
    private int pageOffset = -1;//获取页的起始位置

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageOffset() {
        return pageOffset;
    }

    public void setPageOffset(int pageOffset) {
        this.pageOffset = pageOffset;
    }
}
