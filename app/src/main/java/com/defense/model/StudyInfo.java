package com.defense.model;

/**
 * Created by simon on 17/9/19.
 */

public class StudyInfo {
    private String name;
    private int type;
    private int spec_id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getSpec_id() {
        return spec_id;
    }

    public void setSpec_id(int spec_id) {
        this.spec_id = spec_id;
    }
}
