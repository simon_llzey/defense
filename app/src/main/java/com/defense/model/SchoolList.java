package com.defense.model;

import com.defense.http.model.ResultParam;

import java.util.List;

/**
 * Created by simon on 17/9/19.
 */

public class SchoolList extends ResultParam {


    private String basepath;
    private List<DataBean> data;

    public String getBasepath() {
        return basepath;
    }

    public void setBasepath(String basepath) {
        this.basepath = basepath;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {

        private SchoolBean school;
        private List<SchoolBean> secondSchool;

        public SchoolBean getSchool() {
            return school;
        }

        public void setSchool(SchoolBean school) {
            this.school = school;
        }

        public List<SchoolBean> getSecondSchool() {
            return secondSchool;
        }

        public void setSecondSchool(List<SchoolBean> secondSchool) {
            this.secondSchool = secondSchool;
        }

        public static class SchoolBean {
            /**
             * id : 9
             * name : 成都理工大学
             * logo : Dq/0d/1493707584340.png
             */

            private int id;
            private int school_id;
            private String name;
            private String logo;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getSchool_id() {
                return school_id;
            }

            public void setSchool_id(int school_id) {
                this.school_id = school_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }
        }
    }
}
