package com.defense.model;

/**
 * Created by simon on 17/3/22.
 * 获取登陆后按用户身份特定栏目子列表
 */

public class TradeData {
    private int id;
    private String name;
    private int p_id;
    private String showname;
    private String icon;
    private int enabled;
    private int can_del;

    private String tree_path;
    private int needTrade;
    private String intro;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getP_id() {
        return p_id;
    }

    public void setP_id(int p_id) {
        this.p_id = p_id;
    }

    public String getShowname() {
        return showname;
    }

    public void setShowname(String showname) {
        this.showname = showname;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public int getCan_del() {
        return can_del;
    }

    public void setCan_del(int can_del) {
        this.can_del = can_del;
    }

    public String getTree_path() {
        return tree_path;
    }

    public void setTree_path(String tree_path) {
        this.tree_path = tree_path;
    }

    public int getNeedTrade() {
        return needTrade;
    }

    public void setNeedTrade(int needTrade) {
        this.needTrade = needTrade;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }
}
