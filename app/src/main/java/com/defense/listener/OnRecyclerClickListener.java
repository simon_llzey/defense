package com.defense.listener;

/**
 * Created by Administrator on 2017/3/9.
 * recyclerview点击事件
 */

public interface OnRecyclerClickListener {
    void onItemListener(int position);
    void onItemMultiListener(int position, boolean isCheck);
}
