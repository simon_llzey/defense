package com.defense.custom;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.defense.BuildConfig;
import com.defense.R;
import com.defense.model.LoginParam;
import com.defense.model.LoginResultParam;
import com.tencent.smtt.sdk.QbSdk;

import org.xutils.x;


/**
 * Created by Administrator on 2016/8/9.
 */

public class OverallApplication extends Application {
    public static Context context;
    //保存与服务器对接的sessionid
    public static String cookie;
    //用户信息
    public static LoginResultParam.UserData userData;


    //用户资料

    public static Context getContext() {
        return context;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        x.Ext.init(this);
        x.Ext.setDebug(true);
        QbSdk.initX5Environment(getApplicationContext(), new QbSdk.PreInitCallback() {
            @Override
            public void onCoreInitFinished() {

            }

            @Override
            public void onViewInitFinished(boolean b) {

            }
        });
    }

    @Override
    public void onTerminate() {
        Log.d("App", "onTerminate");

        super.onTerminate();
    }

    public static LoginParam getLoginParam() {
        SharedPreferences read = getSharedPreFrences();
        if (read != null) {
            LoginParam param = new LoginParam();
            String password = read.getString("u_password", "");
            String username = read.getString("u_username", "");
            param.setUsername(username);
            param.setPassword(password);
            return param;
        } else {
            return null;
        }
    }

    public static SharedPreferences getSharedPreFrences() {
        SharedPreferences read = context.getSharedPreferences(context.getString(R.string.app_shared_preferences), Activity.MODE_WORLD_READABLE);
        return read;
    }
}
