package com.defense.custom;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.defense.R;
import com.defense.adapter.TradeListAdapter;
import com.defense.model.TradeData;
import com.defense.utils.SizeUtils;
import com.defense.view.KnowledgeDetailActivity;
import com.defense.view.fragment.KnowledgeFrag;

import java.util.List;

/**
 * Created by Administrator on 2017/3/8.
 */

public class PopupUtils {

    private PopupWindow popupWindow;
    private static PopupUtils popupUtils;

    public static PopupUtils getInstance() {
        if (popupUtils == null) {
            popupUtils = new PopupUtils();
        }
        return popupUtils;
    }

    int z = -1;
    int y = -1;

    /**
     * @param parent
     */
    public void showWindow(final Context context, View parent, int id, final List<TradeData> datas) {
        if (popupWindow == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.knowledge_category_list, null);
            // 创建一个PopuWidow对象
            ListView listView = (ListView) view.findViewById(R.id.lv_list);
            TradeListAdapter tradeListAdapter = new TradeListAdapter(context, datas);
            listView.setAdapter(tradeListAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    context.startActivity(new Intent(context, KnowledgeDetailActivity.class));
                    KnowledgeFrag.getInstance.getContentListData(datas.get(position).getId());
                    if (popupWindow != null)
                        popupWindow.dismiss();
                }
            });
            popupWindow = new PopupWindow(view, SizeUtils.getWidth(context), parent.getWidth(), true);
        }
        // 使其聚集
        popupWindow.setFocusable(true);
        // 设置允许在外点击消失
        popupWindow.setOutsideTouchable(true);
        // 这个是为了点击“返回Back”也能使其消失，并且并不会影响你的背景
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        if (z == -1 && y == -1) {
            int[] location = new int[2];
            parent.findViewById(id).getLocationOnScreen(location);
            z = location[0] - (location[0] / 20);
            y = location[1] + parent.findViewById(id).getHeight() + (location[0] / 30);
        }
        popupWindow.showAtLocation(parent.findViewById(id), Gravity.NO_GRAVITY, z, y);
    }


}
