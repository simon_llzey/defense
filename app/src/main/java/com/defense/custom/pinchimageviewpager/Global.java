package com.defense.custom.pinchimageviewpager;


import com.defense.custom.pinchimageviewpager.images.ImageSource;
import com.defense.custom.pinchimageviewpager.images.JingGuanImageSource;

import java.io.Serializable;
import java.util.List;

/**
 * Created by clifford on 16/3/8.
 */
public class Global implements Serializable {

    private ImageSource[] sTestImages;

    public Global(List<ImagesPageParam> imageList) {
        sTestImages = new ImageSource[imageList.size()];
        for (int i = 0; i < imageList.size(); i++) {
            sTestImages[i] = new JingGuanImageSource(imageList.get(i).getImage(), 950, 634);
        }
    }

    public ImageSource getTestImage(int i) {
        if (i >= 0 && i < sTestImages.length) {
            return sTestImages[i];
        }
        return null;
    }

    public int getTestImagesCount() {
        return sTestImages.length;
    }
}