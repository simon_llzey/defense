package com.defense.custom.pinchimageviewpager;

/**
 * Created by Administrator on 2017/3/9.
 */

public class ImagesPageParam {
    private String image;
    private String title;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
