package com.defense.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.widget.GridView;

/**
 * Created by Administrator on 2017/3/7.
 */

public class LineGridView extends GridView {
    public LineGridView(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
    }

    public LineGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LineGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        View localView1 = getChildAt(0);
        if (localView1 == null) {
            return;
        }
        int column = getWidth() / localView1.getWidth();
        int childCount = getChildCount();
        Paint localPaint;
        localPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        localPaint.setAntiAlias(true);//使用抗锯齿功能
        localPaint.setStyle(Paint.Style.FILL);
        localPaint.setColor(Color.parseColor("#d3d3d3"));
        localPaint.setStrokeWidth((float) 3.0);

        canvas.drawLine((getRight()-getLeft()) /2, getTop(),(getRight()-getLeft())/2, getBottom(), localPaint);
        for (int i = 0; i < childCount; i++) {
            View cellView = getChildAt(i);

            RectF oval = new RectF(0, 0, 30, 30);
//                oval.offset(cellView.getRight()-15, cellView.getBottom());
            oval.offset((cellView.getRight()-cellView.getLeft()) / 2 -15, cellView.getTop() + (cellView.getHeight() / 2 - 15));
            canvas.drawArc(oval, 10, 360, true, localPaint);//绘制圆弧，含圆心

            if ((i + 1) % column == 0) {
//                canvas.drawLine(cellView.getLeft(), cellView.getBottom(), cellView.getRight(), cellView.getBottom(), localPaint);
            }else if((i + 1) > (childCount - (childCount % column))){
             /* canvas.drawLine(cellView.getRight(), cellView.getTop(), cellView.getRight(), cellView.getBottom(), localPaint);
                RectF oval = new RectF(0, 0, 30, 30);
//                oval.offset(cellView.getRight()-15, cellView.getBottom());
                oval.offset(cellView.getRight() - 15, cellView.getTop() + (cellView.getHeight() / 2 - 15));
                canvas.drawArc(oval, 10, 360, true, localPaint);//绘制圆弧，含圆心*/

            } /*else if ((childCount % column) == 0) {
                canvas.drawLine(cellView.getRight(), cellView.getTop(), cellView.getRight(), cellView.getBottom(), localPaint);
                RectF oval = new RectF(0, 0, 30, 30);
//                oval.offset(cellView.getRight()-15, cellView.getBottom());
                oval.offset(cellView.getRight() - 15, cellView.getTop() + (cellView.getHeight() / 2 - 15));
                canvas.drawArc(oval, 10, 360, true, localPaint);//绘制圆弧，含圆心
            }*/ else {
                /*canvas.drawLine(cellView.getRight(), cellView.getTop(), cellView.getRight(), cellView.getBottom(), localPaint);
                RectF oval = new RectF(0, 0, 30, 30);
//                oval.offset(cellView.getRight()-15, cellView.getBottom());
                oval.offset(cellView.getRight() - 15, cellView.getTop() + (cellView.getHeight() / 2 - 15));
                canvas.drawArc(oval, 10, 360, true, localPaint);//绘制圆弧，含圆心*/
            }

        }
       /* if (childCount % column != 0) {
            for (int j = 0; j < (column - childCount % column); j++) {
                View cellView = getChildAt(childCount - 1);
                canvas.drawLine(cellView.getRight(), cellView.getTop(), cellView.getRight(), cellView.getBottom(), localPaint);
                RectF oval = new RectF(0, 0, 30, 30);
                oval.offset(cellView.getRight()-15, cellView.getBottom()/2);
                canvas.drawArc(oval, 10, 360, true, localPaint);//绘制圆弧，含圆心
            }
        }*/

        /*View localView1 = getChildAt(0);
        int column = getWidth() / localView1.getWidth();
        int childCount = getChildCount();
        Paint paint;
        paint = new Paint();
        paint.setAntiAlias(true);                       //设置画笔为无锯齿
        paint.setColor(Color.BLACK);                    //设置画笔颜色
        canvas.drawColor(Color.WHITE);                  //白色背景
        paint.setStrokeWidth((float) 3.0);              //线宽
        paint.setStyle(Paint.Style.STROKE);

        RectF oval=new RectF();

        //RectF对象

//        localPaint.setStyle(Paint.Style.STROKE);
//        localPaint.setColor(getContext().getResources().getColor(R.color.blue));

        for(int i = 0;i < childCount;i++){
            View cellView = getChildAt(i);
            if((i + 1) % column == 0){
            }else if((i + 1) > (childCount - (childCount % column))){
//                canvas.drawLine(cellView.getRight(), cellView.getTop(), cellView.getRight(), cellView.getBottom(), paint);
                oval.left=cellView.getRight();                              //左边
                oval.top=cellView.getTop();                                   //上边
                oval.right=cellView.getRight();                             //右边
                oval.bottom= cellView.getBottom();
                                             //下边
                canvas.drawArc(oval, 25, 150, true, paint);    //绘制圆弧

            }else{
//                canvas.drawLine(cellView.getRight(), cellView.getTop(), cellView.getRight(), cellView.getBottom(), paint);
                oval.left=cellView.getRight();                              //左边
                oval.top=cellView.getTop();                                   //上边
                oval.right=cellView.getRight();                             //右边
                oval.bottom= cellView.getBottom();                //下边
                canvas.drawArc(oval,25, 150, true, paint);    //绘制圆弧

            }
        }*/
    }
}
