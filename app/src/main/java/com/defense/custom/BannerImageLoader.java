package com.defense.custom;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.defense.R;
import com.youth.banner.loader.ImageLoader;

/**
 * Created by Administrator on 2017/2/28.
 */

public class BannerImageLoader extends ImageLoader {

    @Override
    public void displayImage(final Context context, Object path, ImageView imageView) {
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Glide.with(context).
                load(path).
                asBitmap().
                placeholder(R.mipmap.def_3).
                into(imageView);


    }
}
