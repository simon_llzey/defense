package com.defense.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.defense.R;

/**
 * Created by Administrator on 2017/3/13.
 */

public class CusLinearLayout extends LinearLayout {
    public CusLinearLayout(Context context) {
        super(context);
    }
    public CusLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CusLinearLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        View cellView = getRootView();
        Paint localPaint;
        localPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        localPaint.setAntiAlias(true);//使用抗锯齿功能
        localPaint.setStyle(Paint.Style.FILL);
        localPaint.setColor(getContext().getResources().getColor(R.color.blue));
        localPaint.setStrokeWidth((float) 5.0);

        canvas.drawLine(cellView.getLeft(), cellView.getTop(), cellView.getLeft(), cellView.getBottom(), localPaint);
        RectF oval = new RectF(0, 0, 30, 30);
//                oval.offset(cellView.getRight()-15, cellView.getBottom());
//        oval.offset(cellView.getLeft() -30, cellView.getTop() + (cellView.getHeight() / 2 - 15));
        canvas.drawArc(oval, 10, 360, true, localPaint);//绘制圆弧，含圆心
    }
}
