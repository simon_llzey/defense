package com.defense.custom.snapscrollview;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

import com.defense.R;


public class ProductContentPage implements SnapPageLayout.SnapPage {
	
	private Context context;

	private View rootView = null;

	private LinearLayout linearLayout;//展示商品详情



	public ProductContentPage(Context context, View rootView) {
		this.context = context;
		this.rootView = rootView;

		linearLayout = (LinearLayout) this.rootView
				.findViewById(R.id.lin_bottom);
	}

	@Override
	public View getRootView() {
		return rootView;
	}

	@Override
	public boolean isAtTop() {
		int scrollY = linearLayout.getScrollY();
		int height = linearLayout.getHeight();

		if (scrollY ==0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isAtBottom() {
		return false;
	}

}
