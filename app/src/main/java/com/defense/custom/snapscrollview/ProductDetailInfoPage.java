package com.defense.custom.snapscrollview;

import android.content.Context;
import android.view.View;

import com.defense.R;


public class ProductDetailInfoPage implements SnapPageLayout.SnapPage{
	
	private Context context;
	
	private View rootView = null;
	private MyScrollView mcoyScrollView = null;
	
	public ProductDetailInfoPage(Context context, View rootView) {
		this.context = context;
		this.rootView = rootView;
		mcoyScrollView = (MyScrollView) this.rootView
				.findViewById(R.id.scrollview_dommoditydetail);
	}
	
	@Override
	public View getRootView() {
		return rootView;
	}

	@Override
	public boolean isAtTop() {
		return true;
	}

	@Override
	public boolean isAtBottom() {
        int scrollY = mcoyScrollView.getScrollY();
        int height = mcoyScrollView.getHeight();
        int scrollViewMeasuredHeight = mcoyScrollView.getChildAt(0).getMeasuredHeight();

        if ((scrollY + height) >= scrollViewMeasuredHeight) {
            return true;
        }
        return false;
	}

}
